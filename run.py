#!flask/bin/python
if __name__ == "__main__":
    from app import app
    from os import environ
    if 'WINGDB_ACTIVE' in environ:
        app.debug = False
    else:
        app.debug = True

    app.run(host='0.0.0.0',port=5000,threaded=True)

