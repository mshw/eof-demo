import ConfigParser
class config_wrap(object):
    def __init__(self,section="EyeOnFiles"):
        self.cfg = ConfigParser.ConfigParser()
        self.cfg.read('eof.cfg')
        self.section = section
    def __getitem__(self,key):
        return self.cfg.get(self.section, key)
    
CONFIG = config_wrap()