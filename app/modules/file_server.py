from ..lib.class_user import User
from ..lib.helper import get_logout_back_url
from ..lib.html_wrapper import Html_wrapper

# widgets
from ..lib import widget_file_server
from ..lib.widget_page_title		import WidgetPageTitle
from ..lib.widget_content_title 	import WidgetContentTitle
from ..lib.class_share_dir import ShareDir
from ..lib.class_smart_folder import SmartFolder
from flask import session,flash
from button import button

from ..lib import localization as localization

class file_server(object):
    def __init__(self):
        self.user = User.current()
        self.hpt_title = ""
        self.hpt_shares = ""
        self.btn_addshare = button()
        self.btn_addsmart = button()
    def add_share(self,name):
        if not name:
            loc = localization.get_lang()
            flash(loc['empty_field'],category=loc["error"])
            return        
        sd = ShareDir()
        sd.name = name
        sd.save()		
    def add_smartfolder(self, name):
        if not name:
            loc = localization.get_lang()
            flash(loc['empty_field'],category=loc["error"])
            return
        sf = SmartFolder()
        sf.name = name
        sf.save()
    def delete_share(self,guid):
        fs = ShareDir.get_by_guid(guid)
        if fs:
            fs.delete()
        else:
            sf = SmartFolder.get_by_guid(guid)
            if sf:
                sf.delete()

    def render(self):
        user = self.user
        # page title
        page_title = WidgetPageTitle(self)
        page_title.set_data("header_fs")
        page_title.render()
        self.hpt_title = self.title
        add_share = ""
        add_smartfolder = ""
        # title (main block)
        #	widget_file_server.title_template(self.cnt_title.rtxt_title)

        #content_title = WidgetContentTitle(self.hpt_title)
        #content_title.set_data(	title_key = "fs_title" )
        #content_title.render()

        # buttons correcting
        # widget_file_server.edit_buttons(handler=self.btn_addshare)


        loc = localization.get_lang()	

        if not user.is_admin():
            #self.btn_addshare.visible = self.btn_addsmart.visible = "0"
            #self.hpt_shares_top = "170"
            pass
        else:
            lang = localization.get_lang_name()
            btn_params = [
                {"en":140, "ru":160, "fr":185},
                {"en":160, "ru":200, "fr":245}
            ]


            add_share = widget_file_server.button_simple_dialog(loc["fs_addshare"], loc["fs_addshare"], 
                                                                "add_share", loc["create"])
            add_smartfolder = widget_file_server.button_simple_dialog(loc["fs_add_smartfolder"], loc["fs_add_smartfolder"], 
                                                                      "add_smartfolder", loc["create"])			


            #widget_btn_add 	= WidgetButton(self.btn_addshare)
            #widget_btn_add.set_data( title_key = "fs_addshare")
            #widget_btn_add.icon = "add_share"
            #widget_btn_add.text_color = "green"
            #widget_btn_add.width = btn_params[0][lang]
            #widget_btn_add.render()

            #widget_btn_add_smart = WidgetButton(self.btn_addsmart)
            #widget_btn_add_smart.set_data( title_key = "fs_add_smartfolder")
            #widget_btn_add_smart.icon = "add_share"
            #widget_btn_add_smart.text_color = "green"
            #widget_btn_add_smart.width = btn_params[1][lang]
            #widget_btn_add_smart.render()
            #self.btn_addsmart_left = {"en":420, "ru":440, "fr":465}[lang]


        #self.d_rules.title = loc["acl_settings"]

        #self.d_addshare.form.rtxt_title_1.value = loc["fs_addshare"]
        #self.d_addshare.form.rtxt_title_2.value = loc["fs_add_smartfolder"]
        #self.d_addshare.form.btn_addshare_cancel.label = loc["cancel"]
        #self.d_addshare.form.btn_addshare_create.label = loc["create"]

        #self.d_add_subject.cnt.tab.addgroup.title = loc["uag_groups"]
        #self.d_add_subject.cnt.tab.adduser.title = loc["uag_users"]

        # FileShares drawing
        self.hpt_shares = widget_file_server.draw_file_share_folders(user)
        self.hpt_data = add_share+add_smartfolder+self.hpt_shares
        back_url = """ / <a href="/administration">%(admin_header)s</a>""" % {"admin_header" : loc["admin_header"]}
        self.hpt_title = Html_wrapper.get_title(loc['fs_title'], back_url)        
