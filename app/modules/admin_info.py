from ..lib.html_wrapper import Html_wrapper
from ..lib.class_user import User
from ..lib import localization as local
from flask import flash,redirect
from ..lib.utils_migration import clear_db

class admin_info():
    def __init__(self):
        self.user = User.current()
       
        
    def delete_all(self):
        clear_db(False)
            
            
    def render(self):
        loc = local.get_lang()
        self.title = loc["header_hgmb"] + " &raquo; " + loc['info_title']
        back_url = """ / <a href="/administration">%(admin_header)s</a>""" % {"admin_header" : loc["admin_header"]}
        self.hpt_title = Html_wrapper.get_title(loc['info_title'], back_url)        
        self.hpt_data = Html_wrapper.get_left_tree(self.user)+Html_wrapper.get_newlicense()+Html_wrapper.clear_db()+Html_wrapper.view_log()
        
