from ..lib.class_user import User
from ..lib.class_folder import Folder
from ..lib.html_wrapper import Html_wrapper
from ..lib.class_document_share import Document_share, MaxDocException
from ..lib.class_notification import Notification
from ..lib import localization as localization
from ..lib.class_ACL_proxy import rules
from ..lib.class_ACL import ACL
#from ..lib.StringIO import StringIO
from ..lib.helper import scalar
from ..lib.xappy_fulltextsearch import fulltextsearch
from ..lib.xappy_indexconnector import indexconnector
from ..lib.utils_old import get_metas_fromargs,get_document_edit
from ..lib import dialogs
from flask import request, redirect, flash,session
import cgi


class document_share():
    def __init__(self):

        self.user = User.current()
        self.user_is_admin = self.user.can(rules["ADMIN"], "0")#application.id
        self.hpt_data = ""

    def del_folder(self, guid, notify = True):
        loc = localization.get_lang()
        if self.user:
            del_folder = Folder.get_by_guid(guid)
            xconn = indexconnector()
            xsearch = fulltextsearch()
            if del_folder:
                del_folder.set_xapy_access(xconn,xsearch)
                try:
                    del_folder.set_deleted_true()


                    # Delete inner folders
                    for inn_folder in del_folder.get_children():
                        self.del_folder(inn_folder.guid, False)

                    # Delete document shares
                    for doc_share in del_folder.get_document_shares():
                        if doc_share.deleted != 'true':
                            self.del_doc_share(del_folder, doc_share.guid, False)
                except Exception, e:
                    msg = str(e)
                    flash(loc[msg] if msg in loc else msg, category=loc["hp_errtitle"])

    def del_doc_share(self, folder, del_doc_share, notify = True):

        loc = localization.get_lang()
        if folder and self.user.can(rules["READ"], folder.guid):
            del_share = Document_share.get_by_guid(del_doc_share)

            if del_share:
                try:
                    del_share.set_deleted_true()

                    # Set deleted all files in folder
                    documents = []
                    xconn = indexconnector()
                    xsearch = fulltextsearch()

                    del_share.set_connector(xconn)
                    del_share.set_search(xsearch)
                    docsa, count = del_share.get_documents()
                    for doc in docsa:
                        d_name = doc['db_id'][0]
                        documents.append(d_name)
                    del_share.set_documents_deleted(documents, notify)
                except Exception, e:
                    mess = "&mess=" + str(e)
                    msg = str(e)
                    flash(loc[msg] if msg in loc else msg, category=loc["hp_errtitle"])
                    redirect("/documents_share?guid=" + self.folder.guid + mess)

    def new_folder(self, name):
        if self.user and self.user_is_admin:
            if not name:
                loc = localization.get_lang()
                flash(loc['empty_field'],category=loc["error"])
                return            
            folder = Folder()
            folder.name = name
            folder.save()

    def download_doc(self,guid, download_doc, preview=False):
        document_share = Document_share.get_by_guid(guid)
        xconn = indexconnector()
        xsearch = fulltextsearch()	    
        document_share.set_connector(xconn)
        document_share.set_search(xsearch)	
        #from class_config import Config
        #from class_document import Document

        #report_share_guid = Config().get_value("report_share")
        #if report_share_guid == document_share.guid:
                ##raise Exception("True")
                #document = document_share.get_doc_by_db_id(download_doc)
                #if document:
                        #path = document.complete_path();
                        #if application.storage.exists(path):
                                #content = application.storage.readall(path).decode("utf8")
                                #self.hpt_data.htmlcode = content
                                #opened_document = True
        #else:
        return document_share.send_file(download_doc,preview=preview)        

    def operate_docshare(self,docshare):
        """ operation whith docs on doc_share (Add, Delete, send, download)"""
        loc = localization.get_lang()
        doc_share = Document_share.get_by_guid(docshare)
        #raise Exception(unicode([[a, args.get(a)] for a in args if a not in ["sid", "sender"]]))
        
        if doc_share:
            xconn = indexconnector()
            xsearch = fulltextsearch()	    
            doc_share.set_connector(xconn)
            doc_share.set_search(xsearch)
            try:
                ret = document_share_operations(doc_share)
                if ret and isinstance(ret, str) or isinstance(ret, unicode):
                    send_report = ret
                    flash("%(send_report)s!<br>%(mail_fsend_receiver)s: %(email)s" % {
                            "send_report" : loc[send_report],
                            "mail_fsend_receiver" : loc["mail_fsend_receiver"],
                            "email" : request.form["email"]
                        },loc["mail_fsend_title"]
                    )
                else:
                    return ret
            except Exception, e:
                #raise
                flash(str(e),loc["hp_errtitle"])

    def render_doc_share(self, guid, parent):
        loc = localization.get_lang()
        doc_share = Document_share.get_by_guid(guid)
        folder = Folder.get_by_guid(parent)

        self.title = loc["header_hgmb"]
        if not doc_share or not folder:
            flash(loc["share_not_found"], category=loc["hp_errtitle"])
            return
        if not self.user.can(rules['READ'], doc_share.guid):
            flash(loc["ds_noaccess"], category=loc["warning"])
            return
        if doc_share.deleted == 'true':
            flash(loc["ds_deleted"], category=loc["hp_errtitle"])
        xconn = indexconnector()
        xsearch = fulltextsearch()
        folder.set_xapy_access(xconn, xsearch)        
        title = u"""<a href="document_share?guid=%(ds_guid)s&parent=%(ds_parent)s" >
            %(ds_name)s</a>
            """ % {
                    "ds_guid" : doc_share.guid,
                    "ds_parent" : folder.guid,
                    "ds_name" : doc_share.name
                }
        back_url = Html_wrapper.get_back_url(folder)
        if doc_share.deleted == 'true':
            edit_url = ""
        elif self.user.can(rules['USER_ADMIN'], doc_share.guid):
            edit_url = u"""document_share_edit?guid=%(ds_guid)s&parent=%(ds_parent)s""" % {
                "ds_guid" : doc_share.guid,
                "ds_parent" : folder.guid
            }
        else:
            edit_url = ""
        self.title += ' &raquo; ' + doc_share.name
        if not self.user.is_admin():
            notifybtn = Html_wrapper.get_notify_changer_widjet(doc_share, True)
        else:
            notifybtn = ""
        self.hpt_title = Html_wrapper.get_title(title, back_url,edit_url,guid, notifybtn=notifybtn)

        doc_share.set_connector(xconn)

        doc_share.set_search(xsearch)

        notif = None
        if "notif" in request.args:
            notif = Notification.get_by_guid(request.args["notif"])

        self.hpt_data += Html_wrapper.get_search_result(doc_share,folder,"",sortby='-2',max_docs=25, notif=notif, ignore_deleted=True)
        self.hpt_data += dialogs.del_files()

        from ..lib.class_user import User
        from ..lib.class_group import Group
        users = User.get_all()
        groups = Group.get_all()
        email_data = groups_data = ""
        for user in users:
            email_data += u""""%(user_mail)s",""" % {"user_mail": user.email}

        for group in groups:
            groups_data += u""""%(group_name)s",""" % {"group_name": group.name}


        self.hpt_data += js_helper_email % {"tags": email_data, "groups": groups_data}
        self.hpt_data += dialogs.create_sharing()

    def render(self):
        loc = localization.get_lang()
        #share_guid = guid
        message = ""
        if "mess" in request.args:
            message = request.args["mess"]
            # DOCUMENT_SHARE_IS_NOT_EMPTY
            flash(loc[message] if message in loc else message, category=loc["hp_errtitle"])

        if "msg" in request.args:
            message = request.args["msg"]
            flash(message, category="EOF Workflow")
        if self.user:
            if "no_licence" in request.args:
                self.growl = (
                    loc["hp_errtitle"], loc['workflow_no_licence'], "1") #self.growl2.title, self.growl2.text,self.growl2.active
            self.title = loc["header_hgmb"] + ' &raquo; ' + loc["header_dss"]
            back_url = """ / <a href="/administration">%(admin_header)s</a>""" % {"admin_header" : loc["admin_header"]}
            self.hpt_title = Html_wrapper.get_title(loc['ds_title'], back_url)             
            #u"""<span>%(ds_title)s</span>%(guid_map)s
            #    """ % {
            #            "ds_title": loc["ds_title"],
            #            "guid_map": Html_wrapper.get_guid_map("", loc["header_sitemap"])
            #        }
            self.hpt_data = Html_wrapper.get_big_search_form2()
            #old 
            #self.hpt_data += Html_wrapper.get_search_helper(1)

            
            self.hpt_data += Html_wrapper.get_ds_catalog(self.user)
            self.hpt_data += js_helper % {"delete_dialog" : dialogs.delete_folder()}
            self.cnt_srchinfo = Html_wrapper.get_search_helper("search_helper")        #self.cnt_srchinfo.front.text.htmlcode
            
    def edit_guid_share(self, edit_guid_share,edit_guid_doc,args):
        doc_share = Document_share.get_by_guid(edit_guid_share)
        xconn = indexconnector()
        xsearch = fulltextsearch()          
        doc_share.set_connector(xconn)
        doc_share.set_search(xsearch)
    
        doc = doc_share.get_doc_by_db_id(edit_guid_doc)
    
        if doc_share and doc:
            if "editing_end" in args:
                """change metas of document """
                Notification.delete_reminders_for_user_and_doc(doc_share, self.user, doc.guid())
                Notification.send_event("update_file", User.current(), doc_share, [doc.guid()])
                doc_share.update_document(doc.guid(), get_metas_fromargs(doc_share, args), args)
                return ""
            else:
                """ Show Edit form"""
                ret = get_document_edit(doc_share, doc, args)        
                #self.hpt_data += ret
                return ret

def document_share_operations(doc_share,page_cont=None):
    result = ""

    loc = localization.get_lang()
    if "file" in request.files:
        """ add document
	    """
        if 'lastupload' not in session:
            session['lastupload'] = []
        if not request.form['randkey'].isdigit() or request.form['randkey'] in session['lastupload']:
            return page_cont
        session['lastupload'].append(request.form['randkey'])
        f = request.files['file']
        file_data = f
        file_name = f.filename

        #raise Exception(unicode(file_data))
        try:
            doc = doc_share.put_document(file_name, file_data, get_metas_fromargs(doc_share, request.form, page_cont), ret_doc=True, args=request.form)
            Notification.send_event("added_file", User.current(), doc_share, [doc.guid()])
        except MaxDocException:
            flash(loc['MAXIMUM_DOCUMENTS_LIMIT'], loc["hp_errtitle"])
        #raise Exception("YES")
        return page_cont

    """ a list of available documents
    """
    documents = []
    docsa = []
    if doc_share:
        docsa, count = doc_share.get_documents()
        for doc in docsa:
            d_name = doc["db_id"][0]
            if d_name in request.form:
                documents.append(d_name)

    """ do operations """
    if "task" in request.form:
        task = request.form["task"]
        if task == "del":
            """ delete """
            doc_share.set_documents_deleted(documents)
            # Set flag for archive
            if doc_share.deleted == 'false':
                doc_share.set_deleted_partial()
                # Start recursion to check all folders
                parent_guid = doc_share.get_document_share_parent(doc_share.guid)
                parent_folder = Folder.get_by_guid(parent_guid)
                parent_folder.set_parents_partial_deleted()
        elif task == "down":
            """ download """
            return doc_share.get_files(documents)
        elif task == "email":
            """ send email"""
            import re

            subj = request.form["subj"]
            msg = request.form["msg"]
            do_zip = request.form.get("zipattach") == "true"
            if "email" in request.form:
                mail_str = request.form["email"]
                mail_list = [mail.strip() for mail in re.split("[,;]\s*", mail_str)]
                pattern = re.compile("^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$")
                for mail in mail_list:
                    if not pattern.match(mail):
                        continue
                    ret = doc_share.send_files_by_email(mail, documents, subj, msg, do_zip=do_zip)
                    if ret and isinstance(ret, str) or isinstance(ret, unicode):
                        send_report = ret
                        flash("%(send_report)s!<br>%(mail_fsend_receiver)s: %(email)s" % {
                            "send_report": loc[send_report],
                            "mail_fsend_receiver": loc["mail_fsend_receiver"],
                            "email": mail
                        }, loc["mail_fsend_title"])
            else:
                from ..lib.class_group import Group

                group_str = request.form["group"]
                group_list = [mail.strip() for mail in re.split("[,;]\s*", group_str)]
                for g in group_list:
                    group = Group.get_by_name(g)

                    if group:
                        users = User.get_all_by_group(group.guid)
                        for user in users:
                            ret = doc_share.send_files_by_email(user.email, documents, subj, msg, do_zip=do_zip)
                            if ret and isinstance(ret, str) or isinstance(ret, unicode):
                                send_report = ret
                                flash("%(send_report)s!<br>%(mail_fsend_receiver)s: %(email)s" % {
                                    "send_report": loc[send_report],
                                    "mail_fsend_receiver": loc["mail_fsend_receiver"],
                                    "email": user.email
                                }, loc["mail_fsend_title"])
        elif task == "move":
            doc_share.delete_all_contents()
            Notification.send_event("update_ds", User.current(), doc_share)
        elif task == "set_full_text":
            doc = doc_share.get_doc_by_db_id(request.form["doc_id"])
            doc.set_fulltext(request.form["fulltext"].strip())
            doc_share.edit_document(doc)
            Notification.send_event("update_file", User.current(), doc_share, [doc.guid()])

    return result

js_helper = """<script type="text/javascript">
function show_delete(ev, del_guid, del_folder)
{
	var d = jQuery("#delete_folder");
        d.modal('show');
	d.find("h3").html(ev.getAttribute("msgtitle"));
	d.find("#delete_folder_msg").html(ev.getAttribute("msgdescr"));
	if (typeof(del_guid) !== undefined) {
	    $('#del_share').attr('value', del_guid);
    }
	if (typeof(del_folder) !== undefined) {
	    $('#del_folder').attr('value', del_folder);
	}
};
</script>
%(delete_dialog)s
"""

js_helper_email="""
<script type="text/javascript">
var availableTags = [%(tags)s];
var availableGroups = [%(groups)s];
function split( val ) {
    return val.split(/[,;]\s*/);
}
function extractLast( term ) {
    return split( term ).pop();
}


$("#custom-combobox-email-input").on( "keydown", function( event ) {
    if ( event.keyCode === $.ui.keyCode.TAB && $( this ).autocomplete( "instance" ).menu.active ) {
        event.preventDefault();
    }
}).autocomplete({
    minLength: 0,
    source: function( request, response ) {
        response( $.ui.autocomplete.filter(availableTags, extractLast( request.term ) ) );
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {
        var terms = split( this.value );
        terms.pop();
        terms.push( ui.item.value );
        this.value = terms.join( ", " );
        return false;
    }
});
$("#custom-combobox-group-input").on( "keydown", function( event ) {
    if ( event.keyCode === $.ui.keyCode.TAB && $( this ).autocomplete( "instance" ).menu.active ) {
        event.preventDefault();
    }
}).autocomplete({
    minLength: 0,
    source: function( request, response ) {
        response( $.ui.autocomplete.filter(availableGroups, extractLast( request.term ) ) );
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {
        var terms = split( this.value );
        terms.pop();
        terms.push( ui.item.value );
        this.value = terms.join( ", " );
        return false;
    }
});
</script>
"""
