from ..lib.class_user import User
from ..lib.class_folder import Folder
from ..lib.class_search import Search, Search_object
from ..lib.html_wrapper import Html_wrapper
from ..lib.class_document_share import Document_share
from ..lib.class_metafield import Expression
from ..lib import localization as localization

from  ..lib import utils_old
from ..lib.xappy_fulltextsearch import fulltextsearch
from ..lib.xappy_indexconnector import indexconnector

from flask import request, redirect, flash
import json,cgi

class custom_search_edit():
    def __init__(self,guid=None):
        self.user = User.current()
        self.search = Search.get_by_guid(guid) or Search()
        self.guid = guid

    def render(self,):
        loc = localization.get_lang()     
        self.title = loc["header_hgmb"] + ' &raquo; ' + loc["header_cse"]
        search = self.search
        res = ""
        query = []
        expressions = []
        editurl = ""
        folder = ""        
        doc_title = ""
        xconn = indexconnector()
        xsearch = fulltextsearch()    
        folder = Folder.get_by_guid(self.guid)
        if not folder:
            document_share = Document_share.get_by_guid(self.guid)
            if document_share:
                folder = Folder.get_by_guid(document_share.get_document_share_parent(self.guid))
        else:
            document_share = None
                
        if "order" in request.form and request.form["order"] != "":
            expressions = utils_old.get_expressions_from_args(request.form)        

        if search.name:
            if self.user.has_admin_access_into(search):
                editurl = u"""/custom_search?guid=%(guid)s""" %{
                            "guid":search.guid,
                        }
            #search_view = "<a href='/custom_search?guid=%s'>%s</a>" % (search.guid, cgi.escape(search.name))
            doc_title = Html_wrapper.get_title(loc["s_custom"]+": <a href='/custom_search?guid=%s'>%s</a>"%(search.guid,cgi.escape(search.name)))
            
            obj = Search_object.get_by_search_guid(search.guid)
            tip=''
            obj_list=[]
            if obj:
                obj = obj[0]
                doc_share = Document_share.get_by_guid(obj.object_guid)
                if doc_share:
                    share_url = """<a href='document_share?guid=%(guid)s&parent=%(parent)s'>%(name)s</a>
                    """%{"guid":doc_share.guid, "parent":obj.parent_guid, "name":doc_share.name } 
                    obj_list.append(share_url)
                    while obj.parent_guid:
                        folder = Folder.get_by_guid(obj.parent_guid)
                        folder_url =  """<a href='folder?guid=%(guid)s'>%(name)s</a>
                        """%{"guid":folder.guid, "name":folder.name }                        
                        obj_list.append(folder_url)
                        obj = folder
                else:
                    obj = Folder.get_by_guid(obj.object_guid)
                    folder_url =  """<a href='folder?guid=%(guid)s'>%(name)s</a>
                    """%{"guid":folder.guid, "name":folder.name }                  
                    obj_list.append(folder_url)
                    while obj.parent_guid:
                        folder = Folder.get_by_guid(obj.parent_guid)
                        folder_url =  """<a href='folder?guid=%(guid)s'>%(name)s</a>
                        """%{"guid":folder.guid, "name":folder.name }                       
                        obj_list.append(folder_url)
                        obj = folder  
            s = " / ";
            tip = s.join(obj_list)            
            doc_title += " &emsp;&rArr;&emsp; "+ tip #'header_f'-Folder
            doc_title += u"""/  <a href="/documents_share">%(ds_title)s</a>  """ % {
                "ds_title": loc["ds_title"] #Documents share
            }
    
        if "saveform" in request.form:
    
            args_form = json.loads(request.form["saveform"])
            #raise Exception(str([(a, args.get(a)) for a in args]))
            expressions = utils_old.get_expressions_from_args(args_form)
    
        if search and not expressions:
            #flash("Folder is ", str(search))
            #popup_growl("Folder is ", str(search))
            expressions = search.get_expressions()
    
        #if "parent" in request.args and request.args["parent"] !="" :
    
            #parent = request.args["parent"]
    
            #folder = Folder.get_by_guid(parent)
            #document_share = Document_share.get_by_guid(self.guid)
    
        if folder and document_share and self.user.can_read_share(document_share):

            document_share.set_connector(xconn)
            document_share.set_search(xsearch)

            parametrs = "&parent=" + folder.guid

            back =   Html_wrapper.get_back_url(folder)
            share_url = """<a href='document_share?guid=%(guid)s&parent=%(parent)s'>%(name)s</a>
                                                        """%{"guid":document_share.guid, "parent":folder.guid, "name":document_share.name }
            doc_title = Html_wrapper.get_title(share_url, back,editurl=editurl)
            search_object = Search_object()
            search_object.object_guid = document_share.guid
            search_object.parent_guid = folder.guid

            search.set_objects([search_object])
            search.set_expressions(expressions)
        
    
        elif search and not search.name:
            
            #popup_growl("Folder is", str(folder))
            if folder:
                search_object = Search_object()
                search_object.object_guid = folder.guid
    
                search.set_objects([search_object])
                search.set_expressions(expressions)
    
                back =   Html_wrapper.get_back_url(folder.get_parent())
    
                folder_url =  """<a href='folder?guid=%(guid)s'>%(name)s</a>
                                                            """%{"guid":folder.guid, "name":folder.name }
    
                doc_title = Html_wrapper.get_title(folder_url, back, guid=folder.guid,editurl=editurl)

        if "save" in request.form and "gsave" in request.form and request.form.get("savename") !="":
            if  search.get_query():
                if request.form["gsave"] == "update":
                    search.name = ("savename" in request.form) and request.form.get("savename") or search.name
                    search.set_expressions(expressions)
                    search.save()
                    if not self.user.is_admin() :
                        self.user.add_access_to(search, "user_admin")
            
            
                elif request.form["gsave"] == "save_as" and search.get_query() and "savename" in request.form and request.form["savename"]:
                    search_new = Search()
                    search_new.get_query()
                    search_new.set_objects(search.get_objects())
                    search_new.set_expressions(expressions)
                    search_new.name = request.form.get("savename")
                    search_new.save()
                    if not self.user.is_admin():
                        self.user.add_access_to(search_new, "user_admin")
                    self.search = search_new
                    self.guid = search_new.guid   
        elif request.form.get("gsave") == "update":
            search.set_expressions(expressions)

        self.hpt_title = doc_title
        
        self.hpt_data = Html_wrapper.get_custom_search_form(search) + Html_wrapper.get_custom_search_result(search, xsearch, xconn )+\
            Html_wrapper.get_args_form(request.args)
        
      