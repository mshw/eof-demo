from ..lib.class_ACL import ACL
from ..lib.class_user import User
from ..lib.class_ACL_proxy import rules
from ..lib.html_wrapper import Html_wrapper
from ..lib.xappy_fulltextsearch import fulltextsearch
from ..lib.xappy_indexconnector import indexconnector
from ..lib import localization
from ..lib import utils_old
from ..lib.class_folder import Folder
from ..lib.class_document_share import Document_share

class tree_and_rights:

    def __init__(self):
        self.user = User.current()
        self.xconn = indexconnector()
        self.xsearch = fulltextsearch()
        self.user_is_admin = self.user.can(rules["ADMIN"], "0")
        self.hpt_title = ""
        self.hpt_data = ""
        self.title = ""

    def execute_acl(self, args):
        target_object = None
        guid = args["guid"]

        if args["target_type"] == "DS":
            target_object = Document_share.get_by_guid(guid)
        else:
            target_object = Folder.get_by_guid(guid)

        utils_old.execute_ACL(args, target_object, url=None)

    def render_admin(self):
        loc = localization.get_lang()
        self.title = loc["header_rtree"]
        back_url = """ / <a href="/administration">%(admin_header)s</a>""" % {"admin_header": loc["admin_header"]}
        self.hpt_title = Html_wrapper.get_title(loc["header_rtree"], back_url)
        self.hpt_data = Html_wrapper.get_tree_and_rights_editing(self.user)

    def render_print(self):
        loc = localization.get_lang()
        self.hpt_data = Html_wrapper.get_tree_and_rights_printable(self.user)
