from ..lib.class_user import User
from ..lib.class_folder import Folder
from ..lib.class_search import Search, Search_object
from ..lib.html_wrapper import Html_wrapper
from ..lib.class_document_share import Document_share
from ..lib.class_metafield import Expression
from ..lib import localization as localization

from  ..lib import utils_old
from ..lib.xappy_fulltextsearch import fulltextsearch
from ..lib.xappy_indexconnector import indexconnector

from flask import request, redirect, flash
import json,cgi

class custom_search_acl():
    def __init__(self,guid=None):
        self.user = User.current()
        self.search = Search.get_by_guid(guid)
        self.guid = guid
        
    def check(self):
        return bool(self.search is not None and self.user.has_admin_access_into(self.search) )
        
    def update_acl(self,args):
        utils_old.execute_ACL(args, self.search, "/custom_searches_acl?guid=" + self.search.guid)
        
    def render(self):
        loc = localization.get_lang()     
        self.title = loc["header_hgmb"] + ' &raquo; ' + loc["header_csacl"]
        search = self.search
        
        share_url = u"""<a href='/custom_search?guid=%(guid)s'>%(name)s</a>
                        """ %{"name":search.name, "guid":search.guid}
        
        back_url =  u""" / <a href="/searches">%(admin_cs)s</a> / <a href="/Administration">%(admin_header)s</a>""" % {
            #Html_wrapper.get_back_url(parent)
            "admin_cs": loc["admin_cs"],
            "admin_header": loc["admin_header"]
        }
        
        self.hpt_title = Html_wrapper.get_title(share_url, back_url)  
    
        self.hpt_data = Html_wrapper.get_users_form(search, "custom_search_acl")+Html_wrapper.get_groups_form(search, "custom_search_edit")            