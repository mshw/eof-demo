from ..lib.html_wrapper import Html_wrapper
from ..lib.class_user import User
from ..lib import localization as local
from ..lib.class_document_share import Document_share
from ..lib.class_folder import Folder
from ..lib.xappy_fulltextsearch import fulltextsearch
from ..lib.xappy_indexconnector import indexconnector
import cgi
from flask import flash
class archive_tree():
    def __init__(self):
        self.user = User.current()
        
    def rename(self, guid, new_name):
        loc = local.get_lang()
        folder = Folder.get_by_guid(guid)
        if folder:
            folder.rename(cgi.escape(new_name))
        else:
            doc_share = Document_share.get_by_guid(guid)
            if doc_share:
                doc_share.rename(cgi.escape(new_name))
            else:
                flash(loc["hp_errtitle"])
    def delete(self,guid):
        xconn = indexconnector()
        xsearch = fulltextsearch()        
        loc = local.get_lang()
        try:
            folder = Folder.get_by_guid(guid)
            if folder:
                folder.set_xapy_access(xconn, xsearch)
                folder.delete()
            else:
                doc_share = Document_share.get_by_guid(guid)
                if doc_share:
                    doc_share.set_connector(xconn)
                    doc_share.set_search(xsearch)                
                    doc_share.delete()
        except Exception as e:
            msg = e.message
            flash((loc[msg] if msg in loc else msg), loc["hp_errtitle"])
    def copy(self,source_guid, target_guid, num_copy="0"):
        loc = local.get_lang()
        if source_guid and target_guid:
            try:
                folder = Folder.get_by_guid(target_guid)
                if folder is None:
                    doc_share = Document_share.get_by_guid(target_guid)
                    target_guid = doc_share.get_document_share_parent(target_guid)
                    folder = Folder.get_by_guid(target_guid)
                xconn = indexconnector()
                xsearch = fulltextsearch()	                    
                folder.copy_structure(
                    self.user, xconn, xsearch,
                    [source_guid], target_guid,
                    count=int(num_copy)
                )
            except Exception, e:
                msg = e.message
                flash((loc[msg] if msg in loc else msg), loc["hp_errtitle"])
    def render(self):
        loc = local.get_lang()
        self.title = loc["header_hgmb"] + " &raquo; " + loc["header_map_edit"]
        back_url = """ / <a href="/administration">%(admin_header)s</a>""" % {"admin_header" : loc["admin_header"]}
        self.hpt_title = Html_wrapper.get_title(loc["header_map_edit"], back_url)        
        self.hpt_data = Html_wrapper.get_tree_editing(self.user)