from ..lib.class_ACL import ACL
from ..lib.class_user import User
from ..lib.class_group import Group
from ..lib.class_folder import Folder
from ..lib.html_wrapper import Html_wrapper
from ..lib.class_document_share import Document_share
from ..lib import utils_old
from ..lib.class_ACL_proxy import rules, ACL_record
from ..lib.class_metafield import Metafield
from ..lib.class_notification import Notification
from ..lib import html
import copy

from flask import redirect,request, flash
from ..lib import localization as localization
from ..lib.xappy_fulltextsearch import fulltextsearch
from ..lib.xappy_indexconnector import indexconnector
import cgi

class share_edit():
    def __init__(self, guid,parent):
        self.xconn = indexconnector()
        self.xsearch = fulltextsearch()
        self.user = User.current()
        self.user_is_admin = self.user.can(rules["ADMIN"], "0")#application.id
        self.guid = guid
        self.doc_share = Document_share.get_by_guid(guid)
        self.parent = Folder.get_by_guid(parent)

    def execute_acl(self, args):
        utils_old.execute_ACL(args, self.doc_share, "/document_share_edit?guid=%s&parent=%s"%(self.guid,self.parent))
        
    def share_rename(self, new_name):
        old_share = copy.copy(self.doc_share)
        self.doc_share.rename(new_name)
        Notification.send_event("update_ds", User.current(), old_share)

    def add_metafields(self, reqform):
        loc = localization.get_lang()
        metafield = utils_old.get_new_metafield_from_args(request.form, self.doc_share)
        if metafield:
            metafield.set_connector(self.xconn)
            metafield.save()
            Notification.send_event("update_ds", User.current(), self.doc_share)
            self.doc_share.add_metafield(metafield.id)
        else:
            flash(loc["uag_wrongvalues"],loc["hp_errtitle"])
            
    def del_metafields(self, del_met):
        metafield = Metafield.get_by_id(del_met)
        if metafield:
            Notification.send_event("update_ds", User.current(), self.doc_share)
            metafield.delete()
            return True

    def toggle_subscribe(self, form):
        if self.doc_share.subscribe == 'true' and "toggle_checkbox" not in form:
            self.doc_share.toggle_subscribe()
        elif self.doc_share.subscribe == 'false' and "toggle_checkbox" in form:
            self.doc_share.toggle_subscribe()
        
    def edit_metafields(self, reqform):
        metafield = utils_old.get_edited_metafield_from_args(reqform)
        if metafield:
            metafield.set_connector(self.xconn)
            Notification.send_event("update_ds", User.current(), self.doc_share)
            metafield.save()

            
    def can_edit(self):
        return bool(self.doc_share and self.user.can_edit_file_share(self.doc_share))
    
    def render(self):
        loc = localization.get_lang()
        
        doc_share = self.doc_share
        parametrs = "&parent=" + self.parent.guid
        #doc_share.add_metafield()
        back_url = Html_wrapper.get_back_url(self.parent)
        title = loc["admin_ds"] + u""": """ + u"""<a href="document_share?guid=%(ds_guid)s&parent=%(ds_parent)s" >
        %(ds_name)s</a>
        """ % {
            "ds_guid" : doc_share.guid,
            "ds_parent" : self.parent.guid,
            "ds_name" : doc_share.name
        }
        page_title = Html_wrapper.get_title(title, back_url,"",self.doc_share.guid)

        self.hpt_title = page_title


        doc_share.set_connector(self.xconn)
        self.parent.set_xapy_access(self.xconn, self.xsearch)
        res = ""
        if self.user.has_admin_access_into(self.doc_share):
            res += Html_wrapper.get_new_metafield_form(doc_share,"/document_share_edit",parametrs,self.parent.guid)
        #res += Html_wrapper.get_add_new_doc_form(doc_share)
        res += Html_wrapper.get_share_rename_form(doc_share,parametrs,"document_share_edit")
        res += Html_wrapper.get_share_toggle_subscribe_form(doc_share, "document_share_edit")
        res += Html_wrapper.get_users_form(doc_share, "document_share_edit",parametrs, "DS")
        res += Html_wrapper.get_groups_form(doc_share, "document_share_edit",parametrs, "DS")             
        self.hpt_data = res