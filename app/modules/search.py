from ..lib.class_user import User
from ..lib.class_folder import Folder
from ..lib.html_wrapper import Html_wrapper
from ..lib.class_document_share import Document_share
from ..lib import localization as localization
from ..lib.class_ACL_proxy import rules
from ..lib.class_smart_folder import SmartSubFolder
from ..lib.indexers import normalize
from ..lib.xappy_fulltextsearch import fulltextsearch
from ..lib.xappy_indexconnector import indexconnector
from ..lib.indexers import normalize
from ..lib import utils_old
from flask import request, redirect, flash
import cgi
from io import StringIO

TITLE =u"""<div>
        <span style="font-size: x-large" class="Apple-style-span">
        <font face="arial, helvetica, sans-serif" class="Apple-style-span">%(richtext)s
        <strong>
        <a style="color: #000000; cursor: pointer" title="%(search)s">%(search)s</a>&nbsp;
        </strong>
        </font>
        </span>
        </div>"""
class search():
    def __init__(self):
        self.loc = localization.get_lang()
        self.user = User.current()
        #self.user_is_admin = self.user.can(rules["ADMIN"], "0")
        self.xconn = indexconnector()
        self.xsearch = fulltextsearch()        
    
    def smart(self, search):
        smart_sub = SmartSubFolder.get_by_guid(search)   
        if smart_sub:
            self.hpt_data = smart_sub
            return smart_sub.name
            
            
    def get_data_for_folder(self, search,searchmethod, a_folder,result_title=""):
        xconn,xsearch = self.xconn, self.xsearch
        
        folder_res = StringIO()
        if a_folder:
    
            shares = self.user.get_documents_shares(a_folder)
            if shares:
                a_folder.set_xapy_access(xconn, xsearch)
                s_res = Html_wrapper.get_search_result_for_folder(xconn, shares, a_folder, search, sortby='-2', searchmethod=searchmethod)
                #raise Exception(str(s_res))
                if s_res:
                    for share, data in s_res.iteritems():
                        share_url = """<a href='/document_share?guid=%(guid)s&parent=%(parent)s' name='%(guid)s'>%(name)s</a>
                                                            """%{"guid":share.guid, "parent":a_folder.guid, "name":share.name }
                        back =   Html_wrapper.get_back_url(a_folder)
                        folder_res.write(Html_wrapper.get_title_search(share_url, back))
                        folder_res.write(data)
    
            for s_folder  in self.user.get_subfolders(a_folder)	:
                folder_res.write(self.get_data_for_folder(search,searchmethod,s_folder))
    
        folder_str = folder_res.getvalue()
        folder_res.close()
        return folder_str
    def render_all(self, search_query='', searchmethod=3,search_orig=""):

        loc = localization.get_lang()
        self.title = loc["header_hgmb"] + ' &raquo; ' + loc["header_s"]
        search = normalize(utils_old.float2base(utils_old.date2base(search_query))).lower()
        self.hpt_title =  TITLE% {
                "richtext" : loc["s_resulton"],
                "search" : cgi.escape(search_orig or search,True)   
            }        
        xconn,xsearch = self.xconn, self.xsearch
        sear_res_io = StringIO()
        for folder in self.user.get_top_folders():
            sear_res_io.write(self.get_data_for_folder(search, searchmethod,folder))  
            #sear_res_io.write(u"<hr/>")
            
        sear_res = sear_res_io.getvalue()
        sear_res_io.close()        
        self.hpt_data = Html_wrapper.get_big_search_form2(search=search_orig,checked=searchmethod) +sear_res

        from ..lib.class_user import User
        from ..lib.class_group import Group
        from ..lib import dialogs
        from documents_share import js_helper_email
        users = User.get_all()
        groups = Group.get_all()
        email_data = groups_data = ""
        for user in users:
            email_data += u""""%(user_mail)s",""" % {"user_mail": user.email}

        for group in groups:
            groups_data += u""""%(group_name)s",""" % {"group_name": group.name}

        self.hpt_data += js_helper_email % {"tags": email_data, "groups": groups_data}

        self.hpt_data += dialogs.del_files()
        self.hpt_data += dialogs.create_sharing()
        
    def render_guid(self, guid, parent = None, search_orig='', searchmethod=3):
        loc = localization.get_lang()
        search = normalize(utils_old.float2base(utils_old.date2base(search_orig))).lower()
        self.title = loc["header_hgmb"] + ' &raquo; ' + loc["header_s"]
        self.hpt_title =  TITLE% {
            "richtext" : loc["s_resulton"],
            "search" : cgi.escape(search,True)   
        }
        
        doc_title,sear_res = "",""
        xconn,xsearch = self.xconn, self.xsearch
        if parent:
            folder = Folder.get_by_guid(parent)
            document_share = Document_share.get_by_guid(guid)
        
            if folder and document_share and self.user.can_read_share(document_share):
                document_share.set_connector(xconn)
                document_share.set_search(xsearch)
        
                parametrs = "&parent=" + folder.guid
        
                back =   Html_wrapper.get_back_url(folder)
                share_url = """<a href='document_share?guid=%(guid)s&parent=%(parent)s'>%(name)s</a>
                                                                """%{"guid":document_share.guid, "parent":folder.guid, "name":document_share.name }
                doc_title = Html_wrapper.get_title_search(share_url, back)
                sear_res = Html_wrapper.get_search_result(document_share, folder, search, sortby='-2', searchmethod=searchmethod)            
        else:
            folder = Folder.get_by_guid(guid)
            if folder:
                
        
                back =   Html_wrapper.get_back_url(folder.get_parent())
        
                folder_url =  """<a href='folder?guid=%(guid)s'>%(name)s</a>
                                                                """%{"guid":folder.guid, "name":folder.name , "search":search}
                doc_title = Html_wrapper.get_title_search(folder_url, back) + "<hr/>"           
                sear_res = self.get_data_for_folder(search, searchmethod,folder)
        
        #docs = Document_share()
        #docs.set_connector(xconn)
        #docs.set_search(xsearch)
        #folder = Folder()
        #folder.set_xapy_access(xconn, xsearch)
        widget = u"""<div class="one_wrap">
<div class="widget" >
    <div class="widget_title_bjm"><h5>%s</h5></div>
    <div class="widget_body">
        <div class="content_pad">
        %s
        </div>
    </div>
</div>"""
        self.hpt_data = Html_wrapper.get_big_search_form2(guid or "",parent or "",search=search_orig,checked=searchmethod) +doc_title+sear_res
        #self.hpt_data += Html_wrapper.get_search_result(docs,folder,search,request.args,searchmethod=searchmethod)
        
        
    def render(self,search="",searchmethod="3"):
        loc = localization.get_lang()
    
        self.title = loc["header_hgmb"] + ' &raquo; ' + loc["header_s"]
        self.hpt_title =  TITLE% {
            "richtext" : loc["s_resulton"],
            "search" : normalize(utils_old.date2base(search or "")).lower()     
        }        
        self.hpt_data = Html_wrapper.get_big_search_form2(search=search,checked=searchmethod)