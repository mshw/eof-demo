from ..lib.class_ACL import ACL
from ..lib.class_user import User
from ..lib.class_group import Group
from ..lib.class_folder import Folder
from ..lib.html_wrapper import Html_wrapper
from ..lib.class_document_share import Document_share
from ..lib import utils_old
from ..lib.class_ACL_proxy import rules, ACL_record
from ..lib import html
import copy

#from ..lib.file_access import share

from flask import redirect,request
from ..lib.class_notification import Notification
from ..lib import localization as localization
import cgi
from ..lib import utils_old

class folder_edit():
    def __init__(self, guid):
        from ..lib.xappy_fulltextsearch import fulltextsearch
        from ..lib.xappy_indexconnector import indexconnector
        self.xconn = indexconnector()
        self.xsearch = fulltextsearch()
        self.user = User.current()
        self.admin = User.current_admin()
        self.user_is_admin = self.user.can(rules["ADMIN"], "0")#application.id
        self.guid = guid
        self.folder = Folder.get_by_guid(self.guid)

    def check(self):
        return bool(self.folder is not None)
    def folder_rename(self, new_name):
        old_folder = copy.copy(self.folder)
        self.folder.rename(new_name)

    def execute_acl(self, args):
        if self.folder:
            utils_old.execute_ACL(args, self.folder, "/folder_edit?guid=%s" % (self.guid,))

    def render(self):
        xconn = self.xconn
        xsearch = self.xsearch

        user = self.user
        loc  = localization.get_lang()


        self.title = loc["header_hgmb"] + " &raquo; " + loc["header_fe"]

        #obj="0bf6b912-d50b-44ae-af6e-dc0960a02cb9"
        #subj="9da9f8c2-e4d1-4c47-8bfa-d5f10ef81466"
        #raise Exception(str([str(s) for s in ACL_record.get_acls(subj, obj)]))

        folder = self.folder
        user_has_admin_access = user.can(rules["USER_ADMIN"],folder.guid)

        #search_form = Html_wrapper.get_search_form(folder.guid)

        #custom_search_link = u"""<br /><a href="/custom_search_edit?guid=%(guid)s">%(s_custom)s</a>
        #                """%{"guid":folder.guid, "s_custom" : loc["s_custom"]}

        #self.hpt_search_form = search_form + custom_search_link

        back_url = Html_wrapper.get_back_url(folder.get_parent())
        self.hpt_title = Html_wrapper.get_title(cgi.escape(folder.name), back_url, editurl='',  guid=folder.guid)

        self.hpt_data = Html_wrapper.get_folder_rename_form(folder,"folder_edit")+\
            Html_wrapper.get_users_form(folder, "folder_edit")+Html_wrapper.get_groups_form(folder, "folder_edit")
