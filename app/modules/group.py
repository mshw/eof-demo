from ..lib.class_user import User
from ..lib.class_group import Group
from ..lib.html_wrapper import Html_wrapper
from ..lib.class_document_share import Document_share
from ..lib import localization as local
from flask import redirect, url_for



group_form = ""
res = ""




class group():
    def __init__(self, guid):
        self.user = User.current()
        self.group = Group.get_by_guid(guid) if self.user.is_admin() else None
    def check(self):
        return bool(self.user and self.group)
    def get_group_form(self, user, group):
        loc = local.get_lang()
        hiden_space = ""
        delete_link = ""
        cur_users = ""
        group_users = [(usr.name, usr.guid) for usr in group.get_users()]

        for a_user in sorted(set(group_users)):
            cur_users += """ <tr>
    <td>%(user_email)s</td>
    <td>
        <span class="data_actions iconsweet">
        <a class="tip_north" original-title="Delete" href='group?guid=%(guid)s&delete=%(user_guid)s' title='%(delete)s'>X</a>
        </span>
    </td>
    </tr>""" %{
                "guid": group.guid,
                "user_guid": a_user[1],
                "user_email": a_user[0],
                "delete": loc["delete"]}

        available_users = ""
        all_users = [(usr.name, usr.guid) for usr in User.get_all()]
        add_users = set(all_users) - set(group_users)

        for a_user in sorted(set(add_users)):
            available_users += """ <option value='%(guid)s'>%(name)s</option> """ %{"guid":str(a_user[1]), "name":a_user[0] }

        ret_data = u"""<div class="one_wrap">
    <div class="widget" >
        <div class="widget_title_bjm"><h5>%(uag_users)s:</h5></div>
            <div class="widget_body "><div class="content_pad  ">
            <div class="project_sort">
                <table class="activity_datatable" width="100%%" border="0" cellspacing="0" cellpadding="8">
    <tbody>
    %(cur_users)s
    </tbody>
        </table>
        <style>.form_input_mod div.selector span{width: 300px;}</style>
        <form action="" method="POST">
        
            <ul class="form_fields_container">
            <input type='hidden' name='guid' value='%(guid)s'/>
                <li><label>%(uag_adduser)s</label><div class="form_input_mod" style="width: 300px;"><div class="selector" id="uniform-undefined">
                <select name="add_user" style="opacity: 0;">
                    %(avalible_users)s
                </select></div></div></li>
                <li><input type='submit' class='button_small whitishBtn fl_right' value='%(add)s' /></li>
                </ul>
            </form>        
    </div>
        </div></div>
        </div>
                        </div>""" % {
                        "delete_link":delete_link,
                        "cur_users": cur_users,
                        "hiden_space": hiden_space,
                        "avalible_users": available_users,
                        "uag_users": loc["uag_users"],
                        "uag_adduser": loc["uag_adduser"],
                        "add": loc["add"],
                        "guid":self.group.guid
                                  }
        return ret_data

   
    def acl_type(self):
        if self.group:
            from ..lib import utils_old as utol
            redir = "/group?guid=" + self.group.guid
            utol.execute_ACL(args1, self.group, redir)            
    def add_user(self, user):
        if self.group:
            self.group.add_user(user)            
            
    def delete_user(self,user):
        if self.group:
            self.group.delete_user(user)        
            
    def render(self):
        loc = local.get_lang()
        self.title = loc["header_hgmb"] + " &raquo; " + loc["header_g"]
        
        group_form = self.get_group_form(self.user, self.group)

        back_url = """/<a href="/users_and_groups">%(uag_title)s</a>"""%{"uag_title": loc["uag_title"]}
        self.hpt_title = Html_wrapper.get_title("%s: %s"%(loc["header_g"],self.group.name), back_url)
        self.hpt_data = group_form
