from ..lib.class_user import User
from ..lib import localization as local
from ..lib.html_wrapper import Html_wrapper
from flask import flash,redirect
from ..config import CONFIG
from ..lib.class_config import Config
from json import loads,dumps

class netconfig():
    def __init__(self):
        self.user = User.current()   
        self.connection_settings = {}
        self.connection = None
        #try:
            #import pynetlinux
            #nif = pynetlinux.ifconfig.list_ifs()[0]
            #mask = ''
            #l = nif.get_netmask()
            #for t in range(4):
                #if l > 7:
                    #mask += '255.'
                #else:
                    #dec = 255 - (2**(8 - l) - 1)
                    #mask += str(dec) + '.'
                #l -= 8
                #if l < 0:
                    #l = 0

            #self.network = {"server_ip":nif.get_ip(),"server_mask":mask[:-1],"server_gw":pynetlinux.route.get_default_gw()}
        #except ImportError:
            #self.network = {"server_ip":"","server_mask":"","server_gw":""}

    def set_smtp(self, server, port, user, passw, ssl):
        smtp_cfg = {"login":user,
                    "passw":passw,
                    "server":server,
                    "port":port,
                    "ssl":ssl,
                    }
        cfg = Config()
        cfg.set_value("smtpconfig",dumps(smtp_cfg))
        cfg.commit()

    def set_network(self, server, mask, gw):
        loc = local.get_lang()
        if CONFIG["MODE"] != "VM":
            flash ("You are not alowed to change network config for this installation",category=loc["hp_errtitle"])

    def render(self):
        loc = local.get_lang()
        self.title = loc["header_hgmb"] + " &raquo; " +loc["header_netconfig"]
        back_url = """ / <a href="/administration">%(admin_header)s</a>""" % {"admin_header" : loc["admin_header"]}
        self.hpt_title = Html_wrapper.get_title(loc["header_netconfig"], back_url)        
        if self.user.is_admin():
            cfg = Config()
            try:
                smtp_cfg = loads(cfg.get_value("smtpconfig")) or {}
            except Exception as e:
                print e
                smtp_cfg={}            
            self.hpt_data = Html_wrapper.get_netconfig(self.user,smtp_cfg,None)                