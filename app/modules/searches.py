from ..lib.class_user import User
from ..lib.class_folder import Folder
from ..lib.class_document_share import Document_share
from ..lib import localization as local
from ..lib.class_search import Search, Search_object
from ..lib.html_wrapper import Html_wrapper
from ..lib.dialogs import add_customsearch
from ..lib.xappy_fulltextsearch import fulltextsearch
from ..lib.xappy_indexconnector import indexconnector    
class searches():
    def __init__(self):
        self.user = User.current()
        
    def delete(self, guid):
        search = Search.get_by_guid(guid)
        if search:
            search.delete()       
            
    def render(self):
        loc = local.get_lang()
        self.title = loc["header_hgmb"] + " &raquo; " + loc["header_ss"]
        back_url = """ / <a href="/administration">%(admin_header)s</a>""" % {"admin_header" : loc["admin_header"]}
        self.hpt_title = Html_wrapper.get_title(loc["s_customs"], back_url)

        res  = ""
        items,tbody = "",""
        item_templ = u"""<tr><td><a class="tip_north" original-title="%(tip)s" href="custom_search?guid=%(guid)s">%(name)s</a></td>
            <td> <span class="data_actions iconsweet">
            <a class="tip_north adelete" original-title="Delete" href='/searches?del=%(guid)s'>X</a>
            <a class="tip_north" original-title="Edit" href="custom_search_edit?guid=%(guid)s">8</a>
            </span></td></tr>"""

        if self.user.is_admin():
            show_searches = self.user.get_searches()
        else:
            show_searches = self.user.get_admined_searches()
            
        for search in show_searches:
            obj = Search_object.get_by_search_guid(search.guid)
            tip=''
            obj_list=[]
            editurl = u"""/custom_search?guid=%(guid)s""" %{
                "guid":search.guid,   }
            if obj:
                obj = obj[0]
                doc_share = Document_share.get_by_guid(obj.object_guid)
                if doc_share:
                    obj_list.append(doc_share.name)
                    while obj.parent_guid:
                        folder = Folder.get_by_guid(obj.parent_guid)
                        obj_list.append(folder.name)
                        obj = folder
                else:
                    obj = Folder.get_by_guid(obj.object_guid)
                    obj_list.append(obj.name)
                    while obj.parent_guid:
                        folder = Folder.get_by_guid(obj.parent_guid)
                        obj_list.append(folder.name)
                        obj = folder                    
                        
                
                #if obj and obj.parent_guid != "":
                    #folder = Folder.get_by_guid(obj.parent_guid)
                    #doc_share = Document_share.get_by_guid(obj.object_guid)
                    #if doc_share and folder:
                        ##res += get_data_for_doc_share(doc_share, folder, 0)
                        #back =   Html_wrapper.get_back_url(folder)
                        #share_url = """<a href='document_share?guid=%(guid)s&parent=%(parent)s'>%(name)s</a>
                        #"""%{"guid":doc_share.guid, "parent":folder.guid, "name":doc_share.name }
                        #tip = Html_wrapper.get_title(share_url, back,editurl=editurl)                    
                    #else:
                        #folder = Folder.get_by_guid(obj.object_guid)
                        #if folder:
                            ##res += get_data_for_folder(folder, 0)
                            #back =   Html_wrapper.get_back_url(folder.get_parent())
                            #folder_url =  """<a href='folder?guid=%(guid)s'>%(name)s</a>
                            #"""%{"guid":folder.guid, "name":folder.name }
                            #doc_title = Html_wrapper.get_title(folder_url, back, guid=folder.guid,editurl=editurl) 
            s = " / ";
            #seq = ("a", "b", "c"); # This is sequence of strings.
            #print 
            tip = s.join(obj_list)            
            items +=item_templ%{"name": search.name, "guid": search.guid, "tip":tip}
            #add a tip with path
        xconn = indexconnector()
        xsearch = fulltextsearch()              
        self.hpt_data = u"""<div class="one_two_wrap fl_left">
        <div class="widget">
            <div class="widget_title_bjm"><h5>%(s_custom)s</h5></div>
            <div class="widget_body ">
            
                <div class="content_pad  ">
                %(addcs)s
                <hr>
                <table class="activity_datatable" width="100%%"><tbody>%(tbody)s</tbody></table></div>
            </div>
        </div>
    </div>"""%{'s_custom':loc['s_custom'],
               'tbody':items,
               "addcs":add_customsearch(Html_wrapper.tree_builder(User.current(), None, xconn, xsearch, use_url=False)    )
              }        