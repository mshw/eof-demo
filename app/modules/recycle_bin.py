from ..lib.class_user import User
from ..lib.class_folder import Folder
from ..lib.html_wrapper import Html_wrapper
from ..lib.class_document_share import Document_share, MaxDocException
from ..lib import localization as localization
from ..lib.class_ACL_proxy import rules
from ..lib.xappy_fulltextsearch import fulltextsearch
from ..lib.xappy_indexconnector import indexconnector
from ..lib import dialogs
from flask import request, redirect, flash, session
from ..lib.class_search import Search_object, Search
import cgi

class recycle_bin:

    def __init__(self):
        self.user = User.current()
        self.user_is_admin = self.user.can(rules["ADMIN"], "0")  # application.id
        self.hpt_data = ""

    def render_folder(self, guid):
        from folder import Folder

        folder = Folder.get_by_guid(guid)
        loc = localization.get_lang()
        self.title = loc["header_hgmb"] + " &raquo; " + loc["admin_rb"]

        if not folder:
            flash(loc["folder_not_found"], category=loc["hp_errtitle"])
            return

        edit_url = ""
        back_url = Html_wrapper.get_back_url_in_rb(folder.get_parent())

        title = Html_wrapper.get_title(cgi.escape(folder.name), back_url, edit_url, folder.guid)
        self.hpt_title = title

        self.hpt_data = Html_wrapper.get_rb_subcatalog(self.user, folder)
        self.hpt_data += dialogs.res_folder()
        self.hpt_data += js_helper % {"delete_dialog": dialogs.delete_folder()}

    def render_doc_share(self, guid, parent):
        from documents_share import Document_share
        from folder import Folder

        loc = localization.get_lang()
        doc_share = Document_share.get_by_guid(guid)
        folder = Folder.get_by_guid(parent)

        self.title = loc["admin_rb"]
        if not doc_share or not folder:
            flash(loc["share_not_found"], category=loc["hp_errtitle"])
            return

        xconn = indexconnector()
        xsearch = fulltextsearch()
        folder.set_xapy_access(xconn, xsearch)

        title = u"""<a href="recycle_bin_dshare?guid=%(ds_guid)s&parent=%(ds_parent)s" >%(ds_name)s</a>""" % {
            "ds_guid": doc_share.guid,
            "ds_parent": folder.guid,
            "ds_name": doc_share.name
        }
        back_url = Html_wrapper.get_back_url_in_rb(folder)
        edit_url = ""

        self.title += ' &raquo; ' + doc_share.name
        self.hpt_title = Html_wrapper.get_title(title, back_url, edit_url, guid)

        doc_share.set_connector(xconn)
        doc_share.set_search(xsearch)

        self.hpt_data += Html_wrapper.get_rb_search_result(doc_share, folder, sortby='-2', max_docs=25)
        self.hpt_data += dialogs.del_files()
        self.hpt_data += dialogs.res_files()

        from ..lib.class_user import User
        users = User.get_all()
        email_data = ""
        for user in users:
            email_data += u""""%(user_mail)s",""" % {"user_mail": user.email}

    def render(self):
        loc = localization.get_lang()
        self.title = loc["header_hgmb"] + ' &raquo; ' + loc["admin_rb"]
        back_url = """ / <a href="/administration">%(admin_header)s</a>""" % {"admin_header": loc["admin_header"]}
        self.hpt_title = Html_wrapper.get_title(loc['rb_title'], back_url)
        #self.hpt_data += Html_wrapper.get_search_result("", "", "", sortby='-2', max_docs=25)

        self.hpt_data += Html_wrapper.get_rb_catalog(self.user)
        self.hpt_data += dialogs.res_folder()
        self.hpt_data += js_helper % {"delete_dialog": dialogs.delete_folder()}

    def delete_folder(self, folder_guid):


        loc = localization.get_lang()
        if self.user:
            del_folder = Folder.get_by_guid(folder_guid)
            if del_folder:
                xconn = indexconnector()
                xsearch = fulltextsearch()
                del_folder.set_xapy_access(xconn, xsearch)
                try:
                    parent = del_folder.get_parent()

                    for search_obj in Search_object.get_all():
                        if search_obj.parent_guid == del_folder.guid:
                            Search.get_by_guid(search_obj.search_guid).delete()

                    del_folder.delete()
                    if parent:
                        parent.correct_status()
                except Exception, e:
                    msg = str(e)
                    flash(loc[msg] if msg in loc else msg, category=loc["hp_errtitle"])

    def delete_docshare(self, share_guid):
        loc = localization.get_lang()
        if self.user:
            del_doc_share = Document_share.get_by_guid(share_guid)
            if del_doc_share:
                try:
                    parent_guid = del_doc_share.get_document_share_parent(del_doc_share.guid)

                    for search_obj in Search_object.get_all():
                        if search_obj.object_guid == del_doc_share.guid:
                            Search.get_by_guid(search_obj.search_guid).delete()

                    del_doc_share.delete(skip_xapian=True)
                    parent_folder = Folder.get_by_guid(parent_guid)
                    if parent_folder:
                        parent_folder.correct_status()
                except Exception, e:
                    msg = str(e)
                    flash(loc[msg] if msg in loc else msg, category=loc["hp_errtitle"])

    def restore_folder(self, folder_guid, run_parent_recursion = True):
        loc = localization.get_lang()
        res_folder = Folder.get_by_guid(folder_guid)

        if res_folder:
            try:
                res_folder.set_deleted_false()

                # Restore inner folders
                for inn_folder in res_folder.get_children():
                    self.restore_folder(inn_folder.guid, False)

                # Restore document shares
                for doc_share in res_folder.get_document_shares():
                    if doc_share.deleted != 'false':
                        self.restore_docshare(doc_share.guid, False)

                if run_parent_recursion:
                    parent = res_folder.get_parent()
                    if parent:
                        parent.correct_status()
                # TODO
            except Exception, e:
                mess = "&mess=" + str(e)
                msg = str(e)
                flash(loc[msg] if msg in loc else msg, category=loc["hp_errtitle"])
                redirect("/recycle_bin_folder?guid=" + request.args["guid"] + mess)

    def restore_docshare(self, share_guid, run_parent_recursion = True):
        loc = localization.get_lang()
        res_share = Document_share.get_by_guid(share_guid)

        if res_share:
            try:
                res_share.set_deleted_false()

                # Set restored all files in folder
                documents = []
                xconn = indexconnector()
                xsearch = fulltextsearch()

                res_share.set_connector(xconn)
                res_share.set_search(xsearch)

                docsa, count = res_share.get_documents()
                for doc in docsa:
                    d_name = doc['db_id'][0]
                    documents.append(d_name)

                res_share.set_documents_restored(documents)

                # Recursively update status
                if run_parent_recursion:
                    parent_guid = res_share.get_document_share_parent(res_share.guid)
                    parent_folder = Folder.get_by_guid(parent_guid)
                    parent_folder.correct_status()
            except Exception, e:
                mess = "&mess=" + str(e)
                msg = str(e)
                flash(loc[msg] if msg in loc else msg, category=loc["hp_errtitle"])
                redirect("/recycle_bin_folder?guid=" + request.args["guid"] + mess)

    def operate_docshare(self, docshare):
        loc = localization.get_lang()
        doc_share = Document_share.get_by_guid(docshare)

        if doc_share:
            xconn = indexconnector()
            xsearch = fulltextsearch()
            doc_share.set_connector(xconn)
            doc_share.set_search(xsearch)
            try:
                if "task" in request.form:
                    documents = []
                    docsa = []
                    task = request.form["task"]

                    xconn = indexconnector()
                    xsearch = fulltextsearch()

                    doc_share.set_connector(xconn)
                    doc_share.set_search(xsearch)

                    if doc_share:
                        docsa, count = doc_share.get_documents()
                        for doc in docsa:
                            d_name = doc["db_id"][0]
                            if d_name in request.form:
                                documents.append(d_name)


                    if task == "del":
                        doc_share.delete_documents(documents)
                        has_deleted = False

                        # Check if doc share is partial, and we deleted all recycled files
                        if doc_share.is_partial_deleted():
                            docsa, count = doc_share.get_documents()
                            for doc in docsa:
                                if 'deleted' in doc:
                                    has_deleted = True
                            if not has_deleted:
                                doc_share.set_deleted_false()

                                # Tell folders about changes
                                parent_guid = doc_share.get_document_share_parent(doc_share.guid)
                                parent_folder = Folder.get_by_guid(parent_guid)
                                parent_folder.correct_status()

                    elif task == "res":
                        doc_share.set_documents_restored(documents)
                        has_deleted = False
                        docsa, count = doc_share.get_documents()

                        # Check if all files are restored, so ds->false, else partial
                        if doc_share.is_deleted():
                            for doc in docsa:
                                if 'deleted' in doc:
                                    doc_share.set_deleted_partial()
                                    has_deleted = True
                                    break
                            if not has_deleted:
                                doc_share.set_deleted_false()
                        elif doc_share.is_partial_deleted():
                            for doc in docsa:
                                if 'deleted' in doc:
                                    has_deleted = True
                                    break
                            if not has_deleted:
                                doc_share.set_deleted_false()

                        # Tell folders about changes
                        parent_guid = doc_share.get_document_share_parent(doc_share.guid)
                        parent_folder = Folder.get_by_guid(parent_guid)
                        parent_folder.correct_status()
            except Exception, e:
                # raise
                flash(str(e), loc["hp_errtitle"])


js_helper = """<script type="text/javascript">
function show_delete(ev, del_guid, del_folder)
{
	var d = jQuery("#delete_folder");
        d.modal('show');
	d.find("h3").html(ev.getAttribute("msgtitle"));
	d.find("#delete_folder_msg").html(ev.getAttribute("msgdescr"));
	if (typeof(del_guid) !== undefined) {
	    $('#del_share').attr('value', del_guid);
    }
	if (typeof(del_folder) !== undefined) {
	    $('#del_folder').attr('value', del_folder);
	}
};
function show_restore(ev, res_guid, res_folder)
{
	var d = jQuery("#restore_folder");
        d.modal('show');
    d.find("h3").html(ev.getAttribute("msgtitle"));
	d.find("#restore_folder_msg").html(ev.getAttribute("msgdescr"));
	if (typeof(res_guid) !== undefined) {
	    $('#res_share').attr('value', res_guid);
    }
	if (typeof(res_folder) !== undefined) {
	    $('#res_folder').attr('value', res_folder);
	}
};
</script>
%(delete_dialog)s
"""