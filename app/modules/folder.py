# -*- coding: utf-8 -*-
from ..lib.class_ACL import ACL
from ..lib.class_user import User
from ..lib.class_folder import Folder
from ..lib.html_wrapper import Html_wrapper
from ..lib.class_document_share import Document_share
from ..lib.class_ACL_proxy import rules
from ..lib import utils_old as utils_old
#from ..lib.file_access import share
from ..lib.class_notification import Notification
from flask import redirect, request,flash
from ..lib import localization as localization
import cgi
from ..lib import dialogs
'''
if False:

    args = request.arguments

    if "mess" in args:
        message = args["mess"]
        self.growl2.title = loc["hp_errtitle"]
        self.growl2.text = loc[message] if message in loc else message # DOCUMENT_SHARE_IS_NOT_EMPTY
        self.growl2.active = "1"

    if user:
        mess = ""




    else:
        back_url = Html_wrapper.get_logout_back_url()
        redirect("/home_page?back_url=" + back_url)
'''

class folder():
    def __init__(self, guid):
        from ..lib.xappy_fulltextsearch import fulltextsearch
        from ..lib.xappy_indexconnector import indexconnector

        self.xconn = indexconnector()
        self.xsearch = fulltextsearch()
        self.user = User.current()
        self.user_is_admin = self.user.can(rules["ADMIN"], "0")#application.id
        self.guid = guid
        self.folder = Folder.get_by_guid(self.guid)


    def _set_parents_ACL(self, object):
        for acl in ACL.get_by_object(self.folder.guid):
            new_acl = ACL()
            new_acl.subject = acl.subject
            new_acl.object = object.guid
            new_acl.rule = acl.rule
            new_acl.save()

    def del_folder(self, folder_guid, run_parent_recursion = True):
        from ..lib.xappy_fulltextsearch import fulltextsearch
        from ..lib.xappy_indexconnector import indexconnector

        loc = localization.get_lang()
        if self.folder and self.user.can(rules["READ"], self.folder.guid):
            del_folder = Folder.get_by_guid(folder_guid)
            if del_folder:
                xconn = indexconnector()
                xsearch = fulltextsearch()
                del_folder.set_xapy_access(xconn, xsearch)
                try:
                    del_folder.set_deleted_true()
                    if run_parent_recursion:
                        del_folder.set_parents_partial_deleted()

                    # Delete inner folders
                    for inn_folder in del_folder.get_children():
                        self.del_folder(inn_folder.guid, False)

                    # Delete document shares
                    for doc_share in del_folder.get_document_shares():
                        if doc_share.deleted != 'true':
                            self.del_doc_share(doc_share.guid, False)
                except Exception, e:
                    mess = "&mess=" + str(e)
                    msg = str(e)
                    flash(loc[msg] if msg in loc else msg, category=loc["hp_errtitle"])
                    redirect("/folder?guid=" + self.folder.guid + mess)

    def new_folder(self, name):
        if self.folder and self.user.can(rules["READ"], self.folder.guid) and self.user.can(rules["USER_ADMIN"],
                                                                                            self.folder.guid):
            #new_folder_name = name

            new_folder = Folder()
            new_folder.name = name #new_folder_name
            new_folder.parent_guid = self.folder.guid
            new_folder.save()
            if "setacl" in request.form:
                self._set_parents_ACL(new_folder)
            if not self.user_is_admin:
                self.user.add_access_to(new_folder, ACL.rules["USER_ADMIN"])

            redirect("/folder?guid=" + self.folder.guid)    #TODO:no need for redirect, just reinit widget

    def new_share(self, name):
        if self.folder and self.user.can(rules["READ"], self.folder.guid) and self.user.can(rules["USER_ADMIN"],
                                                                                            self.folder.guid):
            #new_share = name
            doc_share = Document_share()
            doc_share.name = name #new_share
            doc_share.create()

            if doc_share:
                self.folder.add_document_share(doc_share, self.user)
                if "setacl" in request.form:
                    self._set_parents_ACL(doc_share)

                if "setreminder" in request.form:
                    doc_share.toggle_subscribe()

                if not self.user_is_admin:
                    self.user.add_access_to(doc_share, ACL.rules["USER_ADMIN"])

                #redirect("/folder?guid=" + self.folder.guid)
                redirect("/document_share_edit?guid=" + doc_share.guid+"&parent="+self.folder.guid)

    def acl_type(self):
        utils_old.execute_ACL(args, self.folder)

    def del_doc_share(self, del_doc_share, run_parent_recursion = True):
        from ..lib.xappy_fulltextsearch import fulltextsearch
        from ..lib.xappy_indexconnector import indexconnector

        loc = localization.get_lang()
        if self.folder and self.user.can(rules["READ"], self.folder.guid):
            del_share = Document_share.get_by_guid(del_doc_share)

            if del_share:
                try:
                    Notification.send_event("delete_ds", User.current(), del_share)
                    del_share.set_deleted_true()

                    # Set deleted all files in folder
                    documents = []
                    xconn = indexconnector()
                    xsearch = fulltextsearch()

                    del_share.set_connector(xconn)
                    del_share.set_search(xsearch)
                    docsa, count = del_share.get_documents()
                    for doc in docsa:
                        d_name = doc['db_id'][0]
                        documents.append(d_name)
                    del_share.set_documents_deleted(documents)

                    # Set parent folders deleted as partial
                    if run_parent_recursion:
                        parent_guid = del_share.get_document_share_parent(del_share.guid)
                        parent_folder = Folder.get_by_guid(parent_guid)
                        parent_folder.set_parents_partial_deleted()

                except Exception, e:
                    mess = "&mess=" + str(e)
                    msg = str(e)
                    flash(loc[msg] if msg in loc else msg, category=loc["hp_errtitle"])
                    redirect("/folder?guid=" + self.folder.guid + mess)
            redirect("/documents_share")#+self.folder.guid)



    def render(self):
        #xconn = self.xconn
        #xsearch = self.xsearch

        loc = localization.get_lang()

        self.title = loc["header_hgmb"] + " &raquo; " + loc["header_f"]
        if not self.folder:
            flash(loc["folder_not_found"], category=loc["hp_errtitle"])
            return
        if self.folder.deleted == 'true':
            flash(loc["folder_deleted"], category=loc["hp_errtitle"])
        #obj = "0bf6b912-d50b-44ae-af6e-dc0960a02cb9"
        #subj = "9da9f8c2-e4d1-4c47-8bfa-d5f10ef81466"
        #raise Exception(str([str(s) for s in ACL_record.get_acls(subj, obj)]))


        user_has_admin_access = self.user.can(rules["USER_ADMIN"], self.folder.guid)

        if self.folder.deleted == 'true':
            edit = ""
        else:
            edit = u"""/folder_edit?guid=%(guid)s""" % {"guid": self.folder.guid} if user_has_admin_access else ""
        back_url = Html_wrapper.get_back_url(self.folder.get_parent())
        title = Html_wrapper.get_title(cgi.escape(self.folder.name), back_url, edit, self.folder.guid)
        self.hpt_title = title        


        #search_form = Html_wrapper.get_search_form(self.folder.guid)

        #custom_search_link = u"""<br /><a href="/custom_search_edit?guid=%(guid)s">%(s_custom)s</a>
        #                """ % {"guid": self.folder.guid, "s_custom": loc["s_custom"]}

        #self.hpt_search_form = search_form + custom_search_link

        self.hpt_data = Html_wrapper.get_ds_subcatalog(self.user, self.folder)
        self.hpt_data += js_helper % {"delete_dialog": dialogs.delete_folder()}

js_helper = """<script type="text/javascript">
function show_delete(ev, del_guid, del_folder)
{
    console.log($(this));
	var d = jQuery("#delete_folder");
        d.modal('show');
	d.find("h3").html(ev.getAttribute("msgtitle"));
	d.find("#delete_folder_msg").html(ev.getAttribute("msgdescr"));
	if (typeof(del_guid) !== undefined) {
	    $('#del_share').attr('value', del_guid);
    }
	if (typeof(del_folder) !== undefined) {
	    $('#del_folder').attr('value', del_folder);
	}
};
</script>
%(delete_dialog)s
"""

