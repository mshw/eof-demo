import cgi
from io import StringIO
from ..lib.class_user import User
from ..lib.html_wrapper import Html_wrapper
from ..lib.class_ACL_proxy import rules
from ..lib.helper import update_xapian_after_ssf_changed
from ..lib import localization as localization
from ..lib.class_smart_folder import SmartFolder, SmartSubFolder
from ..lib import dialogs
from  ..lib import utils_old
from flask import flash
def popup_growl(title, message):
    pass
        #self.growl.title = loc[title] if title in loc else title
        #self.growl.text = loc[message] if message in loc else message
        #self.growl.active = "1"

class smart_folder(object):
    def __init__(self, folder):
        self.smart_folder = SmartFolder.get_by_guid(folder)
        self.user = User.current()

    def check(self):
        return self.smart_folder

    def new_folder(self,new_folder_name):
        new_sub = SmartSubFolder()
        new_sub.name = cgi.escape(new_folder_name)
        new_sub.parent_guid = self.smart_folder.guid
        try:
            new_sub.save()
        except Exception, e:
            loc = localization.get_lang()
            flash(str(e),loc["hp_errtitle"])	

    def delete(self,delete_guid):
        del_sub_guid = delete_guid
        del_sub = SmartSubFolder.get_by_guid(del_sub_guid)
        if del_sub:
            update_xapian_after_ssf_changed(del_sub.guid, self.smart_folder, op="delete")
            del_sub.delete()
            #response.redirect("/smart_folder?folder=%s"%smart_folder.guid)	

    def folder_rename(self, subfolder, folder_new_name):
        ssf = SmartSubFolder.get_by_guid(subfolder)
        if ssf:
            new_name = cgi.escape(folder_new_name)
            old_name = ssf.name
            ssf.name = new_name
            update_xapian_after_ssf_changed(ssf.guid, self.smart_folder, new_name)
            ssf.save()	
            
    def rename(self, new_name):
        if self.user.can(self.user.rules["USER_ADMIN"], self.smart_folder.guid):
            self.smart_folder.name = cgi.escape(new_name)
            self.smart_folder.save()
            
    def execute_acl(self,args):
        redirect = "smart_share_edit?share=" +self.smart_folder.guid
        utils_old.execute_ACL(args, self.smart_folder, redirect)  
        
    def render_edit(self):
        loc = localization.get_lang()
        url = "/smart_share_edit"
        share_url = """<a href='smart_folder?folder=%(guid)s'>%(name)s</a>
                                        """ %{"name":cgi.escape(self.smart_folder.name), "guid":self.smart_folder.guid}

        back_url = """ / <a href="/file_server">%(fs_title)s</a>""" % {
            "fs_title" : loc["fs_title"] }
        self.hpt_title =  Html_wrapper.get_title(share_url , back_url)

        self.hpt_data = Html_wrapper.get_folder_rename_form(self.smart_folder,"smart_folder_edit")+\
            Html_wrapper.get_users_form(self.smart_folder, "smart_folder_edit")+Html_wrapper.get_groups_form(self.smart_folder, "smart_folder_edit")        
        self.title = loc["header_hgmb"] + " &raquo; " + loc["header_fshe"]    
        
    def render(self):
        loc  = localization.get_lang()
        self.title = loc["header_hgmb"] + " &raquo; " + loc["header_smart"]



        new_folder_form = u""
        if self.user.can(self.user.rules["WRITE"], self.smart_folder.guid):
            new_folder_form = res = dialogs.add_sf_subfolder()

        sub_folders = self.smart_folder.sub_folders

        subs_template 		= u"<br/><br/><ul>%s</ul>"
        sub_folder_template = u"""<li><a href="/search?smart=%(guid)s">%(title)s</a><span class="project_badge red iconsweet"> %(edit)s%(delete)s</span></li>"""
        del_url = edit_url = u""
        if self.user.can(self.user.rules["WRITE"], self.smart_folder.guid):
            #del_url = u"""
            #    <a  class='adelete' title='%(delete)s' style="cursor: pointer"
            #    href='/smart_folder?folder=%(parent_guid)s&delete=%(guid)s' onclick="return xdel();">X</a>"""
            del_url = u""" <a class='edit' title='%(delete)s' style="cursor: pointer" msgtitle='%(msgtitle)s' msgdescr='%(msgdescr)s'
                            onclick='return show_delete(this, \"%(parent_guid)s\", \"%(guid)s\")' >X</a>"""
            edit_url = u""" <a class='edit' title='%(edit)s' style="cursor: pointer"
                onclick='return show_rename(\"%(parent_guid)s\", \"%(guid)s\",\"%(title)s\")' >8</a>"""

        if sub_folders:
            subs = subs_template % "".join([
                sub_folder_template % {
                    "title"	: cgi.escape(sub.name),
                    "guid" 	: sub.guid	,
                    "delete": del_url 	% {	"guid":sub.guid, "delete" 	: loc["delete"], "parent_guid" : self.smart_folder.guid,
                                            "title" : cgi.escape(sub.name), "msgtitle":loc["fs_del_smart_title"], "msgdescr": loc["fs_del_subsmart_descr"]},
                    "edit" 	: edit_url 	% {	"guid":sub.guid, "edit" 	: loc["edit"]  , "parent_guid" : self.smart_folder.guid, "title" : cgi.escape(sub.name)}

                    } for sub in sub_folders
            ])
        else:
            subs = u"<br/><br/><br/> %s." % loc["fs_no_subfolders"]

        folder_edit_url = u""
        if self.user.can(self.user.rules["USER_ADMIN"], self.smart_folder.guid):
            folder_edit_url = u"""/smart_folder_edit?guid=%(guid)s"""%{
                "guid":self.smart_folder.guid, "edit" : loc["edit"]}
        result = StringIO()
        back_url = """ / <a href="/file_server">%(fs_title)s</a>""" % {
            "fs_title" : loc["fs_title"] }        
        self.hpt_title = Html_wrapper.get_title(u"""%s -
                                                <a href='/smart_folder?folder=%s'>%s</a>""" % (loc["header_smart"], self.smart_folder.guid, cgi.escape(self.smart_folder.name)),back_url, folder_edit_url )

        #raise Exception(str(new_folder_form).replace("<", "&lt;"))

        result.write(new_folder_form)
        result.write(subs)

        result.write(js_helper % {
            "delete"			: loc["delete"],
            "edit"				: loc["edit"],
            "cancel"			: loc["cancel"],
            "uag_delete_confirm": loc["uag_delete_confirm"],
            "uag_adduser" 		: loc["uag_adduser"],
            "uag_email" 		: loc["uag_email"],
            "uag_pass"			: loc["uag_pass"],
            "uag_confirm" 		: loc["uag_confirm"],
            "create" 			: loc["create"],
            "uag_newgroup"		: loc["uag_newgroup"],
            "fs_del_sub"		: loc["fs_del_sub"],
            "fs_ren_sub"		: loc["fs_ren_sub"],
            "rename_dialog":dialogs.rename_smartfolder(),
            "delete_dialog":dialogs.delete_smartfolder(),

        })

        self.hpt_data = result.getvalue()

        result.close()	


js_helper = """<script type="text/javascript">
function xdel(m) {
	s = m ? m : "%(uag_delete_confirm)s";
	if (confirm(s) == false) return false;
	return true;
	}
function confirmation(parent_guid, guid, email)
{
	jQuery("#confirm").dialog();
	jQuery("#confirm").dialog('destroy');
	$('#confirm').attr('title', '%(fs_del_sub)s "'+email+'"');
	$('#folder').attr('value', parent_guid);
	$('#delete').attr('value', guid);

	jQuery("#confirm").dialog({
		modal: true, resizable: false, width: 350,
		buttons: {
		  %(delete)s: function() {
			$('#delete_confirm').submit();
		  },
		  %(cancel)s: function() {
			jQuery(this).dialog("close");
		  }
    }
  });

};

function show_rename(parent_guid, guid, name)
{
	var d = jQuery("#rename_smartfolder");
        d.modal('show');
	$('#folder').attr('value', parent_guid);
	$('#rename').attr('value', guid);
	$('#folder_rename').attr('value', name);
};

function show_delete(ev, parent_guid, guid)
{
	var d = jQuery("#delete_smartfolder");
        d.modal('show');
    d.find("h3").html(ev.getAttribute("msgtitle"));
	d.find("#delete_folder_msg").html(ev.getAttribute("msgdescr"));
	$('#folder').attr('value', parent_guid);
	$('#delete').attr('value', guid);
};


</script>
%(rename_dialog)s
%(delete_dialog)s"""