from ..lib.class_user import User
from ..lib.class_group import Group
from ..lib.html_wrapper import Html_wrapper
from ..lib.class_document_share import Document_share
from ..lib import localization as local
from flask import redirect, url_for
from cgi import escape
from flask import flash,request
from hashlib import md5

group_form = ""
res = ""


class user():

    def __init__(self, guid):
        self.user = User.current()
        self.user_profile = User.get_by_guid(guid) if self.user.is_admin() or guid == self.user.guid else None

    def check(self):
        return bool(self.user and self.user_profile)
    
    def add_to_group(self, guid):
        self.user_profile.add_to_group(guid)

    def delete_from_group(self, guid):
        self.user_profile.delete_from_group(guid)
    
    def update_user(self, args):
        loc = local.get_lang()
        oldemail = self.user_profile.email
        user = User.get_by_guid(args.get('user_id'))
        user.first_name = escape(args.get('first_name', ""))
        user.last_name 	= escape(args.get('last_name', ""))
        user.email = escape(args.get('email', ""))
        user.notify_mode = escape(args.get('notify_mode', ""))
        
        # login and keywords will be updated if current user is administrator
        if self.user.is_admin():
            user.login = escape(args.get('login', ""))
            user.keywords = escape(args.get('keywords', ""))

        # password will be updated if not null and password==confirm
        new_pass = False
        if args.get('newpass', "") == args.get('confirm', "") != "" and args.get('newpass', "")[0] != "*":
            user.password = md5(args.get("newpass").encode("utf8")).hexdigest()
            new_pass = True
        try:
            if args.get("del_logo") == "true":
                user.set_logo(None)
            elif "logo" in request.files and request.files["logo"].filename:
                ext = request.files["logo"].filename.rsplit('.', 1)[1].lower()
                if ext in ('jpg', 'jpeg', 'png'):
                    user.set_logo(request.files["logo"])
        except Exception, e:
            flash(str(e), loc["hp_errtitle"] + " " + user.login)
        
        try:
            user.save()
            self.user_profile = user
            flash(user.login + "<br/>" + loc["HAS_BEEN_UPDATED"] + ("<br/><br/><b>%s</b>" % loc["APPLY_CHANGES_TO_P2W_D2W"] if new_pass else ""), loc["header_u"])
        except Exception, e:
            flash(str(e), loc["hp_errtitle"]+" "+user.login)

    def render(self):
        loc = local.get_lang()
        self.title = loc["header_hgmb"] + " &raquo; " + loc["header_u"]
        back_url = """/<a href="/users_and_groups">%(uag_title)s</a>""" % {"uag_title": loc["uag_title"]} if self.user.is_admin() else ""
        self.hpt_title = Html_wrapper.get_title("%s: %s" % (loc["header_u"], self.user_profile.name), back_url)
        self.hpt_data = Html_wrapper.get_admin_user(self.user, self.user_profile)
