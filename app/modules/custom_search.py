from ..lib.class_user import User
from ..lib.class_folder import Folder
from ..lib.class_search import Search, Search_object
from ..lib.html_wrapper import Html_wrapper
from ..lib.class_document_share import Document_share
from ..lib.class_metafield import Expression
from ..lib import localization as localization

from ..lib import utils_old
from ..lib.xappy_fulltextsearch import fulltextsearch
from ..lib.xappy_indexconnector import indexconnector
from ..lib import dialogs

from flask import request, redirect, flash
import json

class custom_search():
    def __init__(self,guid):
        self.user = User.current()
        xconn = indexconnector()
        xsearch = fulltextsearch()             
        self.search =Search.get_by_guid(guid)
        
    def render(self):
        loc = localization.get_lang()     
        self.title = loc["header_hgmb"] + ' &raquo; ' + loc["header_cs"]
        search = self.search
        
        if search:
            edit=""
            if self.user.has_write_access_into(search):
                edit = u"""/custom_search_acl?guid=%(guid)s""" %{
                                "guid":search.guid,
                                "edit":loc["edit"]
                            } if self.user.has_admin_access_into(search) else ""
                #if "edit" in args:
                #    s_object = search.get_objects()[0]
                #    #raise Exception(str(s_object))
                #    share_guid = "guid="+s_object.object_guid
                #    parent_guid = s_object.parent_guid and "&parent="+s_object.parent_guid or ""
                #    search_guid = "search_guid="+search.guid
                #    response.redirect("/Custom_search_edit?%s&%s%s" % (
                #        share_guid, search_guid, parent_guid)
                #                      )
    
            self.hpt_title = Html_wrapper.get_title(loc["s_custom"]+": "+search.name, "",editurl=edit,icon='e')
            obj = Search_object.get_by_search_guid(search.guid)
            tip=''
            obj_list=[]
            if obj:
                obj = obj[0]
                doc_share = Document_share.get_by_guid(obj.object_guid)
                if doc_share:
                    share_url = """<a href='document_share?guid=%(guid)s&parent=%(parent)s'>%(name)s</a>
                            """%{"guid":doc_share.guid, "parent":obj.parent_guid, "name":doc_share.name } 
                    obj_list.append(share_url)
                    while obj.parent_guid:
                        folder = Folder.get_by_guid(obj.parent_guid)
                        folder_url =  """<a href='folder?guid=%(guid)s'>%(name)s</a>
                                """%{"guid":folder.guid, "name":folder.name }                        
                        obj_list.append(folder_url)
                        obj = folder
                else:
                    obj = Folder.get_by_guid(obj.object_guid)
                    folder_url =  """<a href='folder?guid=%(guid)s'>%(name)s</a>
                            """%{"guid":folder.guid, "name":folder.name }                  
                    obj_list.append(folder_url)
                    while obj.parent_guid:
                        folder = Folder.get_by_guid(obj.parent_guid)
                        folder_url =  """<a href='folder?guid=%(guid)s'>%(name)s</a>
                                """%{"guid":folder.guid, "name":folder.name }                       
                        obj_list.append(folder_url)
                        obj = folder  
            s = " / ";
            tip = s.join(obj_list)            
            self.hpt_title += " &emsp;&rArr;&emsp; "+ tip #'header_f'-Folder
            self.hpt_title += u"""/  <a href="/documents_share">%(ds_title)s</a>  """ % {
                "ds_title": loc["ds_title"] #Documents share
            }    
                
            self.hpt_data = Html_wrapper.get_custom_search_info(search)
            xconn = indexconnector()
            xsearch = fulltextsearch()                  
            self.hpt_data += Html_wrapper.get_custom_search_result(search, xsearch, xconn )
            self.hpt_data +=dialogs.searchresult()
            
            
  