from ..lib.class_sharing import Sharing
from ..lib.class_document_share import Document_share
from ..lib.class_share_dir import ShareDir, ShareFile
from ..lib.class_ACL import ACL
from ..lib.class_user import User
from ..lib.xappy_fulltextsearch import fulltextsearch
from ..lib.xappy_indexconnector import indexconnector
from datetime import datetime
from ..lib.scheduler import Scheduler

class sharing():

    def __init__(self):
        self.share = Sharing()

    def operate(self, form):
        # Format time to object
        del_date_str = form["del_date_picker"] + " " + form["del_time_picker"]
        if "-" in del_date_str:
            date_format = '%d-%m-%Y %H:%M'
        else:
            date_format = self.share.date_format
        self.share.del_date = datetime.strptime(del_date_str, date_format)

        # Set documents to dictionary

        doc_share = Document_share.get_by_guid(form["docshare"])
        if doc_share:
            self.share.dshare_guid = doc_share.guid
            xconn = indexconnector()
            xsearch = fulltextsearch()

            doc_share.set_connector(xconn)
            doc_share.set_search(xsearch)

            docsa, count = doc_share.get_documents()
            for doc in docsa:
                d_name = doc["db_id"][0]
                if d_name in form:
                    self.share.documents.append(d_name)

        guid = self.share.save()

        # Get users emails to send them link with share
        if form["task"] == "share_email" and doc_share:
            users_rules = ACL.get_by_object(doc_share.guid)

            for acl in users_rules:
                user_ = User.get_by_guid(acl.subject)
                if user_.email in form:
                    self.share.send_share_by_email(user_.email)
            if "custom_email" in form:
                self.share.send_share_by_email(form["custom_email"])


        # Register in scheduler
        Scheduler.register_link(self.share)
        return guid

    def operate_fs(self, form):
        # Format time to object
        del_date_str = form["del_date_picker"] + " " + form["del_time_picker"]
        self.share.del_date = datetime.strptime(del_date_str, self.share.date_format)

        # Set documents to dictionary

        guid_list = form.getlist('g')
        for doc_guid in guid_list:
            self.share.documents.append(doc_guid)
        self.share.type = "fs"
        self.share.dshare_guid = form["file_share_guid"]
        guid = self.share.save()

        # Register in scheduler
        Scheduler.register_link(self.share)
        return guid

    def send_by_email(self, form):
            self.share = Sharing.get_by_guid(form["sharing"])
            self.share.send_share_by_email(form["destination"])

    def download(self, guid):
        self.share = Sharing.get_by_guid(guid)

        if self.share and self.share.guid:

            # Get all files and archive them
            from flask import send_file

            if self.share.type == "fs":
                shareDir = ShareDir.get_by_guid(self.share.dshare_guid)
                if shareDir:
                    return shareDir.send_files(self.share.documents)
                else:
                    return None
            else:
                zip_filename = self.share.zip_documents_to_file()
                name = "%s.zip" % self.share.guid.encode("utf8")

                ret = send_file(zip_filename,
                          mimetype="application/zip",
                          as_attachment=True,
                          attachment_filename=name)
                # ret.headers.add('Content-Disposition', 'attachment')
                return ret
        else:
            return None