from ..lib.class_user import User
from ..lib.html_wrapper import Html_wrapper
from ..lib.helper import setDevMode, isDevMode, has_wf_licence
from ..lib import localization

from flask import redirect, url_for

class administration():
    def __init__(self):
        loc = localization.get_lang()
        self.menu = u""" <a href="/info">%(header_info)s<br/><br/></a>
<a href="/users_and_groups">%(admin_uag)s</a><br><br>
<a href="/documents_share">%(admin_ds)s</a><br><br>
<a href="/file_server">%(admin_fs)s</a><br><br/>
<a href="/searches">%(admin_cs)s</a><br><br/>
<a href="/tree_editing">%(header_map_edit)s</a><br><br/>
<a href="/import_data">%(header_migration)s<br/><br/></a>
<a href="/import_ldap">%(header_ldap)s<br/><br/></a>
<a href="/network">%(header_netconfig)s<br/><br/></a>
<a href="/recycle_bin">%(header_rb)s<br/><br/></a>
<a href="/tree_and_rights">%(header_rtree)s<br/><br/></a>
 """ % {
"admin_uag": loc["admin_uag"],
"admin_ds": loc["admin_ds"],
"admin_fs": loc["admin_fs"],
"admin_cs": loc["admin_cs"],
"header_map_edit": loc["header_map_edit"],
"header_ldap" : loc["header_ldap"],
"header_netconfig":loc["header_netconfig"],
"header_migration" : loc["header_migration"],
"header_info":loc["info_header"],
"header_rb": loc["header_rb"],
"header_rtree": loc["header_rtree"],
}

    def render(self):
        loc = localization.get_lang()
        user = User.current()        
        self.title = loc["header_hgmb"] + " &raquo; " + loc["header_admin"]


        self.hpt_title = Html_wrapper.get_title(loc["admin_header"]) #+ self.menu

        self.hpt_data = self.menu
        #change_mode_to = args.get("mode", castto=int)
        #if change_mode_to in [0,1]:
        #    setDevMode(change_mode_to)

        #self.hpt_menu.htmlcode += "<h1>%s</h1>"%isDevMode()

    
        