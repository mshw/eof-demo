from ..lib.html_wrapper import Html_wrapper
from ..lib.class_user import User
from ..lib import localization as local
from ..lib.utils_migration import export_file, import_file, export_document_shares,delete_import_file
from ..lib.scheduler import Scheduler, BackupTask
from flask import flash,redirect
import traceback,tempfile,os,logging,time,json #debug only

class importexport():
    def __init__(self):
        self.user = User.current()
        
    def get_backup(self):
        if self.user and self.user.is_admin():
            return export_file()    
    def get_doc_export(self):
        if self.user and self.user.is_admin(): 
            return export_document_shares()
    def add_schedule(self,mode,interval,unit,time,rotation,destination=None,login="",passw="",remoteserver=""):
        if int(interval)>0 and mode in ("fulllocal","fullssh","fullsmb") and unit in ("weeks","days","monday","tuesday","wednesday","thursday","friday","saturday","sunday","hours"):
            task = BackupTask()
            task.interval = int(interval)
            task.mode = mode
            task.at_time = time
            task.unit = unit
            task.rotation = rotation
            dest = "%s%s@%s"%(login, ":%s"%passw if passw else "",destination) if login else destination
            if remoteserver:
                task.destination = json.dumps({"server":"%s%s@%s"%(login, ":%s"%passw if passw else "",remoteserver) if login else remoteserver,"path":destination})
            else:
                task.destination = "%s%s@%s"%(login, ":%s"%passw if passw else "",destination) if login else destination
            task.save()
            if task.test():
                Scheduler.register(task)
                return redirect("/import_data")
            else:
                loc = local.get_lang()
                task.delete()
                flash("Cannot access backup destination",loc["hp_errtitle"])
    def list_backups(self):
        return [(task.mode, "%s %s"%(task.interval,task.unit),task.at_time or "",task.rotation, task.status,task.last_backup,task.id,'<a href="?now=%s">Backup Now!</a>'%task.id if task._status !="Running" else "", task.id) for task in BackupTask.get_all()]
    
    def delete_task(self,taskid):
        task = BackupTask.get_by_id(taskid)
        if task:
            task.delete()
    def run_task(self,taskid):
        task = BackupTask.get_by_id(taskid)
        if task:
            task.run_now() 
            time.sleep(0.2)
            
    def restore(self, taskid,revision):
        try:
            logging.info("Restoring backup %s revision: %s started"%(taskid,revision))
            task = BackupTask.get_by_id(taskid)
            if task:
                return task.restore(revision)
        except:
            logging.exception("Error while restoring backup %s revision: %s"%(taskid,revision))
    def import_backup(self,data):
        err = ""
        import urllib
        #try:
            #print tempfile.gettempdir()
            #print os.path.abspath(tempfile.gettempdir())
        #except:pass
        if self.user and self.user.is_admin():
            try:
                import_file(data)
            except Exception as e:
                logging.exception("Error while loading backup")
                err = e
                #print traceback.format_exc()
                delete_import_file()
            if self.user:
                self.user.logoff()
                
            if err:
                print err
                #ajax call - returning just text message
                return urllib.quote_plus("Backup import error:%s"%err) 
            else:
                return "Backup imported!"
        
    def render_restore(self,backupid):
        loc = local.get_lang()
        self.title = loc["header_hgmb"] + " &raquo; " + loc['backup_title']
        back_url = """ / <a href="/import_data">%(backup_title)s</a>""" % {"backup_title" : loc["backup_title"]}
        self.hpt_title = Html_wrapper.get_title(loc.get('view_backups_title',loc["view_title"]), back_url)          
        task = BackupTask.get_by_id(backupid)
        if task:
            self.hpt_data = Html_wrapper.backup_list(self.user,task.list_revisions())

    def render(self):
        loc = local.get_lang()
        self.title = loc["header_hgmb"] + " &raquo; " + loc['backup_title']
        back_url = """ / <a href="/administration">%(admin_header)s</a>""" % {"admin_header" : loc["admin_header"]}
        self.hpt_title = Html_wrapper.get_title(loc['backup_title'], back_url)        
        self.hpt_data = Html_wrapper.get_importexport(self.user,self.list_backups())        