#Not used
from ..lib.class_user import User
#from ..lib.class_file_share import File_share
from xml.dom.minidom import parseString
import traceback
from uuid import uuid4
import base64
def run(data):
    err = "<error><code>%(code)s</code><description>%(description)s</description></error>"

    user = User.current()
    if user is None:
        err = err%{"code":"5", "description":"User is not logged"}
        return err

    if data:
        try:
            """
			<file>
			    <share_guid>guid</share_guid>
			    <path>/path/to/folder</path>
			    <filename>filename</filename>
			    <data>[base64 encoded contents of the file]</data>
			</file>

			"""

            dom_param = parseString(data.encode("UTF-8"))

            share_guid = dom_param.getElementsByTagName("share_guid")
            if share_guid and share_guid[0].firstChild:
                share_guid = share_guid[0].firstChild.nodeValue
                file_share = File_share.get_by_guid(share_guid)

                if file_share is None:
                    err = err%{"code":"7", "description":"file_share is None"}
                    return err

                elif user.has_write_access_into(file_share) == False:
                    err = err%{"code":"8", "description":"can not write into share"}
                    return err


            else:
                #return ("response","Error: Wrong in share_guid")
                err = err%{"code":"9", "description":"Wrong in share_guid"}
                return err


            path_0 = ""
            path = dom_param.getElementsByTagName("path")
            if path and path[0].firstChild:
                path_0 = path[0].firstChild.nodeValue
            else:
                #return ("response","Error: Wrong in sharepath")
                err = err%{"code":"10", "description":"Wrong in path"}
                return err

            filename = dom_param.getElementsByTagName("filename")
            if filename and filename[0].firstChild:
                filename = filename[0].firstChild.nodeValue
            else:
                #return ("response","Error: Wrong in filename")
                err = err%{"code":"11", "description":"Wrong in filename"}
                return err


            data = dom_param.getElementsByTagName("data")
            if data and data[0].firstChild:
                data=data[0].firstChild.nodeValue

                bb=str(data)
                file_data = base64.b64decode(bb)

                if file_share.add_file(path_0, filename, file_data):
                    return "<result>ok</result>"
                else:
                    err = err%{"code":"12", "description":"Wrong in filename"}
                    returnerr
            else:
                err = err%{"code":"13", "description":"Wrong in data"}
                return err
        except Exception, e:
            er = traceback.format_exc()
            err = err%{"code":"0", "description":"Server error:"+str(er)}
            return err		
    else:
        #return ("response","Error: xml_data is absent")
        err = err%{"code":"14", "description":"xml_data is absent"}
        return err

