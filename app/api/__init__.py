from ..lib.helper import xml_error_response

import DS_create_folder,DS_create_share,DS_list,DS_put_document,DS_share_info,DS_tree
import FS_list_shares,FS_ssf_create
import log_in, set_acl,get_acl, get_user_guid, create_user,clear_all
modules = [DS_create_folder,
         DS_create_share,
         DS_list,
         DS_put_document,
         DS_share_info,
         DS_tree,
         FS_list_shares,
         FS_ssf_create,
         log_in, 
         set_acl,
         get_acl, 
         get_user_guid, 
         create_user,
         clear_all]
actions={m.__name__.split('.')[-1]:m.run for m in modules}

def call_action(name, data):
    if name in actions:
        return actions[name](data)
    else:
        return xml_error_response("99")
        