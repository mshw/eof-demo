from ..lib.class_user import User
from xml.dom.minidom import parseString
import traceback
def run(data):
    User.logoff()

    err = "<error><code>%(code)s</code><description>%(description)s</description></error>"

    if data:

        try:

            dom_param = parseString(data.encode("utf8"))

            log = pwd = easter_key = ""
            login = dom_param.getElementsByTagName("login")
            if login and login[0].firstChild:
                log = login[0].firstChild.nodeValue

            password = dom_param.getElementsByTagName("password")
            if password and password[0].firstChild:
                pwd = password[0].firstChild.nodeValue


            #parameter added for powerpack
            easteregg = dom_param.getElementsByTagName("easteregg")
            if easteregg and easteregg[0].firstChild:
                easter_key = easteregg[0].firstChild.nodeValue

    #		user = User.login_hashed(log, pwd)
            if easter_key and easter_key == u"yes":
                user = User.log_in(log, pwd)
            else:
                user = User.login_hashed(log, pwd)

            if user:
                res = """<user><guid>%(guid)s</guid><login>%(login)s</login><photo_url>%(photo_url)s</photo_url></user>
					        """%{"guid":user.guid, "login":user.email, "photo_url":user.picture}
                return res
            else:
                return err%{"code":"1", "description":"Wrong login or password"}
        except Exception, e:
            er = traceback.format_exc()
            debug("Error: "+  str(er ))

            return err%{"code":"0", "description":"Server error:"+str(er)}

    else:
        return err%{"code":"14", "description":"xml_data is absent"}
