from ..lib.class_folder import Folder
from ..lib.class_user import User
from xml.dom.minidom import parseString
import traceback

def run(data):
    err = "<error><code>%(code)s</code><description>%(description)s</description></error>"
    res = ""

    if data:
        try:
            user = User.current()
            if user:
                dom_param = parseString(data.encode("UTF-8"))
                """P
				<xml>
					<guid>guid</guid>
				</xml>
				"""

                new_name =  ""
                res += "<path>"
                guid = dom_param.getElementsByTagName("guid")
                if guid and guid[0].firstChild:
                    folder_guid = guid[0].firstChild.nodeValue
                    folder = Folder.get_by_guid(folder_guid)
                    if folder:
                        if folder.deleted != "true":
                            for a_folder in user.get_subfolders(folder):
                                if a_folder.deleted != "true":
                                    res += """<folder><guid>%(guid)s</guid><name><!%(cdata)s[CDATA[%(name)s]%(cdata)s]></name></folder>"""%{"guid":a_folder.guid, "name":a_folder.name, "cdata":u""}

                            for doc_share in user.get_documents_shares(folder):
                                if doc_share.deleted != "true":
                                    res += """<document_share><guid>%(guid)s</guid><name><!%(cdata)s[CDATA[%(name)s]%(cdata)s]></name></document_share>"""%{"guid":doc_share.guid, "name":doc_share.name, "cdata":u""}
                    else:
                        err = err%{"code":"21", "description":"Wrong folder guid"}
                        return err
                else:
                    for a_folder in  user.get_top_folders():
                        if a_folder.deleted != "true":
                            res += """<folder><guid>%(guid)s</guid><name><!%(cdata)s[CDATA[%(name)s]%(cdata)s]></name></folder>"""%{"guid":a_folder.guid, "name":a_folder.name, "cdata":u""}

                #self.__args["cdata"] = "" || <!%(cdata)s[CDATA[%(name)s]%(cdata)s]>

                res += "</path>"
                return res

            else:
                return err%{"code":"5", "description":"User is not logged"}

        except Exception, e:
            er = traceback.format_exc()
            return err%{"code":"0", "description":"Server error:"+str(er)}
    else:
        return err%{"code":"14", "description":"xml_data is absent"}
