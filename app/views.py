import json
import logging
import os
from functools import wraps
from time import time
from hashlib import md5
from app import app
from datetime import datetime
from flask import render_template, session, redirect, url_for, request, flash, send_file,safe_join, abort,make_response,after_this_request, jsonify
from config import CONFIG
from lib import localization
from lib.class_config import Config
from lib.class_user import User
from lib.dialogs import show_license
from lib.helper import get_logout_back_url
from lib.update import Update
from lib.utils_old import key_escape
from modules.documents_share import document_share
from modules.htp_js import htp_js
from modules.side_panel import get_hp,get_quicksearch, DOCUMENT_SHARE, FILE_SHARE,ADMINISTRATION, RECYCLE_BIN

def auth_dec(f ):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not User.current() or not (User.get_max_users() and User.get_max_documents()):
            # direct connection check
            if "user" in request.args and "hash" in request.args:
                if User.login_token(request.args["user"], request.args["hash"]):
                    return f(*args, **kwargs)
            return redirect("/home_page?back_url=%s" % get_logout_back_url())
        return f(*args, **kwargs)
    return decorated_function

@app.route('/')
def index():
    return redirect(url_for('home_page'))


@app.route('/administration', methods=['GET', 'POST'])
@auth_dec
def adminpage():
    from modules.administration import administration
    from modules.user import user
    cur_user = User.current()
    if cur_user.is_admin():
        page = administration()
    else:
        page = user(cur_user.guid)    
    if request.method == 'POST':
        if "user_id" in request.form and not cur_user.is_admin():
            page.update_user(request.form)        
    
    page.render()
    return render_template("temp.html", page=page, user=cur_user,hp=get_hp(ADMINISTRATION))

# @socketio.on('connected')
# def socket_user_connected():
#     from flask_socketio import join_room, leave_room, rooms
#
#
#     if 'user' in session:
#         username = session['user']
#         rooms = rooms()
#         room = username.guid
#         join_room(room)
#         print username.name + ' has entered the room: ' + room

@app.route('/tree_and_rights', methods=['GET', 'POST'])
@auth_dec
def tree_and_rights():
    from modules.tree_and_rights import tree_and_rights
    cur_user = User.current()
    page = tree_and_rights()
    if cur_user.is_admin():
        if request.method == 'POST':
            page.execute_acl(request.form)

            back_url = "/tree_and_rights?guid=%(guid)s" % {"guid": request.form["guid"]}
            if "parent_guid" in request.form:
                back_url += "&parent=" + request.form["parent_guid"]
            return redirect(back_url)
        else:
            if "del_acl" in request.args:
                page.execute_acl(request.args)

        page.render_admin()
    else:
        return redirect("/home_page")

    return render_template("temp.html", page=page, user=cur_user, hp=get_hp(ADMINISTRATION))

@app.route('/print_tree_and_rights', methods=['GET'])
@auth_dec
def print_tree_and_rights():
    from modules.tree_and_rights import tree_and_rights
    cur_user = User.current()
    page = tree_and_rights()
    if user:
        page.render_print()

    return render_template("print.html", page=page, user=cur_user, hp=get_hp(ADMINISTRATION))

@app.route('/ajax_load_notifications', methods=['POST'])
def ajax_load_notifications():
    user = User.current()
    import json;
    if user:
        from lib.class_notification import Notification
        notifications = Notification.get_by_user(user, True)
        dic = [{
                   "id": n.guid,
                    "title": n.message,
                    "date": n.ttl.strftime(n.date_format),
                    "readed": n.readed
               }
               for n in notifications]
        dic.append("OK")
        return json.dumps(dic)
    else:
        return json.dumps([{}, "OK"])

@app.route('/ajax_remove_notification', methods=['POST'])
def ajax_remove_notification():
    user = User.current()
    if user:
        guid = request.form["guid"]
        from lib.scheduler import reminderqueue
        reminderqueue.put(guid)

        return "success"

@app.route('/ajax_set_readed_notifications', methods=['POST'])
def ajax_set_readed_notifications():
    from lib.class_notification import Notification

    user = User.current()
    data = json.loads(request.form["data"])

    if user:
        Notification.set_readed(data)
    return "success"

@app.route('/ajax_toggle_acl_no_notify', methods=['POST'])
def ajax_toggle_acl_no_notify():
    user = User.current()
    if user:
        share_guid = request.form["share_guid"]
        from lib.class_document_share import Document_share
        doc_share = Document_share.get_by_guid(share_guid)

        if user.need_to_be_notified_about(doc_share):
            user.add_access_to(doc_share, "no_notify")
        else:
            user.delete_access_to(doc_share, "no_notify")
        return "success"

@app.route('/ajax_load_target_acl', methods=['POST'])
def ajax_load_target_acl():
    from lib.html_wrapper import Html_wrapper
    cur_user = User.current()

    if not cur_user:
        return "-1"

    res = ""
    form = request.form
    from lib.xappy_fulltextsearch import fulltextsearch
    from lib.xappy_indexconnector import indexconnector
    from lib.class_folder import Folder
    from lib.class_document_share import Document_share
    parameters = ""


    # Load all users for posted share or folder
    if form['target_type'] == 'DS':
        # Load DS ACL
        dshare = Document_share.get_by_guid(form["guid"])

        if form["parent_guid"]:
            parameters = "&parent=" + form["parent_guid"]

        res += Html_wrapper.get_users_form(dshare, "tree_and_rights", parameters, form["target_type"])
        res += Html_wrapper.get_groups_form(dshare, "tree_and_rights", parameters, form["target_type"])
        if not cur_user.is_admin():
            res += Html_wrapper.get_notify_changer_widjet(dshare)
    else:
        # Load Folder ACL
        folder = Folder.get_by_guid(form["guid"])

        res += Html_wrapper.get_users_form(folder, "tree_and_rights", parameters, form["target_type"])
        res += Html_wrapper.get_groups_form(folder, "tree_and_rights", parameters, form["target_type"])

    return res

@app.route('/ajax_sharing_save', methods=['POST'])
@auth_dec
def sharing_save():
    from modules.sharing import sharing
    from datetime import datetime
    from lib.class_document_share import Document_share
    cur_user = User.current()

    if cur_user:
        module = sharing()
    if "fstask" in request.form:
        if request.form["fstask"] == "share":
            # For FileShare
            guid = module.operate_fs(request.form)
            return request.url_root + "share?guid=" + guid
        if "fstask" in request.form and request.form["fstask"] == "sendbyemail":
            module.send_by_email(request.form)
            return "1"
    else:
        if request.form["task"] == "sendbyemail":
            module.send_by_email(request.form)
            return "1"
        else:
            guid = module.operate(request.form)
            if request.form["task"] == "share":
                return request.url_root + "share?guid=" + guid
            else:
                return "1"

@app.route('/share', methods=['GET', 'POST'])
def share():
    from modules.sharing import sharing
    from datetime import datetime
    from lib.class_document_share import Document_share

    if request.args["guid"]:
        module = sharing()
    else:
        return redirect("/home_page")

    ret = module.download(request.args["guid"])
    if ret != None:
        from threading import Timer

        @after_this_request
        def remove_file(response):
            import re
            d = response.headers['content-disposition']
            file_name = re.findall("filename=(.+)", d)
            file_path = os.path.join(os.getcwd(),
                                     CONFIG['TEMP-DIRECTORY'],
                                     file_name[0].encode("utf8"))
            try:
                os.remove(file_path)
            except:
                def remove_later(path):
                    try:
                        os.remove(path)
                    except:
                        pass

                t = Timer(90.0, remove_later, (file_path,))
                t.start()
            return response
    else:
        loc = localization.get_lang()
        msg = "sharing_expired_missing"
        flash(loc[msg] if msg in loc else msg, category=loc["hp_errtitle"])
        return redirect("/home_page")
    return ret

@app.route('/recycle_bin', methods=['GET', 'POST'])
@auth_dec
def recycle_bin():
    from modules.recycle_bin import recycle_bin
    cur_user = User.current()

    if cur_user.is_admin():
        page = recycle_bin()
    else:
        return redirect("/home_page")

    if request.method == 'POST':
        if 'del_folder' in request.form and request.form['del_folder']:
            page.delete_folder(request.form['del_folder'])
        elif 'del_share' in request.form:
            page.delete_docshare(request.form['del_share'])
        elif 'res_folder' in request.form and request.form['res_folder']:
            page.restore_folder(request.form['res_folder'])
        elif 'res_share' in request.form:
            page.restore_docshare(request.form['res_share'])

    page.render()
    return render_template("temp.html",
                           page = page,
                           user = cur_user,
                           hp = get_hp(RECYCLE_BIN))

@app.route('/recycle_bin_folder', methods=['GET', 'POST'])
@auth_dec
def recycle_bin_folder():
    from modules.recycle_bin import recycle_bin
    cur_user = User.current()

    if cur_user.is_admin():
        page = recycle_bin()
    else:
        return redirect("/home_page")

    if request.method == 'POST':
        if 'del_folder' in request.form and request.form['del_folder']:
            page.delete_folder(request.form['del_folder'])
        elif 'del_share' in request.form:
            page.delete_docshare(request.form['del_share'])
        elif 'res_folder' in request.form and request.form['res_folder']:
            page.restore_folder(request.form['res_folder'])
        elif 'res_share' in request.form:
            page.restore_docshare(request.form['res_share'])

    guid = request.args.get("guid")
    if not guid:
        return redirect(url_for("recycle_bin"))

    page.render_folder(guid)
    return render_template("temp.html",
                           page = page,
                           user = cur_user,
                           hp = get_hp(RECYCLE_BIN))

@app.route('/recycle_bin_dshare', methods=['GET', 'POST'])
@auth_dec
def recycle_bin_dshare():
    from modules.recycle_bin import recycle_bin
    cur_user = User.current()

    if cur_user.is_admin():
        page = recycle_bin()
    else:
        return redirect("/home_page")

    if request.method == 'POST' and "docshare" in request.form:
        ret = page.operate_docshare(request.form["docshare"])

    guid, parent = request.args.get("guid"), request.args.get("parent")


    page.render_doc_share(guid,parent)
    return render_template("temp.html",
                           page=page,
                           user = User.current(),
                           hp=get_hp(RECYCLE_BIN))

@app.route('/users_and_groups', methods=['GET', 'POST'])
@auth_dec
def usrgroup():
    if not User.current().is_admin():
        return redirect("/home_page")    
    from modules.usrgroup import users_and_groups as usgr
    ug = usgr()
    if request.method == 'GET':
        ug.render(request.args)
    if request.method == 'POST':
        ug.render(request.form)

    return render_template("temp.html", 
                           page=ug, user=User.current(), hp=get_hp(ADMINISTRATION))

@app.route('/group', methods=['GET','POST'])
@auth_dec
def group():
    from modules.group import group
    
    if request.method == 'POST':
        if "guid" not in request.form:
            return redirect("/users_and_groups")
        pgroup = group(request.form["guid"])
        if not pgroup.check():
            return redirect("/users_and_groups")
        if "add_user" in request.form:
            pgroup.add_user(request.form["add_user"])
        pgroup.render()
    else:
            if "guid" not in request.args:
                return redirect("/users_and_groups")
            pgroup = group(request.args["guid"])
            if not pgroup.check():
                return redirect("/users_and_groups")            
            if "acl_type" in request.args:
                pgroup.acl_type(request.args)
            if "delete" in request.args:       
                pgroup.delete_user(request.args["delete"])
            pgroup.render()    
    return render_template("temp.html", page=pgroup, user=User.current(), hp=get_hp(ADMINISTRATION))

@app.route('/user', methods=['GET','POST'])
@auth_dec
def user():
    from modules.user import user
    if "guid" not in request.args:
        return redirect("/users_and_groups")
    puser = user(request.args["guid"])
    if not puser.check():
        return redirect('/users_and_groups')
    if request.method == 'POST':
        if request.form.get("group"):
            puser.add_to_group(request.form["group"])        
        elif "user_id" in request.form:
            puser.update_user(request.form)
    else:
        if "delete" in request.args:       
            puser.delete_from_group(request.args["delete"])
    puser.render()
    return render_template("temp.html", page=puser, user=User.current(), hp=get_hp(ADMINISTRATION))

@app.route('/searches', methods=['GET','POST'])
@auth_dec
def admin_searches():
    from modules.searches import searches
    s = searches()
    if request.method == 'GET':
        if "del" in request.args:    
            s.delete(request.args["del"])
    s.render()
    return render_template("temp.html", page=s,
                           user=User.current(), hp=get_hp(ADMINISTRATION))
    
@app.route('/tree_editing', methods=['GET','POST'])
@auth_dec
def tree_editing():
    if not User.current().is_admin():
        return redirect("/home_page")    
    from modules.tree_editing import archive_tree
    at = archive_tree()
    if request.method == 'POST':
        if request.form.get("folder_guid") and "folder_rename" in request.form:
            at.rename(request.form["folder_guid"],request.form["folder_rename"])
        elif request.form.get("cmd")=="delete" and "target_guid" in request.form:
            at.delete(request.form["target_guid"])            
        elif  request.form.get("cmd")=="copy" and "source_guid" in request.form and "target_guid" in request.form:
            at.copy(request.form["source_guid"], request.form["target_guid"],request.form.get("num_copy"))
    else:
       
        pass
    at.render()
    return render_template("temp.html", page=at,
                           user=User.current(), hp=get_hp(ADMINISTRATION))    
@app.route('/file_server', methods=['GET', 'POST'])
@auth_dec
def file_server():
    from modules.file_server import file_server
    fs = file_server()
    if request.method == 'POST':
        if "add_smartfolder" in request.form:
            fs.add_smartfolder(request.form["add_smartfolder"])
        elif "add_share" in request.form:
            fs.add_share(request.form["add_share"])
        elif "delete" in request.form:
            fs.delete_share(request.form["delete"])
    #else:
    #    if "delete_share" in request.args:
    #        fs.delete_share(request.args["delete_share"])



    fs.render()
    return render_template("temp.html", page=fs, user = User.current(), hp=get_hp(FILE_SHARE))

@app.route('/file_share_ajax', methods=['POST'])
def file_share_ajax():
    if not User.current():
        return '{"data":"[]"}'
    from modules.file_share import file_share
    if not "file_share" in request.form:    
        return '{"data":"[]"}'
    fs = file_share(request.form["file_share"],request.form.get("folder",None))
    return fs.get_ajax_data()

@app.route('/doc_fulltext_ajax', methods=['POST'])
def doc_fulltext_ajax():
    loc = localization.get_lang()
    guid, parent, doc_id, user = request.form.get("guid"), request.form.get("parent"), request.form.get("doc_id"), User.current()
    if not guid or not parent or not doc_id:
        return json.dumps({"success":False, "title":loc['error'], "data":loc['wf_q_notargs']})
    
    from lib.class_document_share import Document_share
    from lib.class_folder import Folder
    from lib.xappy_fulltextsearch import fulltextsearch
    doc_share = Document_share.get_by_guid(guid)
    #xconn = indexconnector()
    xsearch = fulltextsearch()	    
    #doc_share.set_connector(xconn)
    doc_share.set_search(xsearch)    
    folder = Folder.get_by_guid(parent)
    if not user or not user.has_write_access_into(doc_share):
        return json.dumps({"success":False, "title":loc['error'], "data":loc['ACCESS_DENIED']})   
    
    if request.method == 'POST': 
        doc = doc_share.get_doc_by_db_id(doc_id)
        return json.dumps({"success":True, "data": doc.fulltext(None)})


@app.route('/file_share', methods=['GET', 'POST'])
@auth_dec
def file_share():
    from modules.file_share import file_share
    if not "share" in request.args:
        return redirect(url_for("file_server"))
    fs = file_share(request.args["share"],request.args.get("parent"))
    if not fs.check():
        return redirect(url_for("file_server"))
    if request.method == 'POST':
        if "upload_file" in request.files:
            if 'lastupload' not in session:
                session['lastupload']=[]
            if request.form['randkey'].isdigit() and not request.form['randkey'] in session['lastupload']:
                session['lastupload'].append(request.form['randkey'])            
    
                up_file = request.files['upload_file']
                fs.change_parent_dir(request.form.get("file_share"))
                #from werkzeug import secure_filename
                #filename = secure_filename(up_file.filename)
                if up_file.filename:
                    fs.upload_file(up_file, up_file.filename)
        elif "folder" in request.form:
            fs.change_parent_dir(request.form.get("file_share"))
            folder = request.form["folder"]
            fs.add_folder(folder)        
        elif "download_file" in request.form:
            return fs.download_file(request.form["download_file"])
        elif "download_list" in request.form:
            fs.download_list(request.form["download_list"])
        elif "fstask" in request.form:
            fs.change_parent_dir(request.form.get("file_share"))
            if request.form["fstask"] == "del":
                fs.delete_list(request.form.getlist("g"))
            elif request.form["fstask"] == "down":
                return fs.download_list(request.form.getlist("g"))
    else:
        if "download" in request.args:
            return fs.download_file(request.args["download"])                

    fs.render()
    return render_template("temp.html", page=fs, user = User.current(), hp=get_hp(FILE_SHARE))

@app.route('/file_share_edit', methods=['GET', 'POST'])
@auth_dec
def file_share_edit():
    from modules.file_share import file_share
    if not "guid" in request.args:
        return redirect(url_for("file_server"))
    fs = file_share(request.args["guid"],None)    
    if not fs.check():
        return redirect(url_for("file_server"))	
    if request.method == 'POST':
        if "folder_rename" in request.form:
            fs.rename_share(request.form["folder_rename"])
        elif "acl_type" in request.form:
            fs.execute_acl(request.form)
    else:
        if "del_acl" in request.args:
            fs.execute_acl(request.args) 

    fs.render_edit()
    return render_template("temp.html", 
                           page=fs, user = User.current(), hp=get_hp(FILE_SHARE))    


@app.route('/smart_folder', methods=['GET', 'POST'])
@auth_dec
def smart_folder():
    if not "folder" in request.args:
        return redirect(url_for("file_server"))	
    from modules.smart_folder import smart_folder
    sf = smart_folder(request.args["folder"])   
    if not sf.check():
        return redirect(url_for("file_server"))
    if request.method == 'POST':
        if "new_folder" in request.form:
            sf.new_folder(request.form["new_folder"])
        elif "folder_rename" in request.form and "rename" in request.form:
            sf.folder_rename(request.form["rename"],request.form["folder_rename"])	
        elif "delete" in request.form:
            sf.delete(request.form["delete"])            
            
    sf.render()
    return render_template("temp.html", page=sf, user = User.current(), hp=get_hp(FILE_SHARE))

@app.route('/smart_folder_edit', methods=['GET', 'POST'])
@auth_dec
def smart_folder_edit():
    from modules.smart_folder import smart_folder
    if not "guid" in request.args:
        return redirect(url_for("file_server"))
    sf = smart_folder(request.args["guid"])    
    if not sf.check():
        return redirect(url_for("file_server"))	
    if request.method == 'POST':
        if "folder_rename" in request.form:
            sf.rename(request.form["folder_rename"])
        elif "acl_type" in request.form:
            sf.execute_acl(request.form)
    else:
        if "del_acl" in request.args:
            sf.execute_acl(request.args)    

    sf.render_edit()
    return render_template("temp.html", 
                           page=sf, user = User.current(), hp=get_hp(FILE_SHARE))    

@app.route('/documents_share', methods=['GET', 'POST'])
@auth_dec
def documents_share():
    from modules.documents_share import document_share

    pp = document_share()
    if request.method == 'POST':
        if "new_folder" in request.form:
            pp.new_folder(request.form["new_folder"])
        elif "del_folder" in request.form:
            pp.del_folder(request.form["del_folder"])
    if request.method == 'GET':
        if "ln" in request.args:
            set_lang = request.args['ln']
            localization.switch_lang(set_lang.lower() )
            return redirect(request.args.get("back_url", "/documents_share"))
        else:
            if "lang" not in session:
                set_lang = str(request.environ.get("HTTP_ACCEPT_LANGUAGE",""))[:2]
                localization.switch_lang(set_lang if set_lang else "fr")	
    pp.render()
    return render_template("temp.html",
                           page=pp, user = User.current(),hp=get_hp(DOCUMENT_SHARE))


@app.route('/preview', methods=['GET', 'POST'])
@auth_dec
def preview():
    guid,parent = request.args.get("guid"), request.args.get("parent")
    if not guid or not parent or not "preview" in request.args:
        return redirect(url_for("documents_share"))
    pp = document_share()
    ret = pp.download_doc(guid, request.args["preview"], preview=True)
    if ret:
        return ret
    else:
        abort(404)
            
@app.route('/document_share', methods=['GET', 'POST'])
@auth_dec
def document_share_page():
    guid,parent = request.args.get("guid"), request.args.get("parent")
    if not guid or not parent:
        return redirect(url_for("documents_share"))

    from modules.documents_share import document_share
    
    pp = document_share()
    if request.method == 'POST':
        if "docshare" in request.form:    
            ret = pp.operate_docshare(request.form["docshare"])
            if ret:
                return ret
        elif "edit_guid_share" in request.form and "edit_guid_doc" in request.form:
                ret = pp.edit_guid_share(request.form["edit_guid_share"], request.form["edit_guid_doc"], request.form)
                pp.hpt_data += ret
    else:
        if "download_doc" in request.args:
            ret = pp.download_doc(guid, request.args["download_doc"])
            if ret:
                return ret
        

    pp.render_doc_share(guid,parent)
    return render_template("temp.html",
                           page=pp, user = User.current(),hp=get_hp(DOCUMENT_SHARE),quick=get_quicksearch(guid,parent))

@app.route('/document_share_edit', methods=['GET', 'POST'])
@auth_dec
def document_share_edit():
    from modules.document_share_edit import share_edit
    guid = request.args.get("guid")
    parent = request.args.get("parent")
    if not guid or not parent:
        return redirect(url_for("documents_share"))
    pp = share_edit(guid,parent)
    if not pp.can_edit():
        return redirect(url_for("documents_share"))
    
    if request.method == 'POST':
        if "acl_type" in request.form:
                pp.execute_acl(request.form)
        if "share_rename" in request.form:
            pp.share_rename(request.form["share_rename"])
        elif "type" in request.form and request.form.get("name"):
            pp.add_metafields(request.form)
        elif "metid" in request.form:
            pp.edit_metafields(request.form)
        elif "subscribe" in request.form:
            pp.toggle_subscribe(request.form)
            
    else:
        if "del_acl" in request.args:
            pp.execute_acl(request.args)
        elif "del_met" in request.args:
            pp.del_metafields(request.args["del_met"])        
    pp.render()
    return render_template("temp.html",
                           page=pp, user = User.current(), hp=get_hp(DOCUMENT_SHARE))


@app.route('/folder', methods=['GET', 'POST'])
@auth_dec
def folder():
    from modules.folder import folder

    guid = request.args.get("guid")
    if not guid:
        return redirect(url_for("documents_share"))

    pp = folder(guid)

    if request.method == 'POST':
        if "new_folder" in request.form:
            pp.new_folder(request.form["new_folder"])
            #elif "del_folder" in request.form:
            #    pp.del_folder(request.form["del_folder"])
        elif "new_share" in request.form:
            pp.new_share(request.form["new_share"])

        elif "acl_type" in request.form:
            pp.acl_type()

        elif "del_folder" in request.form and request.form["del_folder"]:
            pp.del_folder(request.form["del_folder"])

        elif "del_share" in request.form:
            pp.del_doc_share(request.form["del_share"])

    pp.render()
    return render_template("temp.html",#"folder.html",
                           page=pp, user = User.current(),hp=get_hp(DOCUMENT_SHARE),quick=get_quicksearch(guid))

@app.route('/folder_edit', methods=['GET', 'POST'])
@auth_dec
def folder_edit():
    from modules.folder_edit import folder_edit

    guid = request.args.get("guid")
    if not guid:
        return redirect(url_for("documents_share"))

    pp = folder_edit(guid)
    if not pp.check():
        return redirect(url_for("documents_share"))
    if request.method == 'POST':
        if "folder_rename" in request.form:
            pp.folder_rename(request.form["folder_rename"])
        elif "acl_type" in request.form:
            pp.execute_acl(request.form)
    else:
        if "del_acl" in request.args:
            pp.execute_acl(request.args)         

    pp.render()
    return render_template("temp.html",
                           page=pp, user = User.current(),hp=get_hp(DOCUMENT_SHARE))



@app.route('/custom_search', methods=['GET', 'POST'])
@auth_dec
def custom_search():
    from modules.custom_search import custom_search
    loc = localization.get_lang()
    guid = request.args.get("guid")
    ret = ""
    if not guid:
        return redirect(url_for("documents_share")) 
    pp = custom_search(guid)    
    if request.method == 'GET':
        pass
        
    elif request.method == 'POST':
        #Copy-paste from Search - TODO: combine code
        if "docshare" in request.form:
            """ operation whith docs on doc_share (Delete, send, dounload)"""      
            ds = document_share()
            ret = ds.operate_docshare(request.form["docshare"])
            if ret:
                return ret
                     
        elif "edit_guid_share" in request.form and "edit_guid_doc" in request.form:
            """ Edit metas of document"""     
            ds = document_share()
            ret = ds.edit_guid_share(request.form["edit_guid_share"],request.form["edit_guid_doc"],request.form)
    pp.render()
    pp.hpt_data += ret
    return render_template("temp.html",
                           page=pp, user = User.current(),hp=get_hp(DOCUMENT_SHARE))    

@app.route('/custom_search_edit', methods=['GET', 'POST'])
@auth_dec
def custom_search_edit():
    from modules.custom_search_edit import custom_search_edit
    loc = localization.get_lang()
    guid = request.args.get("guid")
    if not guid:
        return redirect(url_for("documents_share"))     
    pp = custom_search_edit(guid)
    if request.method == 'GET':
        pp.render()

    elif request.method == 'POST':
        #Copy-paste from Search - TODO: combine code
        ret = ""
        if "docshare" in request.form:
            """ operation whith docs on doc_share (Delete, send, dounload)"""      
            from modules.documents_share import document_share
            ds = document_share()
            ret = ds.operate_docshare(request.form["docshare"])
            if ret:
                return ret
                #flash(ret,loc["mail_fsend_title"]) 

        elif "edit_guid_share" in request.form and "edit_guid_doc" in request.form:
            """ Edit metas of document"""     
            ds = document_share()
            ret = ds.edit_guid_share(request.form["edit_guid_share"],request.form["edit_guid_doc"],request.form)
        
        pp.render()
        pp.hpt_data += ret
    return render_template("temp.html",
                           page=pp, user = User.current(),hp=get_hp(DOCUMENT_SHARE))    
@app.route('/custom_search_acl', methods=['GET', 'POST'])
@auth_dec
def custom_search_acl():
    from modules.custom_search_acl import custom_search_acl
    guid = request.args.get("guid")
    if not guid:
        return redirect(url_for("documents_share"))  
    pp = custom_search_acl(guid)
    if not pp.check():
        return redirect(url_for("documents_share"))  
    if request.method == 'GET':
        if request.args.get("del_acl"):
            pp.update_acl(request.args)
    elif request.method == 'POST':
        if "acl_type" in request.form:
            pp.update_acl(request.form)
    pp.render()
    return render_template("temp.html",
                           page=pp, user = User.current(),hp=get_hp(DOCUMENT_SHARE))   

@app.route('/Tree', methods=['GET', 'POST'])
@auth_dec
def tree():
    from modules.tree import tree

    folder = request.args.get("map")
    pp = tree(folder)
    pp.render()
    return render_template("temp.html",
                           page=pp, user = User.current(), hp=get_hp(ADMINISTRATION))

@app.route('/import_data', methods=['GET','POST'])
@auth_dec
def import_data():
    if not User.current().is_admin():
        return redirect("/home_page")
    loc = localization.get_lang()
    from modules.importexport import importexport
    pp = importexport()
    if request.args.get("view","").isdigit():
        pp.render_restore(request.args["view"])
    else:
        if request.method == 'GET':
            send = None
            if request.args.get("delete","").isdigit():
                pp.delete_task(request.args["delete"])
                return redirect("/import_data")
            elif request.args.get("now","").isdigit():
                pp.run_task(request.args["now"])
                return redirect("/import_data")
                
            elif request.args.get("restore","").isdigit() and request.args.get("revision","").isdigit():
                if pp.restore(request.args["restore"],request.args["revision"]):
                    return redirect("/home_page?msg=Restored!")
            elif "backup" in request.args:
                send = pp.get_backup()
            elif "export" in request.args:
                send = pp.get_doc_export()
            if send:
                from lib.utils_migration import ARCHIVE_FULLNAME
                from threading import Timer
                @after_this_request
                def remove_file(response):
                    try:
                        os.remove(ARCHIVE_FULLNAME)
                    except:
                        def remove_later(path):
                            try:
                                os.remove(path)
                            except:pass
                        t = Timer(90.0, remove_later,(ARCHIVE_FULLNAME,))
                        t.start()
                    return response                     
                return send
        else:
            if all(request.form.get(a) for a in ["mode","unit","rotation","interval"]):
                ret = pp.add_schedule(*[request.form.get(a) for a in ["mode","interval","unit","time","rotation","destination","login","passw","remoteserver"]])
                if ret:
                    return ret
            elif "file_upload" in request.files:
                fname = request.files["file_upload"].filename
                ret = pp.import_backup(request.files["file_upload"])
                logging.info(ret)
                if ret:
                    return json.dumps({"files": [fname],"message":ret})
                
            
        pp.render()
    return render_template("temp.html", page=pp,
                               user=User.current(), hp=get_hp(ADMINISTRATION))     

@app.route('/import_ldap', methods=['GET','POST'])
@auth_dec
def import_ldap():
    if not User.current().is_admin():
        return redirect("/home_page")    
    loc = localization.get_lang()
    from modules.importldap import importldap
    pp = importldap()
    if request.method == 'GET':
        if "clean_ldap" in request.args:
            pp.delete_subjects()
            User.logoff()
            return redirect("/home_page")
    else:
        try:
            if all(request.form.get(a) for a in ["login","password","server","base_dn"]):
                ret = pp.connect(request.form["login"],request.form["password"],request.form["server"],request.form["base_dn"])
                if ret:
                    flash(ret,category=loc["error"])
                elif pp.connection:
                    pp.fetch_users()
            elif pp.load_session() and all(request.form.get(a) for a in ["user_login","user_guid","group_name","group_guid","group_users"]):
                pp.save_config(request.form.get("user_guid"),request.form.get("user_login"),request.form.get("user_first_name",""),request.form.get("user_last_name",""),
                               request.form.get("group_guid"),request.form.get("group_name"),request.form.get("group_users"))
            elif "preview" in request.form:
                pp.save_subjects()
            else:
                flash(loc["config_error"],category=loc["error"])
        except Exception as e:
            flash(unicode(e),loc["ldap_import_error"])
    pp.render()
    return render_template("temp.html", page=pp,
                               user=User.current(), hp=get_hp(ADMINISTRATION))     
@app.route('/info', methods=['GET'])
@auth_dec
def info():
    if not User.current().is_admin():
        return redirect("/home_page")    
    loc = localization.get_lang()
    from modules.admin_info import admin_info
    pp = admin_info()
    if request.method == 'GET':
        if "delete_data" in request.args:
            pp.delete_all()    
            flash("Database successfully cleaned.",category='Success')
            return redirect("/home_page?logoff")
    pp.render()
    return render_template("temp.html", page=pp,
                               user=User.current(), hp=get_hp(ADMINISTRATION)) 


@app.route('/network', methods=['GET','POST'])
@auth_dec
def network():
    if not User.current().is_admin():
        return redirect("/home_page")    
    loc = localization.get_lang()
    from modules.netconfig import netconfig
    pp = netconfig()
    if request.method == 'GET':
        pass
    else:
        if all(request.form.get(a) for a in ["server","mask","gw"]):
            pp.set_network(request.form["server"],request.form["mask"],request.form["gw"])
        if all(request.form.get(a) for a in ["server","port","user","password","ssl"]):
            pp.set_smtp(request.form["server"],request.form["port"],request.form["user"],request.form["password"],request.form["ssl"])        
            
        
    pp.render()
    return render_template("temp.html", page=pp,
                               user=User.current(), hp=get_hp(ADMINISTRATION))     

@app.route('/reset_password', methods=['GET','POST'])
def reset_password():
    loc = localization.get_lang()
    if "resetid" in request.args and "guid" in request.args:
        user = User.get_by_guid(request.args.get("guid"))
        if not user:
            return redirect("/home_page")  
        new_resetid = user.generate_hashcode()
        if not request.args.get("resetid") == new_resetid:
            return redirect("/home_page")          
        hidestyle = ""
        
        title = "%s, %s!"%(loc["hp_hello"],user.login)
        text_info =loc["APPLY_CHANGES_TO_P2W_D2W"]
        if request.method == 'POST':
            if request.form.get("form_newpass",None) == request.form.get("form_confirm",""):
                user.password = md5(request.form["form_newpass"]).hexdigest()
                user.save_new_password()                
                title = "<b>%(hp_passchanged)s</b>"%{"hp_passchanged" : loc["hp_passchanged"]}
                text_info = "<br><br><a href='/home_page'>%(hp_back)s</a>"%{
                    "hp_back" : loc["hp_back"]
                }
                hidestyle = 'style="display:none"'
                flash(loc["hp_passchanged"],category=loc['hp_restoretitle'])
            else:
                flash(loc["hp_wrongconfirm"],category=loc["hp_errinput"])
            
        return render_template('reset.html', hp_passchanged = title,
                               text_reset_title = title,text_info = text_info,
                               hidestyle = hidestyle,uag_confirmpass = loc["uag_confirmpass"],hp_forgot = loc['hp_forgot'],
                               hp_enter=loc['hp_enter'],hp_password=loc['hp_password'])
@app.route('/registration', methods=['GET', 'POST'])
def registration():
    loc = localization.get_lang()
    return render_template('newlogin.html', backurl="",hp_forgot = loc['hp_forgot'],
                       hp_enter=loc['hp_enter'],hp_login=loc['hp_login'],hp_password=loc['hp_password'],
                       hp_next=loc['hp_next'],hp_restoretitle=loc['hp_restoretitle'],hp_back=unicode(loc['hp_back']),
                       register=show_license())
    

@app.route('/home_page', methods=['GET', 'POST'])
def home_page():
    @after_this_request
    def add_header(response):
        response.headers['P3P'] = 'CP="ALL IND DSP COR ADM CONo CUR CUSo IVAo IVDo PSA PSD TAI TELo OUR SAMo CNT COM INT NAV ONL PHY PRE PUR UNI"'
        return response
    limits = User.get_max_users() and User.get_max_documents()
    if User.admin_exists() and limits:
        register= ""
    else:
        register=show_license(User.admin_exists(),limits)
    cfg = Config()
    
    loc = localization.get_lang()
    url = request.args.get("back_url", "/documents_share")
    skip_login = False
    version = "v.%s/%s. DB v.%s<br/>%s"%(CONFIG["VERSION"],CONFIG["VERSIONDATE"],Update.get_cur_version(),loc['hp_copyright'])
    if request.method == 'GET':
        if request.args.get("msg"):
            flash(request.args.get("msg"),category=loc["workflowlog_log_info"])        
        elif request.args.get("error"):
            flash(request.args.get("error"),category=loc["hp_errtitle"])

        if "logoff" in request.args:
            User.logoff()
            #return redirect("/home_page")
        elif User.current() and limits:
            return redirect(url)        
    elif request.method == 'POST':
        if all(request.form.get(a) for a in ["login","email","password","r_password"]):
            p1 = request.form["password"]
            p2 = request.form["r_password"]
            if p1 and p1==p2:
                try:
                    
                    u = User.get_by_login(request.form["login"]) or User()
                    u.login = request.form["login"]
                    u.email = request.form["email"]
                    u.password = md5(p1).hexdigest()
                    u.set_as_admin()    
                    register= ""
                    skip_login = True
                    flash(loc["hp_passchanged"])
                except Exception as e:
                    flash(unicode(e),category=loc["hp_errtitle"])
            else:
                flash("incorrect or empty password ",category=loc["hp_errtitle"])
        elif request.form.get("restore") =="restore" and "login" in request.form:
            u = User.get_by_login(request.form["login"])
            if u:
                u.reset_password()
                flash(loc["hp_sent"],category=loc['hp_restoretitle'])
            else:
                flash(loc["hp_usernotexist"],category=loc['hp_restoretitle'])
        elif not skip_login and register=="" and "login" in request.form and "password" in request.form:
            server_host = request.environ["HTTP_HOST"].split(":")[0]
            if server_host:
                cfg.set_value("server_host",server_host)
                cfg.commit()
            user = User.log_in(request.form["login"], request.form["password"])
            if user:
                return redirect(request.form.get("back_url", "/documents_share"))
            else:
                flash(loc["hp_errinput"], category=loc["hp_errtitle"])

    return render_template('newlogin.html',title=loc['header_hgmb'], backurl=url,hp_forgot = loc['hp_forgot'],
                           hp_enter=loc['hp_enter'],hp_login=loc['hp_login'],hp_password=loc['hp_password'],
                           hp_next=loc['hp_next'],hp_restoretitle=loc['hp_restoretitle'],hp_back=unicode(loc['hp_back']),
                           register=register,version = version)

@app.route('/search', methods=['GET', 'POST'])
@auth_dec
def search():
    from modules.search import search
    loc = localization.get_lang()
    pp = search() 
    if "smart" in request.args:
        searchmethod = 1
        is_smart_search = True        
        search_query = request.args["smart"]
        search_orig = pp.smart(request.args["smart"])        
    else:
        is_smart_search = False
        searchmethod=None
        search_query = request.args.get("search","")    
        search_orig = ""
    if request.method == 'GET':
        
        #if "smart" in request.args:
        #    pp.render_all(search, request.args.get("searchmethod"))
        #el
        if "search" in request.args and request.args["search"].strip() or is_smart_search:
            if 	"guid" in request.args and request.args["guid"] !="" :
                pp.render_guid(request.args["guid"],request.args.get("parent"),search_query,request.args.get("searchmethod"))
            else:
                pp.render_all(search_query, request.args.get("searchmethod",searchmethod),search_orig)
        else:
            pp.render(search_query, request.args.get("searchmethod","3"))
    else:
       
        if "docshare" in request.form:
            """ operation whith docs on doc_share (Delete, send, dounload)"""      
            
            ds = document_share()
            ret = ds.operate_docshare(request.form["docshare"])
            if ret:
                return ret            
            
            #doc_share = Document_share.get_by_guid(args["docshare"])       
        if "edit_guid_share" in request.form and "edit_guid_doc" in request.form:
            """ Edit metas of document"""     
            ds = document_share()
            ret = ds.edit_guid_share(request.form["edit_guid_share"],request.form["edit_guid_doc"],request.form)
            if 	"guid" in request.args and request.args["guid"] !="" :
                pp.render_guid(request.args["guid"],request.args.get("parent"),search_query,request.args.get("searchmethod"))
                pp.hpt_data += ret
            else:
                pp.render_all(search_query, request.args.get("searchmethod"))
                pp.hpt_data += ret
                                   
        elif "search" in request.args and request.args["search"].strip() or is_smart_search:
            if 	"guid" in request.args and request.args["guid"] !="" :
                pp.render_guid(request.args["guid"],request.args.get("parent"),search_query,request.args.get("searchmethod"))
            else:
                pp.render_all(search_query, request.args.get("searchmethod"))
          
    return render_template('temp.html', page=pp, user=User.current(),
                           hp=get_hp(DOCUMENT_SHARE))

@app.route('/htp.js')
def htp_js_view():
    response = make_response(htp_js())
    response.headers['Content-Type'] = "application/javascript"
    response.headers['Expires'] = int(time() + 60.0)
    return response    

@app.route('/logo')
@auth_dec
def userlogo():
    if "guid" in request.args:
        user = User.get_by_guid(request.args["guid"])
    else:
        user = User.current()
    if user and os.path.exists(user.logo):
        return send_file(user.logo,mimetype="image/png",cache_timeout=5,conditional = True)
    else:
        abort(404)

@app.route('/API', methods=['POST'])
def api():
    from api import call_action
    if request.method == 'POST':
        if "action" in request.form:
            return call_action(request.form["action"],request.form.get("xml_data"))
            

oldstaticpath = os.path.join(app.root_path, "static")

@app.route('/img/<path:filename>')
def custom_static(filename):
    if '..' in filename or filename.startswith('/'):
        abort(404)
    filename = safe_join(oldstaticpath, filename)
    if not os.path.isfile(filename):
        abort(404)
    return send_file(filename,cache_timeout=60,conditional = True)


