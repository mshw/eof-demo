"""
	WorkflowInstance.

	Describes info on a document involved in some WorkflowInstance process.
"""

from class_CRUD import CRUD
from uuid import uuid4
import os, os.path, shutil

TABLE_NAME = "workflow_instance"

class WorkflowInstance(object):

    STATES = {
        "stopped" 	: 0,
        "pause" 	: 1,
        "active" 	: 2,
        "finished" 	: 3
    }


    def __init__(self):
        # database info
        self.id		= None
        self.workflow_guid	= ""
        self.author_guid	= ""
        self.current_user_guid = ""
        self.document_guid	= ""
        self.guid			= ""
        self.status 		= self.STATES["stopped"]
        self.state 			= "no state"

        # properties (lazy loading)
        self._workflow_class= None	# Instance of a WorkflowInstance class
        self._author		= None	# Author
        self._current_user	= None
        self._document		= None	# instance of a current document
        self._path			= ""	# path for storage versions of documents

    def __fill_from_row(self, row):
        (	self.id, self.workflow_guid, self.author_guid,
                 self.current_user_guid, self.document_guid,
                 self.guid, self.status, self.state
                 ) = row
        return self

    def __insert(self):
        self.guid = str(uuid4())
        self.id = CRUD.create(TABLE_NAME,
                              workflow_guid		= self.workflow_guid,
                              author_guid			= self.author_guid,
                              current_user_guid	= self.current_user_guid,
                              document_guid		= self.document_guid,
                              status				= self.status,
                              state				= self.state,
                              guid				= self.guid
                              )
        return self

    @staticmethod
    def __retrieve(**kwargs):
        rows = CRUD.retrieve(TABLE_NAME, **kwargs)
        return [WorkflowInstance().__fill_from_row(row) for row in rows]

    @staticmethod
    def __retrieve_one(**kwargs):
        row = CRUD.retrieve_one(TABLE_NAME, **kwargs)
        return WorkflowInstance().__fill_from_row(row) if row else None

    def __update(self):
        CRUD.update( TABLE_NAME,
                     {"id": self.id},
                     workflow_guid		= self.workflow_guid,
                     author_guid			= self.author_guid,
                     current_user_guid	= self.current_user_guid,
                     document_guid		= self.document_guid,
                     status				= self.status,
                     state				= self.state,
                     )
        return self

    def __delete(self):
        CRUD.delete(TABLE_NAME, id=self.id)
        return self

    def save(self):
        if self.id:
            self.__update()
        else:
            os.makedirs(self.path)
            self.__insert()

        return self

    def delete(self):
        if os.path.exists(self.path):
            shutil.rmtree(self.path)
        return self.__delete()

    # PROPERTIES

    @property
    def workflow_class(self):
        if not self.workflow_guid:
            return ""
        if not self._workflow_class:
            from class_workflow import Workflow
            self._workflow_class = Workflow.get_by_guid(self.workflow_guid)
        return self._workflow_class

    @workflow_class.setter
    def workflow_class(self, value):
        from class_workflow import Workflow
        if isinstance(value, Workflow):
            self._workflow_class 	= value
            self.workflow_guid 		= value.guid

    @property
    def author(self):
        if not self.author_guid:
            return ""
        if not self._author:
            from class_user import User
            self._author = User.get_by_guid(self.author_guid)
        return self._author

    @author.setter
    def author(self, value):
        from class_user import User
        if isinstance(value, User):
            self._author 	= value
            self.author_guid= value.guid

    @property
    def current_user(self):
        if not self.current_user_guid:
            return ""
        if not self._current_user:
            from class_user import User
            self._current_user = User.get_by_guid(self.current_user_guid)
        return self._current_user

    @current_user.setter
    def current_user(self, value):
        from class_user import User
        if isinstance(value, User):
            self._current_user 		= value
            self.current_user_guid 	= value.guid

    @property
    def document(self):
        if not self.document_guid:
            return ""
        if not self._document:
            from class_share_file import ShareFile
            self._document = ShareFile.get_by_guid(self.document_guid)
        return self._document

    @document.setter
    def document(self, value):
        from class_share_file import ShareFile
        if isinstance(value, ShareFile):
            self._document 		= value
            self.document_guid 	= value.guid

    @property
    def path(self):
        if not self._path:
            self._path = os.path.join(
                CONFIG["SHARE-DIRECTORY"],
                self.workflow_guid,
                self.guid
            )
        return self._path

    def set_state(self, key):
        if not key in self.STATES: return
        self.state = self.STATES[key]

    # GETTIES

    @staticmethod
    def all():
        return WorkflowInstance().__retrieve()

    @staticmethod
    def get_by_id(workflow_id):
        return WorkflowInstance().__retrieve_one(	id = workflow_id )

    @staticmethod
    def get_by_guid(workflow_guid):
        return WorkflowInstance().__retrieve_one(	guid = workflow_guid )

    @staticmethod
    def get_by_workflow_guid(wf_guid):
        return WorkflowInstance().__retrieve(	workflow_guid = wf_guid )


    # DOCUMENT METHODS

    def documents(self):
        pass

    def actual_document(self):
        if not os.path.exists(self.path):
            raise Exception("FILE_NOT_FOUND") # file already deleted or wrong path
        count = len( os.listdir(self.path) )
        if count != 0:
            full_path = so.path.join( self.path, str(count - 1) )
        else:
            full_path = self.document.complete_path

        if os.path.isfile(full_path):
            obsolete_request.send_file(
                self.name.encode("UTF8"),
                os.stat(full_path).st_size,
                open(full_path),
                self.mime()
            )

    def clean_documents(self):
        pass

    def add_document(self, doc_data):
        count = len( os.listdir(self.path) )
        full_path = so.path,join( self.path, str(count) )

        f = open(full_path, "w")
        if  type(doc_data) == types.FileType or hasattr(doc_data, "read"):
            shutil.copyfileobj(doc_data, f)
        else:
            f.write(doc_data)#.encode("utf8"))
        f.close()
