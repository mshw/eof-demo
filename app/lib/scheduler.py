import threading
import schedule
import logging
from class_CRUD import CRUD
from flask import flash
import time, Queue, datetime
from class_sharing import Sharing
from backup_drivers import FullLocalBackup, FullSSHBackup, FullSMBBackup
from class_trace import Trace
global jobqueue, linkqueue
jobqueue = Queue.Queue()
reminderqueue = Queue.Queue()
linkqueue = Queue.Queue()

class Scheduler:
    #cease_continuous_run = threading.Event()        
    @classmethod
    def register(self,task):
        try:
            job = schedule.every(task.interval)
            job.unit = task.unit
            if task.unit in ("monday","tuesday","wednesday","thursday","friday","saturday","sunday"):
                job.start_day = job.unit
                job.unit = "weeks"
                job.at(task.at_time)
            elif task.unit in ('days', 'hours'):
                job.at(task.at_time)      
            global jobqueue
            job.do(jobqueue.put, task)
        except AssertionError:
            flash("Invalid backup schedule")

    @classmethod
    def register_link(cls, link):
        try:
            global linkqueue
            interval = link.del_date - datetime.datetime.now()
            logging.info("New link register in scheduler with interval: %s" % interval)
            schedule.every(interval.seconds).seconds.do(cls.excecute_link, link)
        except AssertionError:
            flash("Invalid link registering")

    @classmethod
    def excecute_link(cls, link):
        linkqueue.put(link)
        return schedule.CancelJob
        # Remove scheduler task and make deletion task for thread
        
    @classmethod
    def start(self):
        for task in BackupTask.get_all():
            if task._status == "Running":
                task._status = "Scheduled"
                task.save()            
            self.register(task)

        Sharing.delete_outdated()
        for link in Sharing.get_all():
            self.register_link(link)
        #logging.info("1") 
        continuous_thread = ScheduleThread()
        continuous_thread.start()
        worker_thread = ScheduleWorkerThread()
        worker_thread.start()
        link_worker_thread = ScheduleLinkWorkerThread()
        link_worker_thread.start()
        remind_thread = ScheduleReminderWorkerThread()
        remind_thread.start()
        reminder_trash = ScheduleReminderTrashWorkerThread()
        reminder_trash.start()
        #logging.info("2")
        #return self.cease_continuous_run        
    
class ScheduleThread(threading.Thread):
    @classmethod
    def run(cls):
        while True:
            schedule.run_pending()
            time.sleep(10)
        #while not Scheduler.cease_continuous_run.is_set():
        #    schedule.run_pending()

class ScheduleWorkerThread(threading.Thread):
    @classmethod
    def run(cls):
        import logging
        while True:
            try:
                global jobqueue
                job_func = jobqueue.get(True, 60)
                logging.info("New task: %s"%job_func.destination)
                job_func()
                jobqueue.task_done()
                time.sleep(10)
            except Queue.Empty:
                continue
            except Exception as e:
                logging.info("worker traceback: " + Trace.exception_trace())
                logging.info("worker: %s" % str(e))

class ScheduleLinkWorkerThread(threading.Thread):
    @classmethod
    def run(cls):
        import logging
        while True:
            try:
                global linkqueue
                link = linkqueue.get(True, 60)
                logging.info("New link deletion task: %s" % link.guid)
                link.delete(link.guid)
                linkqueue.task_done()
                logging.info("Link deleted: %s" % link.guid)
                time.sleep(10)
            except Queue.Empty:
                continue
            except Exception as e:
                logging.info("worker traceback: " + Trace.exception_trace())
                logging.info("worker: %s" % str(e))

class ScheduleReminderWorkerThread(threading.Thread):
    @classmethod
    def run(cls):
        import logging
        while True:
            try:
                from class_notification import Notification, NotificationType
                from xappy_fulltextsearch import fulltextsearch
                from class_document import Document
                from class_document_share import Document_share

                logging.info("Main reminder scheduler starts work")
                reminders = Notification.get_all_by_type(NotificationType.mail_reminder())
                documents = {}
                conn = fulltextsearch()
                logging.info("reminder worker starts for %s reminders" % len(reminders))
                for reminder in reminders:
                    if "meta_guid" not in reminder.message:
                        Notification.delete(reminder.guid)
                        continue

                    share_guid = reminder.message["data_guid"]
                    doc_guid = reminder.message["documents"][0]
                    share = Document_share.get_by_guid(share_guid)
                    if not share:
                        Notification.delete(reminder.guid)
                        continue

                    if doc_guid in documents:
                        doc = documents[share_guid]
                    else:
                        search = conn.search(share_guid, [{"meta": "db_id", "value": doc_guid,  "type": ""}], 0, 100000,
                                             sortby=None)
                        s_docs = [Document.from_xapian_document(doc) for doc in search]
                        if len(s_docs) == 0:
                            Notification.delete(reminder.guid)
                            continue

                        doc = s_docs[0]
                        doc.xapian_description()
                        documents[share_guid] = doc


                    meta_time_str = doc.meta()[reminder.message["meta_guid"]]
                    delta_days = int(reminder.message["days_before"])
                    meta_time = datetime.datetime.strptime(meta_time_str, "%Y-%m-%d") - datetime.timedelta(days=delta_days)
                    if meta_time <= datetime.datetime.today():
                        reminder.remind()
                time.sleep(60)
            except Exception as e:
                logging.info("reminder worker: %s" % str(e))
                logging.info("reminder worker traceback: " + Trace.exception_trace())

class ScheduleReminderTrashWorkerThread(threading.Thread):
    @classmethod
    def run(cls):
        import logging
        while True:
            try:
                from class_notification import Notification
                global reminderqueue

                guid = reminderqueue.get(True, 60)
                Notification.delete(guid)

                reminderqueue.task_done()
            except Queue.Empty:
                continue
            except Exception as e:
                logging.info("worker traceback: " + Trace.exception_trace())
                logging.info("worker: %s" % str(e))

       
TABLE_NAME = "backup_task"
DRIVERS = {"fulllocal":FullLocalBackup,"fullssh":FullSSHBackup,"fullsmb":FullSMBBackup}
class BackupTask(object):
    """A periodic job as used by `Scheduler`."""
    
    def __init__(self):
        self.id = None
        self.interval = 1  # pause interval * unit between runs
        self.mode = None # Mode of backup task
        self.unit = "days"  # time units, e.g. 'minutes', 'hours', ...
        self.at_time = None  # optional time at which this job runs
        self.last_run = None  # datetime of the last run
        self.next_run = None  # datetime of the next run
        self.period = None  # timedelta between runs
        self._status = "Scheduled"
        self._last_backup = "Never"
        self.rotation = 1
        self.destination = None
        self.remoteserver = ""
        self.__implementation = None
        self.job = None
        
    @property
    def status(self):
        return self._status or "Scheduled"
    
    @property
    def last_backup(self):
        return self._last_backup or "Never"

    
    def __fill_from_row(self, row):
        (	self.id, self.mode, self.interval, 
                 self.unit, self.at_time, self.rotation, self.destination,
                 self._status, self._last_backup
                 ) = row
        return self    
    
    
    def __insert(self):
        self.id = CRUD.create(TABLE_NAME,
                              mode		= self.mode,
                              interval		= self.interval,
                              unit		= self.unit,
                              at_time		= self.at_time,
                              rotation		= self.rotation,
                              destination       = self.destination,
                              status            = self.status,
                              last_backup       = self.last_backup
                              )
        return self    
    
    @staticmethod
    def __retrieve(**kwargs):
        rows = CRUD.retrieve(TABLE_NAME, **kwargs)
        return [BackupTask().__fill_from_row(row) for row in rows]

    @staticmethod
    def __retrieve_one(**kwargs):
        row = CRUD.retrieve_one(TABLE_NAME, **kwargs)
        return BackupTask().__fill_from_row(row) if row else None
   
    @staticmethod
    def get_all():
        return BackupTask().__retrieve()

    @staticmethod
    def get_by_id(task_id):
        return BackupTask().__retrieve_one(id = task_id )    
    
    def __update(self):
        CRUD.update( TABLE_NAME,
                     {"id": self.id},
                     mode		= self.mode,
                     interval		= self.interval,
                     unit		= self.unit,
                     at_time		= self.at_time,
                     rotation		= self.rotation,
                     destination        = self.destination,
                     status             = self.status,
                     last_backup        = self.last_backup
                     )
        return self

    def __delete(self):
        CRUD.delete(TABLE_NAME, id=self.id)
        return self

    def save(self):
        if self.id:
            self.__update()
        else:
            self.__insert()

        return self

    def delete(self):
        #if not self.__implementation:
        #    self.__implement()      
        #self.__implementation.delete()
        self.__implementation = None
        return self.__delete()

    def run_now(self):
        logging.info("Backup Now!: %s"%self.id)
        global jobqueue
        jobqueue.put(self)
        #self()
        
    def __call__(self):
        if not self.__implementation:
            self.__implement()
        logging.info("Backup task %s started"%self.id)
        rows = CRUD.retrieve(TABLE_NAME,id=self.id)
        if not rows:
            logging.info("Backup task %s removed"%self.id)
            return schedule.CancelJob
         
        
        try:
            self._status = "Running"
            self.save()
            self.__implementation.backup()
            self._status = "Scheduled"
            self._last_backup = time.strftime('%X %x')
            self.save()
        except Exception as e:
            logging.exception("Error while running backup %s"%self.id)
        
    
    def test(self):
        if not self.__implementation:
            self.__implement()        
        return self.__implementation.test()
    
    def __implement(self):
        if self.__implementation:
            return
        self.__implementation = DRIVERS[self.mode](self.id)
        
    def list_revisions(self):
        if not self.__implementation:
            self.__implement()        
        return self.__implementation.list_revisions()
    
    def restore(self, revision):
        if not self.__implementation:
            self.__implement()              
        return self.__implementation.restore(revision)