import unittest
import random
from class_FileShare import FileShare

from uuid import uuid4

kw = {}
count = 20

class Test_FileShare( unittest.TestCase ):

    #@unittest.skip("")
    def test_000_db_backup(self):
        # here 2 methods are checked: all() and delete().

        # save current data to session
        file_share_db = FileShare.all()
        session["file_share_db"] = file_share_db

        # clean database (objects are still saved in session)
        [fs.delete() for fs in file_share_db]


    def test_001_saving(self):
        # generate dict for FileShare objects
        global kw
        kw = dict(["Share_%0.2i" % i, str(uuid4())] for i in range(count))

        for k,v in sorted(kw.items()):
            fs = FileShare()
            fs.name = k
            fs.guid = v
            fs.save()

        file_shares = FileShare.all()
        self.assertEqual(len(file_shares), count) # check new shares

        for fs in file_shares:
            fs.name = "%s - %s" % (fs.name, "updated")
            fs.save()

        file_shares = FileShare.all()
        self.assertEqual(len(file_shares), count) # check if there is still same updated shares

    def test_002_retrieving(self):
        # method all() seems working fine, so skip it here
        global kw

        fs_guid = random.choice(kw.values())
        fs = FileShare.get_by_guid(fs_guid)
        self.assertEqual(fs_guid, fs.guid) # same guids

        fs_id = fs.id
        fs = None
        fs = FileShare.get_by_id(fs_id)
        self.assertEqual(fs_id, fs.id) # same ids

        fs = FileShare.all(limit=5)
        self.assertEqual(len(fs), 5) # correct limit

        fs = FileShare.all(limit=8, offset=6)
        self.assertEqual(len(fs), 8) # correct limit & offset
        self.assertEqual(fs[0].name, "Share_%0.2i - updated" % 6) # correct offset

        try:
            fs = FileShare.all(offset=10)
            self.assertTrue(False) # if launced: incorrect
        except:
            self.assertTrue(True) # should be launched



    def test_003_objects_count(self):	pass

    def test_004_size(self): pass

    def test_005_date_creation(self): pass

    def test_006_date_modified(self): pass

    def test_007_exists(self): pass

    def test_008_rename(self): pass

    def test_009_users_acl(self): pass

    def test_010_groups_acl(self): pass

    def test_011_subjects_acl(self): pass


    @unittest.skip("")
    def test_FINAL_db_restore(self):
        # clean database
        file_share_db = FileShare.all()
        [fs.delete() for fs in file_share_db]

        # restore previous database

        file_share_db = session.get("file_share_db", default = [])
        for fs in file_share_db:
            # setting id to None signals that object is not persistent and must be inserted, not updated
            fs.id = None
            fs.save()