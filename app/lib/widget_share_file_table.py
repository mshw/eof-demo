from class_share_dir import ShareDir
import json
from operator import attrgetter
import localization
import cgi

EDIT_BUTTON = u"""<span class="project_badge red iconsweet"><a style='cursor:pointer;' title='%(edit)s'>8</a></span>"""

class WidgetShareFileTable:

    def __init__(self, dtable=None):
        loc = localization.get_lang()

        self.__dtable 			= dtable
        self.__dirs		 		= [] 	# set_data
        self.__files	 		= [] 	# set_data
        self.__parent_dir		= None 	# set_data
        self.__sort_column 		= 0
        self.__is_descending 	= False
        self.__folders_first 	= True
        self.__header			= [
            "guid", loc["fs_filename"], loc["fs_date"], loc["mf_functions"]
        ]

    def set_data(self, dirs=[], files=[], parent_dir=""):
        self.__dirs		= dirs
        self.__files	= files
        self.__parent_dir = parent_dir

    def set_sort_column(self, column_index):
        self.__sort_column = column_index if isinstance(column_index, int) else 0

    def set_descending(self):
        self.__is_descending = True

    def set_ascending(self):
        self.__is_descending = False

    def set_folders_first(self):
        self.__folders_first = True

    def set_files_first(self):
        self.__folders_first = False

    def set_header(self, header):
        loc = localization.get_lang()
        self.__header = header if isinstance(header, list) else [
            "guid", loc["fs_filename"], loc["fs_date"], loc["mf_functions"]
        ]

    @staticmethod
    def header_format():
        loc = localization.get_lang()
        return [ "guid", loc["fs_filename"], loc["fs_date"], loc["mf_functions"] ]

    @classmethod
    def dir_row_format(self, sd):
        loc = localization.get_lang()
        return [
            "<input type=\"checkbox\"/ value=\"%s\" name=\"g\">"%sd.guid, 					# key
            self.dir_string_format(sd), # folder title
            sd.date_created, 			# date
            "<span>%s</span>" % (	# functions available
                                         EDIT_BUTTON % {"edit":loc["edit"]}
                                         )
        ]

    @classmethod
    def file_row_format(self, sf):
        loc = localization.get_lang()
        return [
            "<input type=\"checkbox\"/ value=\"%s\" name=\"g\">"%sf.guid,
            self.file_string_format(sf),
            sf.date_created,
            "<span>%s</span>" % (	# functions available
                                         EDIT_BUTTON % {"edit":loc["edit"]}
                                         )
        ]

    @staticmethod
    def dir_string_format(sd):
        return u"""<div class="folder">
               <img src='img/7b54f544-3dd9-4f18-b949-714de441ba84.res'>&nbsp;&nbsp;%s
               </div>""" % cgi.escape(sd.name)

    @staticmethod
    def file_string_format(sf):
        return u"""<div class="file">
               <img src='img/9927e1a7-2474-41ec-b1b1-9c0bbffa6f4b.res'>&nbsp;&nbsp;%s
               </div>""" % cgi.escape(sf.name)

    def render(self, only_header = False):
        if only_header:
            self.__dtable.header 	= json.dumps(self.__header)
            return
        result = []
        loc = localization.get_lang()

        # sorting dirs and files by choosed attribute (sort_column)
        sort_column = ["name", "date_created"][self.__sort_column]
        dirs 	= sorted(
            self.__dirs,
            key = attrgetter( sort_column ),
            reverse = self.__is_descending
        )
        files 	= sorted(
            self.__files,
            key 	= attrgetter( sort_column ),
            reverse = self.__is_descending
        )

        # put link to the parent folder of current one (only in case if parent folder is not a top one)
        if not self.__parent_dir.is_top_folder:
            self.__parent_dir.parent.name = "[..]"
            result.append( self.dir_row_format(self.__parent_dir.parent) )

        if self.__folders_first:
            # directories
            [ result.append( self.dir_row_format(sd) ) for sd in dirs ]
            # files
            [ result.append( self.file_row_format(sf) ) for sf in files]

        else:
            # files
            [ result.append( self.file_row_format(sf) ) for sf in files]
            # directories
            [ result.append( self.dir_row_format(sd) ) for sd in dirs ]

        if result:
            self.__dtable.header 	= json.dumps(self.__header)
            self.__dtable.data		= json.dumps(result)
        else:
            self.__dtable.header 	= json.dumps(self.__header)
            self.__dtable.data = ""



