from class_db import Database
from threading import RLock
from indexers import normalize


class Metafield:

    @classmethod
    def get_all(self):
        query = """SELECT id, name, type_id,  `default`, `constraint`,  required, user_can_modify,  visible
					FROM metafield """
        rows = Database.maindb().fetch_all(query,)

        return [Metafield(row) for row in rows]

    @classmethod
    def get_all_metafield_in_doc_share(self):
        query = """SELECT id, doc_share_guid, metafield_id
					FROM metafield_in_doc_share """
        rows = Database.maindb().fetch_all(query,)

        return [{
            "id":row[0], "doc_share_guid":row[1], "metafield_id":row[2]
            } for row in rows]

    @classmethod
    def get_by_id(self, id):
        query = """SELECT id, name, type_id,  `default`, `constraint`,  required, user_can_modify,  visible
					FROM metafield
					WHERE id = ?"""
        row = Database.maindb().fetch_one(query, (str(id),))

        if row:
            return Metafield(row)
        return None

    @classmethod
    def get_additional_for_doc_share(self, doc_share, connector ):
        query = """SELECT metafield.id, metafield.name, metafield.type_id,  metafield.`default`, metafield.`constraint`, metafield.required, metafield.user_can_modify,  metafield.visible
					FROM metafield, metafield_in_doc_share
					WHERE metafield_in_doc_share.metafield_id=metafield.id
							and metafield_in_doc_share.doc_share_guid=?
							and metafield.user_can_modify != '-1'"""
        rows = Database.maindb().fetch_all(query,(str(doc_share),))

        return [Metafield(row, connector) for row in rows]

    @classmethod
    def get_required_metafields(self):
        query = """SELECT id, name, type_id,  `default`, `constraint`, required, user_can_modify,  visible
					FROM metafield
					WHERE user_can_modify = '-1'"""
        rows = Database.maindb().fetch_all(query,)

        return [Metafield(row) for row in rows]

    @classmethod
    def get_by_constraint(self, constraint):
        query = """SELECT id, name, type_id,  `default`, `constraint`, required, user_can_modify,  visible
					FROM metafield
					WHERE `constraint` = ?"""
        rows = Database.maindb().fetch_all(query, (constraint,))

        return [Metafield(row) for row in rows]

    @classmethod
    def get_by_doc_share(self, doc_share):
        query = """SELECT metafield.id, metafield.name, metafield.type_id,  metafield.`default`, metafield.`constraint`, metafield.required, metafield.user_can_modify,  metafield.visible
					FROM metafield, metafield_in_doc_share
					WHERE metafield_in_doc_share.metafield_id=metafield.id
							and metafield_in_doc_share.doc_share_guid=?
					ORDER BY  metafield.id ASC 		"""
        rows = Database.maindb().fetch_all(query,(doc_share,))

        return [Metafield(row) for row in rows]


    @classmethod
    def validate_autometa(self, connector):
        if not connector.checking_metadata("1"):
            connector.create_index("1")
        if not connector.checking_metadata("2"):
            connector.create_index("2", "date")
        if not connector.checking_metadata("3"):
            connector.create_index("3")


    def __init__(self, row=None, connector=None):
        self.__connector = None

        if row:
            self.id = row[0]
            self.name = row[1]
            self.type_id = row[2]
            self.default = "" if row[3] == "NULL" else row[3]
            self.constraint = "" if row[4] == "NULL" else row[4]
            self.required = row[5]
            self.user_can_modify = row[6]
            self.visible = row[7]

        else:
            self.id = -1
            self.name = ""
            self.type_id = ""
            self.default = ""
            self.constraint = "0"
            self.required = "1"
            self.user_can_modify = "1"
            self.visible = "1"
        if connector:
            self.set_connector(connector)
        global index_lock
        if "index_lock" not in globals():
            index_lock = RLock()

    def __str__(self):
        return "%s" % "".join(self.name+"_"+self.default)


    def save(self, force_insert=False):
        if self.name == "":
            return

        if force_insert:
            query = """INSERT INTO metafield(id, name, type_id,  `default`, `constraint`,  required, user_can_modify,  visible   )
							VALUES(?,?,?,?,?,?,?, ?)"""

            row = Database.maindb().commit( query,(self.id, self.name, self.type_id, self.default,
                                                   self.constraint, self.required, self.user_can_modify, self.visible))

            self.validate()

        elif self.id == -1:
            query = """INSERT INTO metafield(name, type_id,  `default`, `constraint`,  required, user_can_modify,  visible   )
							VALUES(?,?,?,?,?,?,?)"""

            row = Database.maindb().commit( query,(self.name, self.type_id, self.default,
                                                   self.constraint, self.required, self.user_can_modify, self.visible))
            self.id = row
            self.validate()
        else:
            query = """UPDATE metafield
						SET name=?, `default`=?, `constraint`=?, required=?, user_can_modify=?, visible=?
						 WHERE id=?	"""
            row = Database.maindb().commit( query,(self.name,  self.default ,
                                                   self.constraint, self.required, self.user_can_modify, self.visible, self.id ))
        return 	self.id



    def validate(self):
        str_id = str(self.id)
        conn = self.__get_connector()
        with index_lock:
            if not conn.checking_metadata([str_id]):
                conn.create_index(str_id)
            """if str(self.type_id) == "2":
				conn.create_index(str_id, "date")
			elif str(self.type_id) == "3":
				conn.create_index(str_id, "float")
			else:"""


        #raise Exception(str(len(row)))


    def delete(self):
        if self.user_can_modify != "-1":
            query = """DELETE FROM metafield_in_doc_share
						WHERE metafield_id=? """
            Database.maindb().commit(query, (self.id,))

            query = """DELETE FROM metafield
							WHERE id=? """
            Database.maindb().commit(query, (self.id,))

    def __get_connector(self):
        if self.__connector:
            return self.__connector
        else:
            raise Exception("'__connector'  is not set")

    def set_connector(self, connector):
        self.__connector = connector
    #def get_expressions(self):
    #	return Expression_type.get_expressions_by_meta_type(self.id)


class Metas_type:

    @classmethod
    def get_all(self):

        query = """SELECT id, name, description, regexp
						FROM `metas_type`
						ORDER BY name"""
        rows = Database.maindb().fetch_all( query, )
        return [Metas_type(row) for row in rows]

    @classmethod
    def get_by_id(self, mtid):
        query = """SELECT id, name, description, regexp
						FROM `metas_type`
						WHERE id=?
						"""
        row = Database.maindb().fetch_one( query, str(mtid), )
        if row:
            return Metas_type(row)
        return None


    def __init__(self, row=None):
        if row:
            self.id = row[0]
            self.name = row[1]
            self.description = row[2]
            self.regexp = row[3]
        else:
            self.id = 0
            self.name = ""
            self.description = ""
            self.regexp = ""


    def get_expressions(self):
        return Expression_type.get_expressions_by_meta_type(self.id)


class Expression_type:

    @classmethod
    def get_expressions_by_meta_type(self, mid):
        query = """SELECT `expr_type`.id, `expr_type`.name
						FROM `expr_type`
						INNER JOIN `expr_t_in_met_t`
							ON `expr_type`.id=`expr_t_in_met_t`.`expr_type_id`
						WHERE `expr_t_in_met_t`.`metas_type_id`=?
						ORDER BY name"""
        rows = Database.maindb().fetch_all( query, str(mid), )
        return [[row[1].encode("utf8"),row[1].encode("utf8")] for row in rows]


    def __init__(self, row=None):
        if row:
            self.id = row[0]
            self.name = row[1]

class Expression:

    @classmethod
    def get_all(self):
        query = """SELECT id, key, meta,  value, expr_type,  meta_type, search_guid
					FROM expression """
        rows = Database.maindb().fetch_all(query,)

        return [Expression(row) for row in rows]

    @classmethod
    def get_by_search_guid(self, search_guid):
        query = """SELECT id, key, meta,  value, expr_type,  meta_type, search_guid
					FROM expression
					WHERE search_guid=?"""
        rows = Database.maindb().fetch_all(query, (str(search_guid),))

        return [Expression(row) for row in rows]

    @classmethod
    def get_by_key(self, key):
        query = """SELECT id, key, meta,  value, expr_type,  meta_type, search_guid
					FROM expression
					WHERE key=?"""
        row = Database.maindb().fetch_one(query, (str(key),))

        if row:
            return Metafield(row)
        return None

    def __init__(self, row=None):
        if row:
            self.id = row[0]
            self.key = row[1]
            self.meta = row[2]
            self.value = row[3]
            self.expr_type = row[4] # min
            self.meta_type = row[5]
            self.search_guid = row[6]

        else:
            self.id = 0
            self.key = ""
            self.meta = ""
            self.value = ""
            self.expr_type = "" # min
            self.meta_type = ""
            self.search_guid = ""
                #self.key = row[0]
            #self.key = row[0]
            #self.key = row[0]

    def get_regexp(self):
        return Metas_type.get_by_id(self.meta_type).regexp

    def save(self):
        query = """INSERT INTO expression(key, meta,  value, expr_type,  meta_type, search_guid )
					VALUES(?,?,?,?,?,?)"""
        row = Database.maindb().commit( query,(self.key, self.meta,  self.value, self.expr_type,  self.meta_type, self.search_guid ))

    def delete(self):
        # delete
        query = """DELETE FROM expression
					WHERE id=? """
        Database.maindb().commit(query, (self.id,))
