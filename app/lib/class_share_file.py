from class_CRUD 		import CRUD
from class_ACL_proxy 	import ACL_proxy, ACL_record
from class_xml_action 	import xml_action
from class_xml_event 	import xml_event
from engine_core 		import workflow_queue
from uuid 		import uuid4
from datetime 			import date
import os, os.path, shutil,cgi
from storage import storage

TABLE_NAME = "share_file"

MIME_TABLE = {
    #basic types
    ".txt": "text/plain",
    ".rtf": "application/rtf",
    ".pdf": "application/pdf",
    ".xml": "application/xml",
    ".log": "text/plain", #"plain/text",
    ".eml": "application/email",
#	".msg": "text/plain",

#office
".doc": "application/msword",
".xls": "application/vnd.ms-excel",
".docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
".pptx": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
".odt": "application/vnd.oasis.opendocument.text",
".xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",

#progs
".py" : "text/plain"
}

class ShareFile(object):

    def __init__(self):
        self.id		= None
        self.parent_guid = ""
        self.guid	= ""
        self.name	= ""
        self.is_dir	= "0"
        self._date_created = ""
        self._complete_path = ""
        self._acl_subjects	= None # groups and users in one list, those who have access to this object
        self._parent 		= None
        self._is_top_folder	= None
        self._content		= None
        self._top_folder	= None

    # STORAGE
    @property
    def complete_path(self):
        if self._complete_path is "":
            self._complete_path = "/".join((self.parent_guid,self.guid))
        return self._complete_path

    @property
    def date_created(self):
        bad_dir_string = "---"
        if not self._date_created:
            full_path = self.complete_path
            if self.exists():
                self._date_created = date.fromtimestamp(os.stat(storage.abs_path(self.complete_path)).st_mtime ).strftime("%d / %m / %Y")
            else:
                self._date_created = bad_dir_string
        return self._date_created

    @property
    def acl_subjects(self):
        from class_user import User
        from class_group import Group
        if self._acl_subjects is None:
            acls = ACL_record.retrieve(object=self.guid)
            self._acl_subjects = [
                User.get_by_guid(acl.subject) or Group.get_by_guid(acl.subject) for acl in acls
            ]
        return self._acl_subjects

    @property
    def is_top_folder(self):
        self._is_top_folder = True if not self.parent_guid else False
        return self._is_top_folder

    @property
    def parent(self):
        if not self._parent:
            from class_share_dir 	import ShareDir
            self._parent = ShareDir.get_by_guid(self.parent_guid)
        return self._parent

    @property
    def content(self):
        if self._content == None:
            import indexers
            content_method = indexers.get_mime_indexer(self.mime())
            self._content = content_method(self.complete_path) if content_method else ""
        return self._content

    @property
    def top_folder(self):
        if self._top_folder == None:
            if self.parent_guid == "":	self._top_folder = self
            else:
                parent_dir = self.parent
                while parent_dir.parent:
                    parent_dir 	= parent_dir.parent
                self._top_folder = parent_dir
        return self._top_folder

    def __getitem__(self, key):
        return self.__dict__[key] if key in self.__dict__ else None

    def __str__(self):
        return self.name.encode("utf8")#u"( %s )" % self.name.encode("utf8")

    def __fill_from_row(self, row):
        (self.id, self.parent_guid, self.guid, self.name, self.is_dir) = row
        return self

    def fill_from_row(self, row):
        (self.id, self.parent_guid, self.guid, self.name, self.is_dir) = row
        return self

    def __insert(self):
        self.id = CRUD.create(TABLE_NAME,
                              parent_guid=self.parent_guid, guid=self.guid,
                              name=self.name, is_dir="0")
        return self

    def __update(self):
        CRUD.update(TABLE_NAME, {"id": self.id},
                    parent_guid=self.parent_guid,
                    name=self.name)
        return self

    @staticmethod
    def __retrieve(**kwargs):
        rows = CRUD.retrieve(TABLE_NAME, **kwargs)
        return [ShareFile().__fill_from_row(row) for row in rows]

    @staticmethod
    def __retrieve_one(**kwargs):
        row = CRUD.retrieve_one(TABLE_NAME, **kwargs)
        return ShareFile().__fill_from_row(row) if row else None

    def __delete(self):
        CRUD.delete(TABLE_NAME, guid=self.guid)
        return self

    def save(self, file_handler=""):
        if not self.name:
            raise Exception("FS_EMPTY_NAME")

        if self.id: 		# modifying file
            self.__update()
            if file_handler:
                self.add_file(file_handler)		# actually, previous file would be replaced
        elif file_handler=="no_file":
            if not self.guid:
                self.guid = str(uuid4()) 			# define guid of file
            self.is_dir = "0"
            self.__insert()
        elif file_handler: 	# adding new file
            self.guid = str(uuid4()) 			# define guid of file
            self.is_dir = "0"
            self.add_file(file_handler)			# saving file to disk
            self.__insert()						# add record  to db
        else:
            raise Exception("EMPTY_FILE_HANDLER")

        return self

    def delete(self):
        self.file_delete() 	# physical deletion of the file
        self.__delete()		# from Share_file db
        # acl table
        ACL_record.delete( object=self.guid )

    def clean_acls(self):
        ACL_record.delete( object=self.guid )
        self._acl_subjects = None


    @staticmethod
    def get_by_id(share_file_id):
        return ShareFile.__retrieve_one(id=share_file_id, is_dir="0")

    @staticmethod
    def get_by_guid(share_file_guid):
        return ShareFile.__retrieve_one(guid=share_file_guid, is_dir="0")

    @staticmethod
    def get_by_parent_guid(share_file_parent_guid):
        return ShareFile.__retrieve_one(guid=share_file_parent_guid, is_dir="0")


#	@staticmethod
#	def get_by_share_guid(share_file_share_guid):
#		return ShareFile().__retrieve_one(guid=share_file_share_guid, is_dir="0")

    @staticmethod
    def all(limit="", offset=""):
        return ShareFile.__retrieve(limit=limit, offset=offset, is_dir="0")

    def path(self):
        """ returns a full path (starting from top-level folder) of a current share dir """

        path = [self]
        while not path[-1].is_top_folder:
            path.append(path[-1].parent)
        path.reverse()
        return u"\\".join([e.name for e in path])

    # STORAGE
    def add_file(self, file_handler):
        storage.write(self.complete_path, file_handler)

    # STORAGE
    def file_delete(self):
        if not self.exists():
            return "FILE_NOT_FOUND" # file already deleted or wrong path
        storage.delete(self.complete_path)
        return True

    def exists(self):
        return storage.exists(self.complete_path)

    def handler(self):
        return storage.open(self.complete_path)

    def link(self): 		raise Exception("NOT_IMPLEMENTED")

    def abs_path(self):
        return storage.abs_path(self.complete_path)

    def move_to(self, folder):
        # physical move
        a = self.abs_path()
        b = storage.abs_path(folder.complete_path)
        shutil.move(a, b)

        # database move
        self.parent_guid = folder.guid
        self.save()

    def copy_to(self, folder):
        # database copy
        result_file = ShareFile()
        result_file.parent_guid = folder.guid
        result_file.name = self.name
        result_file.save("no_file")

        # physical copy
        a = self.abs_path()
        b = result_file.abs_path()
        shutil.copyfile(a, b)

        return result_file

    # STORAGE
    def download(self):
        if not self.exists():
            raise Exception("FILE_NOT_FOUND") # file already deleted or wrong path
        from flask import send_file
        ret =  send_file(storage.abs_path(self.complete_path), self.mime(), as_attachment=False, attachment_filename=cgi.escape(self.name.encode("utf8")))
        ret.headers.add('Content-Disposition', 'inline' , filename=cgi.escape(self.name.encode("utf8")))
        return ret

    def mark(self): raise Exception("NOT_IMPLEMENTED")

    def print_file(self): raise Exception("NOT_IMPLEMENTED")

    def rename(self): raise Exception("NOT_IMPLEMENTED")

    """ Return all users who has access to this file,
		including those who are in groups connected to it.
	"""
    def users(self):
        result = set(self.users_acl())
        for group in self.groups_acl():
            for user in group.get_users():
                result.add(user)
        return result

    def users_acl(self):
        from class_user import User
        return [subject for subject in self.acl_subjects if isinstance(subject, User)]

    def groups_acl(self):
        from class_group import Group
        return [subject for subject in self.acl_subjects if isinstance(subject, Group)]

    def subjects_acl(self):
        return [subject for subject in self.acl_subjects]

    def add_access_to(self, subject_guid, rule):
        new_acl = ACL_record()
        new_acl.subject = subject_guid
        new_acl.object 	= self.guid
        new_acl.rule 	= rule
        new_acl.save()

    def delete_access_for(self, subject_guid, rule=""):
        ACL_record.delete(
            subject=subject_guid, object=self.guid, rule=rule
        )

    def mime(self):
        return MIME_TABLE[ self.extension() ] if self.extension() in MIME_TABLE else "application/octet-stream"

    def extension(self):
        root,fextension = os.path.splitext( self.name )
        return fextension.lower()

    def size(self):
        """ returns a size of a current ShareFile in bytes """
        return storage.getsize(self.complete_path)

    def send_event(self, event_name, user_id, workflow_id=""):
        event = xml_action()
        if event_name == "insert":
            has_ocr = "True" if self.content else "False"
            event.set_data(	"file_inserted",
                                   user_id		= user_id,
                                   folder_id	= self.parent_guid,
                                   file_id		= self.guid,
                                   has_ocr		= has_ocr,
                                   content		= self.content,
                                   workflow_id = workflow_id
                                   )
        elif event_name == "delete":
            event.set_data( "file_deleted",
                            user_id = user_id,
                            file_id	= self.guid,
                            workflow_id = workflow_id
                            )
        else:
            raise Exception("Wrong event_name")

        event = xml_event(event.render())
        workflow_queue.put(event)