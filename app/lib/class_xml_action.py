# templates

NOTIFY = u'''
    <action name="notify">
    <workflow id="%(workflow_id)s"/>
    <attribute name="content"><!%(cdata)s[CDATA[%(content)s]%(cdata)s]></attribute>
    </action>
'''

DOWNLOAD = u'''
    <action name="downloadFile">
    <workflow id="%(workflow_id)s"/>
    <attribute name="mappedFolder">%(folder_id)s</attribute>
    <attribute name="fileId">%(file_id)s</attribute>
    </action>
'''

DELETE = u'''
    <action name="deleteFile">
    <workflow id="%(workflow_id)s"/>
    <attribute name="mappedFolder">%(folder_id)s</attribute>
    <attribute name="fileId">%(file_id)s</attribute>
    </action>
'''

TRIGGER = u'''
    <action name="%(action_name)s">
    <workflow id="%(workflow_id)s"/>
    <attribute name="fileId">%(file_id)s</attribute>
    </action>
'''

TRIGGER_FOLDER = u"""
    <action name="triggerFolder">
    <workflow id="%(workflow_id)s"/>
    <attribute name="folderId">%(folder_id)s</attribute>
    <attribute name="eventType">%(event_type)s</attribute>
    </action>
"""

UNLOCK_TRIGGER = u"""
    <action name="unlock_trigger">
    <workflow id="%(workflow_id)s"/>
    <attribute name="folderId">%(folder_id)s</attribute>
    <attribute name="eventType">%(event_type)s</attribute>
    </action>
"""

USER_DIALOG = u'''
<action name="question">
    <workflow id="%(workflow_id)s"/>
    <attribute name="answerType">%(answerType)s</attribute>
    <attribute name="question"><!%(cdata)s[CDATA[%(question)s]%(cdata)s]></attribute>
    <attribute name="idontKnow">%(idontKnow)s</attribute>
    <attribute name="renewDelay">%(renewDelay)s</attribute>
    <attribute name="choice"><answers>%(answers)s</answers></attribute>
</action>
'''

USER_DIALOG_ANSWER = u''' <answer value="%(value)s"><!%(cdata)s[CDATA[%(display)s]%(cdata)s]></answer> '''

EVENT_INSERT = u'''
<event name="insert">
    <user id="%(user_id)s"/>
    <workflow id="%(workflow_id)s"/>
    <attribute name="fileId">%(file_id)s</attribute>
    <attribute name="mappedFolder">%(folder_id)s</attribute>
    <attribute name="ocr">%(has_ocr)s</attribute>
    <attribute name="content"><!%(cdata)s[CDATA[%(content)s]%(cdata)s]></attribute>
</event>
'''

EVENT_DELETE = u'''
<event name="delete">
    <user id="%(user_id)s"/>
    <workflow id="%(workflow_id)s"/>
    <attribute name="fileId">%(file_id)s</attribute>
</event>
'''


EVENT_ANSWER = u'''
<event name="userAnswer">
    <user id="%(user_id)s"/>
    <workflow id="%(workflow_id)s"/>
    <attribute name="answerValue"><!%(cdata)s[CDATA[%(answer)s]%(cdata)s]></attribute>
</event>
'''

WORKFLOW_GLOBAL_VARIABLE = u'''<variable name="%(key)s"><!%(cdata)s[CDATA[%(value)s]%(cdata)s]></variable>
'''

WORKFLOW_QUESTION_ANSWER = u'''<answer name="$%(key)s"><!%(cdata)s[CDATA[%(value)s]%(cdata)s]></answer>
'''

METAFIELD_VALUE = u'''
<metafield id="%(id)s">
    <name><!%(cdata)s[CDATA[%(name)s]%(cdata)s]></name>
    <value><!%(cdata)s[CDATA[%(value)s]%(cdata)s]></value>
</metafield>
'''

xml_templates = {
    "notify" 		: [NOTIFY, 			["workflow_id", "content"]],
    "download_file" : [DOWNLOAD, 		["workflow_id", "folder_id", "file_id"]],
    "trigger" 		: [TRIGGER, 		["workflow_id", "action_name", "file_id"]],
    "user_dialog"	: [USER_DIALOG, 	["workflow_id", "answerType", "question","renewDelay","answers","idontKnow"]],
    "delete_file" 	: [DELETE, 			["workflow_id", "folder_id", "file_id"]],
    "trigger_folder": [TRIGGER_FOLDER,	["workflow_id", "folder_id","event_type"]],
    "unlock_trigger": [UNLOCK_TRIGGER,	["workflow_id", "folder_id","event_type"]],
    "file_inserted"	: [EVENT_INSERT,	["workflow_id", "user_id", 	"folder_id", "file_id", "has_ocr","content"]],
    "file_deleted"	: [EVENT_DELETE,	["workflow_id", "user_id", 	"file_id"]],
    "user_answer"	: [EVENT_ANSWER,	["workflow_id", "user_id", 	"answer"]],
    "workflow_global_variable"	: [WORKFLOW_GLOBAL_VARIABLE,	["key", "value"]],
    "workflow_question_answer"	: [WORKFLOW_QUESTION_ANSWER,	["key", "value"]],
    "metafield_value" 			: [METAFIELD_VALUE, 			["id", "name", "value"]]
}

class xml_action(object):

    def __init__(self):
        self.__key 	= ""
        self.__args = {}

    def set_data(self, template_key, **kwargs):
        if template_key in xml_templates:	self.__key = template_key
        else:	raise KeyError(template_key)

        if all(key in xml_templates[self.__key][1] and kwargs[key] is not None for key in kwargs.keys()):
            self.__args.update(kwargs)
        else:	raise Exception(u"bad params: %s"%unicode(kwargs))

    def render(self):
        if not self.__args and not self.__key:
            raise Exception(u"not enough data: %s , %s" % (unicode(self.__key), unicode(self.__args)))
        self.__args["cdata"] = u""
        return xml_templates[self.__key][0] % self.__args