from class_db import Database
from uuid import uuid4
from class_ACL_proxy import ACL_proxy, ACL_record

class SmartFolder(object):

    def __init__(self):
        self.id = None
        self.guid = str(uuid4())
        self.name = ""

        self.__sub_folders = None
        self._acl_subjects	= None # groups and users in one list, those who have access to this object

    def __init_from_db(self, row):
        self.id 	= row[0]
        self.guid 	= row[1]
        self.name 	= row[2]
        return self

    def __insert(self):
        #self.guid =
        query = """INSERT INTO smart_folder(name, guid)
					VALUES(?,?)"""
        rows = Database.maindb().commit(query,(self.name, self.guid),)

    def __update(self):
        query = """
			UPDATE 	smart_folder
			SET 	name=?
			WHERE 	guid=?
		"""
        rows = Database.maindb().commit(query,(self.name, self.guid),)

    def __delete(self):
        query = """DELETE FROM smart_folder WHERE id=? """
        Database.maindb().commit(query, (str(self.id),))

    def save(self):
        sf = self.get_by_name(self.name)
        if sf and sf.id != self.id:
            raise Exception("SAME_NAME_ERROR_SMART_FOLDER")

        if self.id:	self.__update()
        else:		self.__insert()

    def delete(self):
        if self.id:
            # delete metafield connected to this smart folder
            from class_metafield import Metafield
            smart_metas = Metafield.get_by_constraint(self.guid)
            if smart_metas:
                for sm in smart_metas:
                    sm.delete()

            # delete sub-folders
            for ssf in self.sub_folders:
                ssf.delete()

            # delete itself from database
            self.__delete()

    @property
    def sub_folders(self):
        if self.__sub_folders == None:
            self.__sub_folders = SmartSubFolder.get_by_parent_guid(self.guid)

        return self.__sub_folders

    @classmethod
    def get_by_id(self, id):
        query = """SELECT id, guid, name
					FROM smart_folder
					WHERE `id` = ?"""
        row = Database.maindb().fetch_one(query, (str(id),))
        return SmartFolder().__init_from_db(row) if row else None

    @classmethod
    def get_by_guid(self, guid):
        query = """SELECT id, guid, name
					FROM smart_folder
					WHERE `guid` = ?"""
        row = Database.maindb().fetch_one(query, (str(guid),))

        return SmartFolder().__init_from_db(row) if row else None

    @classmethod
    def get_by_name(self, name):
        query = """SELECT id, guid, name
					FROM smart_folder
					WHERE `name` = ?"""
        row = Database.maindb().fetch_one(query, (name,))

        return SmartFolder().__init_from_db(row) if row else None

    @classmethod
    def all(self):
        query = """SELECT id, guid, name
					FROM smart_folder
					ORDER BY `name`	"""
        rows = Database.maindb().fetch_all(query,)
#		raise Exception(str(rows))
        return [SmartFolder().__init_from_db(row) for row in rows]

    @property
    def acl_subjects(self):
        from class_user import User
        from class_group import Group
        if self._acl_subjects is None:
            acls = ACL_record.retrieve(object=self.guid)
            self._acl_subjects = [
                User.get_by_guid(acl.subject) or Group.get_by_guid(acl.subject) for acl in acls
            ]
        return self._acl_subjects

    def users_acl(self):
        from class_user import User
        return [subject for subject in self.acl_subjects if isinstance(subject, User)]

    def groups_acl(self):
        from class_group import Group
        return [subject for subject in self.acl_subjects if isinstance(subject, Group)]

    def subjects_acl(self):
        return self.acl_subjects #[subject for subject in self.acl_subjects]

    def add_access_to(self, subject_guid, rule):
        new_acl = ACL_record()
        new_acl.subject = subject_guid
        new_acl.object 	= self.guid
        new_acl.rule 	= rule
        new_acl.save()

    def delete_access_for(self, subject_guid, rule=""):
        ACL_record.delete(
            subject=subject_guid, object=self.guid, rule=rule
        )

    def copy_acls_to(self, share_object):
        new_acl = ACL_record()
        new_acl.object 	= share_object.guid
        new_acl.rule 	= ACL_proxy.rules['SEE']

        for acl in self.subjects_acl():
            new_acl.subject = acl.guid
            new_acl.save()


###################################################################


class SmartSubFolder(object):

    def __init__(self):
        self.id = None
        self.parent_guid = ""
        self.guid = str(uuid4())
        self.name = ""

        self.__parent_folder = None
        self._acl_subjects	= None # groups and users in one list, those who have access to this object

    def __hash__(self):
        return self.id

    def __init_from_db(self, row):
        self.id 	= row[0]
        self.parent_guid = row[1]
        self.guid 	= row[2]
        self.name 	= row[3]
        return self

    def __insert(self):
        query = """INSERT INTO smart_sub_folder(name, guid, parent_guid)
					VALUES(?,?,?)"""
        rows = Database.maindb().commit(query,(self.name, self.guid, self.parent_guid),)

    def __update(self):
        query = """
			UPDATE 	smart_sub_folder
			SET 	name=?
			WHERE 	guid=?
		"""
        rows = Database.maindb().commit(query,(self.name, self.guid),)

    def __delete(self):
        query = """DELETE FROM smart_sub_folder WHERE id=? """
        Database.maindb().commit(query, (str(self.id),))

    def save(self):
        ssf = self.get_by_name_and_parent_guid(self.name, self.parent_guid)
        if ssf and ssf.id != self.id:
            raise Exception("SAME_NAME_ERROR_SMART_FOLDER")

        if self.id:	self.__update()
        else:		self.__insert()

    def delete(self):
        if self.id:	self.__delete()

    @property
    def parent_folder(self):
        if self.__parent_folder == None:
            self.__parent_folder = SmartFolder.get_by_guid(self.parent_guid)

        return self.__parent_folder

    @classmethod
    def get_by_id(self, id):
        query = """SELECT id, parent_guid, guid, name
					FROM smart_sub_folder
					WHERE `id` = ?"""
        row = Database.maindb().fetch_one(query, (str(id),))
        return SmartSubFolder().__init_from_db(row) if row else None

    @classmethod
    def get_by_parent_guid(self, parent_guid):
        query = """SELECT id, parent_guid, guid, name
					FROM smart_sub_folder
					WHERE `parent_guid` = ?"""
        rows = Database.maindb().fetch_all(query, (str(parent_guid),))

        return [SmartSubFolder().__init_from_db(row) for row in rows]

    @classmethod
    def get_by_names(self, names):
        query = u"""SELECT id, parent_guid, guid, name
            FROM smart_sub_folder
            WHERE %s  """
        names_condition = u" OR ".join([u"(UPPER(name) = '%s')" % name.upper() for name in names])
        rows = Database.maindb().fetch_all(query%names_condition, ())
        #raise Exception(query%names_condition)

        return [SmartSubFolder().__init_from_db(row) for row in rows]

    @classmethod
    def get_by_guid(self, guid):
        query = """SELECT id, parent_guid, guid, name
					FROM smart_sub_folder
					WHERE `guid` = ?"""
        row = Database.maindb().fetch_one(query, (str(guid),))

        return SmartSubFolder().__init_from_db(row) if row else None

    @classmethod
    def get_by_name_and_parent_guid(self, name, parent_guid):
        query = """SELECT id, parent_guid, guid, lower(name)
					FROM smart_sub_folder
					WHERE lower(`name`) = ? and `parent_guid` = ?"""
        row = Database.maindb().fetch_one(query, (name.lower(), parent_guid,))

        return SmartSubFolder().__init_from_db(row) if row else None

    @classmethod
    def all(self):
        query = """SELECT id, parent_guid, guid,  name
					FROM smart_sub_folder
					ORDER BY `name`	"""
        rows = Database.maindb().fetch_all(query,)
        return [SmartSubFolder().__init_from_db(row) for row in rows]

    @property
    def parent_metafields(self):
        from class_metafield import Metafield
        pf = self.parent_folder
        smart_metas = Metafield.get_by_constraint(pf.guid)
        return smart_metas

    @property
    def acl_subjects(self):
        from class_user import User
        from class_group import Group
        if self._acl_subjects is None:
            acls = ACL_record.retrieve(object=self.guid)
            self._acl_subjects = [
                User.get_by_guid(acl.subject) or Group.get_by_guid(acl.subject) for acl in acls
            ]
        return self._acl_subjects

    def users_acl(self):
        from class_user import User
        return [subject for subject in self.acl_subjects if isinstance(subject, User)]

    def groups_acl(self):
        from class_group import Group
        return [subject for subject in self.acl_subjects if isinstance(subject, Group)]

    def subjects_acl(self):
        return self.acl_subjects #[subject for subject in self.acl_subjects]

    def add_access_to(self, subject_guid, rule):
        new_acl = ACL_record()
        new_acl.subject = subject_guid
        new_acl.object 	= self.guid
        new_acl.rule 	= rule
        new_acl.save()

    def delete_access_for(self, subject_guid, rule=""):
        ACL_record.delete(
            subject=subject_guid, object=self.guid, rule=rule
        )

    def copy_acls_to(self, share_object):
        new_acl = ACL_record()
        new_acl.object 	= share_object.guid
        new_acl.rule 	= ACL_proxy.rules['SEE']

        for acl in self.subjects_acl():
            new_acl.subject = acl.guid
            new_acl.save()