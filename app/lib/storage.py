import os,shutil
from werkzeug.datastructures import FileStorage
from ..config import CONFIG
def filepath(name):
    return os.path.join(os.getcwd(),CONFIG["STORAGE-DIRECTORY"], name)
class storage(object):

    def __init__(self):
        pass
    def open(self, filename,mode="rb"):
        return open(filepath(filename),mode)


    def readall(self, filename):
        f = open(filepath(filename),"rb")
        return f.read()

    def write(self, filename, content):

        if isinstance(content,FileStorage):
            if content.filename and hasattr(content.stream, "name") and os.path.isfile(content.stream.name):
                content.close()
                shutil.move(content.stream.name, filepath(filename))
            else:
                content.save(filepath(filename))
        else:
            f = open(filepath(filename),"wb")
            f.write(content)
            f.close()


    def getsize(self, filename):
        return os.path.getsize(filepath(filename))

    def delete(self, filename):
        os.remove(filepath(filename))

    def abs_path(self, filename):
        return filepath(filename)

    def exists(self, filename):
        return os.path.exists(filepath(filename))


    def mkdir(self, foldername):
        if not os.path.exists(filepath(foldername)):
            os.makedirs(filepath(foldername))


    def rmtree(self, foldername):
        shutil.rmtree(filepath(foldername))


storage = storage()