
from class_db import Database

class ACL:

    #"defined rules"#

    rules = {
        'READ'			: "read",
        'WRITE'			: "write",
        'USER_ADMIN'	: "user_admin",
        'ADMIN'			: "admin",
        'NO_NOTIFY'     : "no_notify",
    }

    @classmethod
    def get_acls(self, subject, object):
        add_rules = ""
        if ACL.rules:
            all_rules = ["acl.rule='%s'" % rule for rule in ACL.rules.values()]
            add_rules = "and (%s)" % " or ".join(all_rules)

        query = """
			SELECT acl.id, acl.subject , acl.object ,acl.rule
			FROM  acl
			WHERE acl.subject=?
			and acl.object=?
			%s
		""" % add_rules
        rows = 	Database.maindb().fetch_all(query, (str(subject),str(object)))

        ret_data = [ACL(row) for row in rows]

        query = """SELECT acl.id, acl.subject , acl.object ,acl.rule
					FROM  acl, user_in_group
					WHERE user_in_group.user_guid=?
								and user_in_group.group_guid=acl.subject
								and acl.object=?
								%s
							""" % add_rules
        rows =  Database.maindb().fetch_all(query, (str(subject),str(object),))

        for row in rows:
            ret_data.append(ACL(row))
        #raise Exception(str(len(rows)))
        return ret_data

    @classmethod
    def subject_can_read_object(self,subject_guid, object_guid):
        add_rules = ""
        if ACL.rules:
            all_rules = ["acl.rule='%s'" % rule for rule in ACL.rules.values()]
            add_rules = "and (%s)" % " or ".join(all_rules)

        query = """SELECT *
					FROM  acl
					WHERE acl.subject=?
							and acl.object=?
							%s """ % add_rules
        #raise Exception(query)
        row =  Database.maindb().fetch_all(query, (str(subject_guid),str(object_guid),))
        # ret_data = [ACL(row) for row in rows]
        if row:
            return True

        query = """SELECT *
					FROM  acl, user_in_group
					WHERE user_in_group.user_guid=?
								and user_in_group.group_guid=acl.subject
								and acl.object=?
								%s
							""" % add_rules
        row =  Database.maindb().fetch_all(query, (str(subject_guid),str(object_guid),))

        if row:
            return True
        return False

    @classmethod
    def subject_can_write_object(self,subject_guid, object_guid):
        query = """SELECT *
					FROM  acl
					WHERE acl.subject=?
							and acl.object=?
							and  (acl.rule='write' or acl.rule='user_admin') """
        row =  Database.maindb().fetch_all(query, (str(subject_guid),str(object_guid),))

        if row:
            return True

        query = """SELECT *
					FROM  acl, user_in_group
					WHERE user_in_group.user_guid=?
								and user_in_group.group_guid=acl.subject
								and acl.object=?
								and (acl.rule='write' or acl.rule='user_admin') """
        row =  Database.maindb().fetch_all(query, (str(subject_guid),str(object_guid),))

        if row:
            return True
        return False

    @classmethod
    def subject_can_admin_object(self, subject_guid, object_guid):
        query = """SELECT *
					FROM  acl
					WHERE acl.subject=?
							and acl.object=?
							and  acl.rule='user_admin'  """

        row =  Database.maindb().fetch_all(query, (str(subject_guid),str(object_guid),))

        if row:
            return True

        query = """SELECT *
					FROM  acl, user_in_group
					WHERE user_in_group.user_guid=?
								and user_in_group.group_guid=acl.subject
								and acl.object=?
								and acl.rule='user_admin'
							"""
        row =  Database.maindb().fetch_all(query, (str(subject_guid),str(object_guid),))

        if row:
            return True
        return False

    @classmethod
    def current_user_can_delete_object(self, object):
        from class_user import User
        user = User.current()
        return user.is_admin() or ACL.subject_can_admin_object(user.guid, object.guid)

    @classmethod
    def current_user_can_edit_object(self, object):
        from class_user import User
        user = User.current()
        return True if user.is_admin() else ACL.subject_can_admin_object(user.guid, object.guid)

    @classmethod
    def current_user_can_write_object(self, object):
        from class_user import User
        user = User.current()
        return user.is_admin() or ACL.subject_can_write_object(user.guid, object.guid)

    @classmethod
    def current_user_can_delete_share(self, object):
        from class_user import User
        user = User.current()
        return True if user.is_admin() else ACL.subject_can_admin_object(user.guid, object.guid)

    @classmethod
    def get_by_object(self, object_guid):
        query = """SELECT acl.id, acl.subject , acl.object ,acl.rule
					FROM  acl
					WHERE acl.object=?  """
        rows = 	Database.maindb().fetch_all(query, (str(object_guid),))
        return [ACL(row) for row in rows]

    @classmethod
    def get_by_subject(self, subject):
        query = """SELECT acl.id, acl.subject , acl.object ,acl.rule
					FROM  acl
					WHERE acl.subject=?  """
        rows = 	Database.maindb().fetch_all(query, (subject,))
        return [ACL(row) for row in rows]
    
    @classmethod
    def get_by_full_match(self,subj, object, acl_type):
        query = """SELECT acl.id, acl.subject , acl.object ,acl.rule
					FROM  acl
					WHERE acl.subject=? AND acl.object=? AND acl.rule=?"""
        row = 	Database.maindb().fetch_one(query, (str(subj),str(object),str(acl_type)))
        if row:
            return ACL(row)
        return None

    @classmethod
    def get_by_id(self, id):
        query = """SELECT acl.id, acl.subject , acl.object ,acl.rule
					FROM  acl
					WHERE acl.id=?  """
        row = 	Database.maindb().fetch_one(query, (str(id),))

        if row:
            return ACL(row)
        return None

############################################

    def __init__(self, row=None):
        if row:
            self.id = row[0]
            self.subject = row[1]
            self.object = row[2]
            self.rule = row[3]
        else:
            self.id = -1
            self.subject = ""
            self.object = ""
            self.rule = ""


    def save(self,):
        query = """SELECT *
					FROM  acl
					WHERE  acl.subject=? and acl.object=? and acl.rule=? """
        rows = 	Database.maindb().fetch_all(query, (self.subject, self.object, self.rule,))
        if rows:
            return

        query = """INSERT INTO acl(subject, object, rule)
						VALUES(?,?,?)"""
        row = Database.maindb().commit( query,( self.subject, self.object, self.rule ))



    def delete(self):
        query = """DELETE FROM acl
						WHERE id=? """
            #Database.maindb().commit(query, (guid,))
        row = Database.maindb().commit( query,( int(self.id),))
    @staticmethod
    def delete_acl(subj_guid, obj_guid,  acl_type):
        query = """DELETE	FROM  acl
					WHERE  acl.subject=? and acl.object=? and acl.rule=? """
        rows = 	Database.maindb().commit( query, (subj_guid, obj_guid, acl_type,))





