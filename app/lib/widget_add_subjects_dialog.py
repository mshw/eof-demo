import json
from io import StringIO
import localization

loc = localization.get_lang()

EMPTY_DATA_HEADER_FORMAT = """ <b>No data have been found</b> """

GROUP_HEADER_FORMAT = u""" %s:<br/>""" % loc["uag_groups"]
GROUP_STR_FORMAT = u""" <div
    class='deselected_group'
    onclick='execEventBinded("%(hpt_users_guid)s","custom",{"guid":"%(group_guid)s", "is_group":"%(is_group)s"})'
    >
    <b>%(group_name)s</b>&nbsp;
    </div>
    <br/>
"""
GROUP_SELECTED_STR_FORMAT = u"""  <div
    class='selected_group'
    onclick='execEventBinded("%(hpt_groups_guid)s","custom",{"guid":"%(group_guid)s", "is_group":"%(is_group)s"})'
    >
    <b>%(group_name)s</b>&nbsp;
    </div>
    <br/>
"""

USER_HEADER_FORMAT = u"""%s:<br/>""" % loc["uag_users"]
USER_STR_FORMAT	= u"""  <div
    class='deselected_user'
    onclick='execEventBinded("%(hpt_users_guid)s","custom",{"guid":"%(user_guid)s", "is_group":"%(is_group)s"})'
    >
    <i>%(user_email)s</i>&nbsp;
    </div>
    <br/>
"""
USER_SELECTED_STR_FORMAT	= u"""  <div
    class='selected_user'
    onclick='execEventBinded("%(hpt_groups_guid)s","custom",{"guid":"%(user_guid)s", "is_group":"%(is_group)s"})'
    >
    <i>%(user_email)s</i>&nbsp;
    </div>
    <br/>
"""

CSS_users = u"""
<style>
    .deselected_user
    {
    color: #000000;
    cursor: pointer;

    font-size: 12px !important;
    font-weight: bold !important;
    height: 20px !important;
    padding-left: 12px !important;
    padding-right: 12px !important;
    padding-top: 3px;
    border:1px solid #FFF;
    }
    .deselected_user:hover
    {
    border:1px solid #DDD;
    border-radius:6px;
    -moz-border-radius:6px;
    -webkit-border-radius:6px;
    behavior:url(/4b3615ee-bb32-4a1f-96b3-bc3adcc2e361.htc);
    }

    .selected_user
    {
    color: #FFFFFF;
    cursor: pointer;

    background: url("/e9e19d8b-1cac-457f-8683-4f967dec5deb.png") repeat-x scroll 0 0 #419301;
    border-radius: 5px 5px 5px 5px;

    font-size: 12px !important;
    font-weight: bold !important;
    height: 20px !important;
    padding-left: 12px !important;
    padding-right: 12px !important;
    padding-top: 3px;
    border:1px solid #FFF;
    }
</style>
"""
CSS_groups = u"""
<style>
    .deselected_group
    {
    color: #000000;
    cursor: pointer;

    font-size: 12px !important;
    font-weight: bold !important;
    height: 20px !important;
    padding-left: 12px !important;
    padding-right: 12px !important;
    padding-top: 3px;
    border:1px solid #FFF;
    }
    .deselected_group:hover
    {
    border:1px solid #DDD;
    border-radius:6px;
    -moz-border-radius:6px;
    -webkit-border-radius:6px;
    behavior:url(/4b3615ee-bb32-4a1f-96b3-bc3adcc2e361.htc);
    }


    .selected_group
    {
    color: #FFFFFF;
    cursor: pointer;

    background: url("/594dfe8c-53c6-47d6-a7f9-def7d6bcb4d7.png") repeat-x scroll 0 0 #E0A014;
    border-radius: 5px 5px 5px 5px;

    font-size: 12px !important;
    font-weight: bold !important;
    height: 20px !important;
    padding-left: 12px !important;
    padding-right: 12px !important;
    padding-top: 3px;
    border:1px solid #FFF;
    }
</style>
"""

class WidgetAddSubjectsDialog:

    def __init__(self,
                 dialog		= None, tabView		= None,
                 cnt_user	= None, cnt_group	= None,
                 hpt_users	= None, hpt_groups	= None,
                 btn_submit	= None, btn_cancel	= None
                 ):
        self.__dialog		= dialog 		# *
        self.__tabView		= tabView 		# *
        self.__cnt_user 	= cnt_user 		# *
        self.__cnt_group 	= cnt_group 	# *
        self.__hpt_users 	= hpt_users 	# * (used also for action select_subject)
        self.__hpt_groups 	= hpt_groups 	# * (used also for action deselect_subject)
        self.__btn_submit 	= btn_submit 	# *
        self.__btn_cancel 	= btn_cancel 	# *
        self.__users		= []
        self.__groups		= []
        self.__selected_users 	= []
        self.__selected_groups 	= []

    def set_data(self, users, groups):
        self.__users	= users
        self.__groups	= groups

    def add_user_to_selection(self, user):
        if user not in self.__selected_users:
            self.__selected_users.append(user)

    def add_group_to_selection(self, group):
        if group not in self.__selected_groups:
            self.__selected_groups.append(group)

    def remove_user_from_selection(self, user):
        if user in self.__selected_users:
            self.__selected_users.remove(user)

    def remove_group_from_selection(self, group):
        if group in self.__selected_groups:
            self.__selected_groups.remove(group)

    def get_selected_subjects(self):
        return self.__selected_users, self.__selected_groups

    def render(self):
        result_users, result_groups = StringIO(), StringIO()
        result_users.write(CSS_users)
        result_groups.write(CSS_groups)

        hpt_users_id, hpt_groups_id = self.__hpt_users.id.replace("-","_"), self.__hpt_groups.id.replace("-","_")

        for user in self.__users:
            if user in self.__selected_users:
                result_users.write(
                    USER_SELECTED_STR_FORMAT % {
                        "hpt_groups_guid": hpt_groups_id,
                        "user_email"	: unicode(user.email),
                        "user_guid"		: str(user.guid),
                        "is_group"		: "0"
                    }
                )
            else:
                #raise Exception(str(user.guid))
                result_users.write(
                    USER_STR_FORMAT % {
                        "hpt_users_guid": hpt_users_id,
                        "user_email"	: unicode(user.email),
                        "user_guid"		: str(user.guid),
                        "is_group"		: "0"
                    }
                )

        for group in self.__groups:
            if group in self.__selected_groups:
                result_groups.write(
                    GROUP_SELECTED_STR_FORMAT % {
                        "hpt_groups_guid": hpt_groups_id,
                        "group_name"	: group.name,
                        "group_guid"	: str(group.guid),
                        "is_group"		: "1"
                    }
                )
            else:
                result_groups.write(
                    GROUP_STR_FORMAT % {
                        "hpt_users_guid": hpt_users_id,
                        "group_name"	: group.name,
                        "group_guid"	: str(group.guid),
                        "is_group"		: "1"
                    }
                )

        self.__btn_cancel.action("setText",[loc["cancel"]])
        self.__btn_submit.action("setText",[loc["add"]])

        self.__hpt_users.action("setHTML",[result_users.getvalue()])
        self.__hpt_groups.action("setHTML",[result_groups.getvalue()])

        #self.__dialog.action("show",[300])

        result_groups.close()
        result_users.close()

    def destroy(self):
        self.__dialog.action("hide",[300])

    @staticmethod
    def group_header_format():
        return ["guid","email","in_groups"]

    @staticmethod
    def user_header_format():
        return ["guid","name","users_in"]

    @classmethod
    def group_row_format(self, group):
        return [group.guid, self.group_string_format(group), ""]

    @classmethod
    def user_row_format(self, user):
        return [user.guid, self.user_string_format(user), ""]

    @staticmethod
    def group_string_format(group):
        return """<div>
			<img src='/e2116dd1-ccac-4237-9d12-cba2038ccdbb.png'>&nbsp;&nbsp;%s
		</div>""" % group.name

    @staticmethod
    def user_string_format(user):
        return """<div>
			<img src='/a1b60a58-5f75-47fb-ab01-e32f2cfc4055.png'>&nbsp;&nbsp;%s
		</div>""" % user.name

