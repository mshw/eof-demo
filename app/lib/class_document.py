import datetime
from string import lower
from indexers import normalize
import os, os.path, shutil,mimetypes,cgi
from storage import storage

MIME_TABLE = {
    #basic types
    ".txt": "text/plain",
    ".csv": "text/plain",
    ".rtf": "application/rtf",
    ".pdf": "application/pdf",
    ".xml": "application/xml",
    ".log": "text/plain", #"plain/text",
    ".eml": "application/email",
#	".msg": "text/plain",

#office
".doc": "application/msword",
".xls": "application/vnd.ms-excel",
".docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
".pptx": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
".ppt": "application/vnd.openxmlformats-officedocument.presentationml.presentation",

".odt": "application/vnd.oasis.opendocument.text",
".xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",

#progs
".py" : "text/plain"
}

ICON_TABLE = {
    # doc, docx, xls, txt, bmp, jpg, msg, eml, tif, tiff, png
    #basic types
    ".txt": "txt",
    ".rtf": "doc",
    ".pdf": "pdf",
    ".xml": "txt",
    ".log": "txt",

    #office
    ".doc"	: "doc",
    ".xls"	: "xls",
    ".csv"	: "xls",
    ".docx"	: "docx",
    ".odt"	: "doc",
    ".ppt"	: "txt",
    ".pptx"	: "txt",
    ".pptm"	: "txt",
    ".bmp"	: "bmp",
    ".xlsx"	: "xls",
    ".jpg"	: "jpg",
    ".tif"	: "jpg",
    ".tiff"	: "jpg",
    ".png"	: "jpg",
    ".msg"	: "msg",
    ".eml"	: "eml"

}

class wDocument(object):

    def __init__(self, document):
        self.name 	= document.filename()
        self.guid 	= document.guid()
        self.doc 	= document

    def handler(self):
        return storage.open(self.complete_path())

    def delete(self):
        storage.delete(self.complete_path())

    def users_acl(self):
        from class_document_share import Document_share

        share = Document_share.get_by_guid(self.doc.ds_guid())
        if share:
            pass

    def users(self):
        pass

    def move_to(self, share_folder):
        pass

    def copy_to(self, share_folder):
        pass

    def get_by_guid(self):
        pass


class Document():

    @classmethod
    def from_xapian_document(self, doc):
        document = self(ds_guid = "...", filename = "...", user_email = "..." )
        document.load_from_xapian_document(doc)
        return document

    def __init__(self, ds_guid = None, filename = None, user_email = None ):
        self.__meta = {}
        self.__physical_name = None
        self.__fulltext = None
        self.__complete_path = ""

        self.__ds_guid = ds_guid
        self.__generate_autometa(filename, user_email)

    def complete_path(self):
        if self.__complete_path == "":
            self.__complete_path = "/".join((self.__ds_guid, self.__physical_name))
        return self.__complete_path


    def __generate_autometa(self,filename, user_email=None):
        from class_user import User
        self.__filename = filename
        self.__created_date = datetime.datetime.now().strftime("%Y-%m-%d")
        if user_email:
            self.__author = user_email
        else:
            self.__author = User.current().email



    def meta(self):
        return self.__meta


    def set_meta(self,meta):
        self.__meta=meta


    def absolute_physical_path(self):
        return self.complete_path()


#	def share_physical_path(self):
#		import os.path
#		return os.path.join(self.__ds_guid, self.__physical_name)


    def set_physical_name(self, guid):
        self.__physical_name = guid
        return self.__physical_name


    def guid(self):
        return self.__physical_name


    def filename(self,html_encode=False):
        if html_encode:
            return cgi.escape(self.__filename)
        else:
            return self.__filename

    def set_filename(self, value):
        self.__filename = value


    def author(self):
        return self.__author


    def fulltext(self, length=400):
        if length == 0:
            length = None
        import utils_old
        txt = utils_old.html_escape(self.__fulltext[:length])
        return txt


    def extension(self):
        root,fextension = os.path.splitext( self.filename() )
        return fextension.lower()


    def created_date(self):
        temp = datetime.datetime.strptime(self.__created_date, "%Y-%m-%d")
        result = temp.strftime("%d-%m-%Y")
        return result


    def mime(self):
        return MIME_TABLE[ self.extension() ] if self.extension() in MIME_TABLE else mimetypes.guess_type(self.filename())[0] or "application/octet-stream"

    def icon_class(self):
        extension = self.extension()
        return ICON_TABLE[ extension ] if extension in ICON_TABLE else "txt"


    def generate_fulltext(self):
#		from thread_indexer import index_document
#		index_document(self)

        import indexers
        index_function = indexers.get_mime_indexer( self.mime() )
        self.__fulltext = index_function( self.absolute_physical_path() ) #returns UTF-8 !!!
        self.__fulltext = normalize( self.__fulltext )
        return self.__fulltext

    def set_fulltext(self, fulltext):
        self.__fulltext = normalize( fulltext )

    def check_path(self):
        if not storage.exists(self.__ds_guid):
            storage.mkdir(self.__ds_guid)

    def delete_file(self):
        #TODO
        if self.__physical_name:
            storage.delete( self.absolute_physical_path() )
        else:
            pass


    def update_meta(self, meta):
        self.__meta.update(meta)


    def save_file(self, binary_data):
        import uuid
        self.check_path()
        self.delete_file()
        self.set_physical_name( str(uuid.uuid4()) )
        storage.write(self.complete_path(), binary_data)

#		f = open(self.complete_path(), "w")
#		f.write(binary_data)#.encode("utf8"))
#		f.close()


    def xapian_description(self):
        meta_copy = self.meta()
        xapian_dict = {
            "category": self.__ds_guid,
            "db_id": self.__physical_name,
            "text": self.__fulltext,
            "1": normalize( self.__filename.replace("_", " _ ")) + u"<span_divider>%s" % self.__filename ,
            "2": self.__created_date,
            "3": self.__author,

        }
        meta_copy.update( xapian_dict )
        return dict( [ (id,meta_copy[id]) for id in meta_copy if meta_copy[id] or id in ["category","db_id","text"] ] ) #filter empty metas


    def ds_guid(self):
        return self.__ds_guid

    def load_from_xapian_document(self, xapian_document):
        xapian_dict = dict( [ (meta,xapian_document.data[meta][0]) for meta in xapian_document.data ] )
        #raise Exception(str(xapian_dict))
        self.id = xapian_document.id
        self.__ds_guid = xapian_dict["category"]
        self.__physical_name = xapian_dict["db_id"]
        self.__fulltext =  xapian_dict["text"]
        #self.__fulltext_search_result  = xapian_document.summarise("text", maxlen=300)
        filename = xapian_dict["1"].split("<span_divider>")
        if len(filename) == 2:
            self.__filename = filename[1]
            pass
        else:
            self.__filename = filename[0]
        self.__created_date = xapian_dict["2"]
        self.__author = xapian_dict["3"]


        self.set_meta( dict( [ (meta,xapian_dict[meta]) for meta in xapian_dict if meta not in ["category", "db_id", "text", "1", "2", "3"]  ] ) )

    def sp( self, st ):
        #return st
        b, tail = st[:0], st
        while len(tail)>20:
            b += tail[:20] + "&shy;"
            tail = tail[20:]
        #raise Exception()
        return b+tail

    def set_mandatory_meta(self, fn, cd, author):
        self.__filename 	= fn
        self.__created_date = cd
        self.__author 		= author


