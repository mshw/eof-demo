# -*- coding: utf-8 -*-
import re
import cgi
from xml.sax.saxutils import escape
from datetime import datetime
from flask import request

def get_current_lang(lang):
        if lang == "en":
                import localization_en
                return localization_en.localization_dict

        if lang == "ru":
                import localization_ru
                return localization_ru.localization_dict

        if lang == "fr":
                import localization_fr
                return localization_fr.localization_dict

        import localization_en
        return localization_en.localization_dict


def has_wf_licence():
        return False

def isDevMode():
        from class_db import Database
        query = """
		SELECT value FROM config
		WHERE item='dev_mode'
	"""
        res = Database.maindb().fetch_one(query)
        if not res:
                query = """INSERT OR REPLACE INTO config (item, value ) VALUES ('dev_mode',?)"""
                Database.maindb().commit(query,(0,),)
                res = "0"
        result = True if int(res[0]) == 1 else False
        #raise Exception(result)
        return result

def setDevMode(value):
        from class_db import Database
        value = 1 if value else 0
        query = """INSERT OR REPLACE INTO config (item, value ) VALUES ('dev_mode',?)"""
        Database.maindb().commit(query,(value,),)


def get_env_argument(key):
        result = request.environ.get(key,"")
        #raise Exception(str(env)) # to look through env on appropriate params
        #result = env[key] if key in env else ""
        return result

def get_referer_url():
        result = get_env_argument("HTTP_REFERER")
        if "home_page" in result.lower():
                result = "/documents_share"
        return result

def get_logout_back_url():
        result = "%s?%s"%(request.path,request.environ["QUERY_STRING"].replace('&',"%26"))
        return result

def current_page_name():
        result = get_env_argument("SCRIPT_NAME").split("/")[1].split(".")[0]
        return result


def search_escape( text):
        html_escape_table = {
                "&": "&amp;",
                #'"': "&quot;",
                #"'": "&apos;",
                ">": "&gt;",
                "<": "&lt;"

        }

        return "".join(html_escape_table.get(c,c) for c in text)

def html_escape( text):
        html_escape_table = {
                "&": "&amp;",
                '"': "&quot;",
                "'": "&apos;",
                ">": "&gt;",
                "<": "&lt;"

        }

        return "".join(html_escape_table.get(c,c) for c in text)

def filename_escape(text):
        filename_escape_table = {
                "\\": "_",
                "/"	: "_",
                "|"	: "_",
                ":"	: "_",
                "*"	: "_",
                "?"	: "_",
                '"'	: "_",
                "<"	: "_",
                ">"	: "_",
                "&"	: "_",
                "#"	: "_",
                "'"	: "_",
                ","	: "_",
                ";"	: "_"
        }

        return "".join(filename_escape_table.get(c,c) for c in text)

def date2base(text=""):
        result = text
        re_obj = re.compile(r"(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}")

        for match in re_obj.finditer(result):
                #print match.group()
                datevalue = datetime.strptime(match.group(), "%d-%m-%Y").strftime("%Y-%m-%d")
                result = result.replace(match.group(), datevalue)
        return result

def date2norm(text):
        result = text

        re_obj = re.compile(r"(19|20)[0-9]{2}[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])")

        for match in re_obj.finditer(result):
                #print match.group()
                datevalue = datetime.strptime(match.group(), "%Y-%m-%d").strftime("%d-%m-%Y")
                result = result.replace(match.group(), datevalue)
        return result

def collect_from_args_to(req_args, args_list):
        return dict([k, req_args[k] if k in req_args and req_args[k] else ""] for k in args_list)

def getVal(args, a=""):
        result = ""
        if a.__class__ == int:
                try:
                        result = args and args[a] or ""
                except:
                        result = ""
        else:
                result = args and a in args and args[a] or ""
        return result


def scalar(key, defaultvalue = None, castto = None):
        return request.arguments.get(key, defaultvalue, castto)
#    value = request.arguments[ key ] if key in request.arguments else defaultvalue
#    return castto(value) if castto and value else value

def ssn_scalar(key, defaultvalue = None, castto = None):
        notdecoded = session[ key ] if key in session else defaultvalue
        value = notdecoded.decode("utf8") if isinstance( notdecoded, str ) else notdecoded
        return castto(value) if castto and value else value

def popup_growl(growl, ttl, msg):
        growl.title, growl.text, growl.active = ttl, msg, "1"


def encode_xml(xml):
        """ encodes an xml document so that it can be stored in the database (avoids <![CDATA[ ]]> issue) """
        return xml.replace("]]>", "]]]"+"]><!+"+"[CDATA[>")

def decode_xml(xml):
        """ decodes an xml document stored in the database to usual format """
        return xml.replace("]]]"+"]><!+"+"[CDATA[>", "]]>")


def str_to_html(str):
        """ converts a specified string so it can be displayed as html """
        return "<pre>%s</pre>" % escape(str)


def log_bugs(message):
        pass
        #log().info_bug(message)


ERROR_XML = "<error><code>%s</code><description>%s</description></error>"

ERROR_RESPONSES = {
        "00" : "User is not logged in",
        "01" : "Wrong login or password",
        "02" : "Argument is absent: '%s'",
        "03" : "Argument is bad or empty: '%s' : '%s'",
        "04" : "Access error: %s",
        "05" : "Creation error: %s",
        "06" : "Failed: %s",
        "13" : "Server error: %s",
        "99" : "Action not found",
}


def xml_error_response(code, *args):
        code = str(code)
        if      code == "00":
                result = ERROR_RESPONSES[code]
        elif 	code == "01":
                result = ERROR_RESPONSES[code]
        elif 	code == "02":
                result = ERROR_RESPONSES[code] % str(args[0])
        elif 	code == "03":
                result = ERROR_RESPONSES[code] % (str(args[0]),str(args[1]))
        elif 	code == "04":
                result = ERROR_RESPONSES[code] % str(args[0])
        elif 	code == "05":
                result = ERROR_RESPONSES[code] % str(args[0])
        elif 	code == "06":
                result = ERROR_RESPONSES[code] % str(args[0])
        elif 	code == "13":
                from class_trace import Trace
                result = ERROR_RESPONSES[code] % str(Trace.exception_trace())
        elif 	code == "99":
                result = ERROR_RESPONSES[code]
        
        else:
                result = "no code for this error: %s" % str(code)
        return ERROR_XML % (code, result)


def update_xapian_after_ssf_changed(ssf_guid, smart_folder, new_name="", op="rename"):
        from class_document_share 	import Document_share
        from xappy_indexconnector 	import indexconnector
        from xappy_fulltextsearch 	import fulltextsearch
        from class_smart_folder 	import SmartSubFolder
        from class_metafield 		import Metafield
        import cgi
        from class_db import Database

        query = """
		SELECT document_share.id, document_share.guid,  document_share.name
		FROM document_share
		WHERE document_share.guid in
		(
			SELECT doc_share_guid FROM metafield_in_doc_share
			WHERE metafield_in_doc_share.doc_share_guid = document_share.guid
			AND metafield_in_doc_share.metafield_id in
			(
				SELECT metafield.id FROM metafield
				WHERE metafield.`constraint` = ?
			)
		)
	"""

        rows = Database.maindb().fetch_all(query, (smart_folder.guid,))
        ds_shares = [Document_share(row) for row in rows] if rows else []

        ic = indexconnector()
        #raise Exception(str(len(ds_shares)))
        if ds_shares:

                metafields = Metafield.get_by_constraint(smart_folder.guid)
                meta_list = dict([(str(m.id), m) for m in metafields if m.type_id == 5])
                #raise Exception(str(meta_list))
                for ds in ds_shares:
                        res = []
                        search_result = fulltextsearch().search(ds.guid, [],  0,  100000, sortby="2")
                        for doc in search_result:
                                res.append(doc)
                        docs, docs_count = res,  search_result.matches_estimated

                        if docs_count == 0: continue

                        for doc in docs:
                                # here to change meta with smart index

                                docinf = {}
                                for meta in doc.data:
                                        docinf[meta] = doc.data[meta][0]
                                        if str(meta) not in meta_list:
                                                continue

                                        meta_id = doc.data[meta][0].split("<span_divider>")[0]
                                        if meta_id == ssf_guid:
                                                if op == "delete":
                                                        docinf[meta] = u""
                                                else:
                                                        docinf[meta] = u"%s<span_divider>%s" % (meta_id, new_name)



                                ic.replace_doc(docinf, doc)
        ic.flush()
        ic.close()