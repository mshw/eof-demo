
from class_CRUD import CRUD

rules =	{
    'READ'			: "read",
    'WRITE'			: "write",
    'USER_ADMIN'	: "user_admin",
    'ADMIN'			: "admin",
    'SEE'			: "see",
    'NO_NOTIFY'     : "no_notify"
}

access_types = {
    "DS"	: ['read', 'write',	'user_admin', "no_notify"],
    "FS"	: ['read', 'write',	'user_admin'],
    "F"		: ['read', 'write',	'user_admin'],
    "FS_files"	: ['see'],
    "SF"	: ['read', 'write',	'user_admin'],
}


class ACL_proxy:

    rules = rules
    access_types = access_types

    def __init__(self):
        self.is_admin 	= None
        self.acls		= {}

    def has_access(self, subject_guid, object_guid, rule):
        if self.is_admin: return True

        if object_guid not in self.acls:
            user_acls = ACL_record.retrieve(
                subject	= subject_guid,
                object	= object_guid
            )
            self.acls[object_guid] = [acl.rule for acl in user_acls] # if user_acls else []

        func = ACL_proxy.access_dict[rule]
        user_acls = func(self.acls[object_guid])

        return user_acls


    def f_read(acls):
        return any(rule in acls for rule in [
            rules['READ'], 	rules['WRITE'], rules['USER_ADMIN'], rules['ADMIN']
        ])


    def f_write(acls):
        return any(rule in acls for rule in [
            rules['WRITE'], rules['USER_ADMIN'], rules['ADMIN']
        ])


    def f_user_admin(acls):
        return any(rule in acls for rule in [
            rules['USER_ADMIN'], rules['ADMIN']
        ])


    def f_admin(acls):
        return rules['ADMIN'] in acls

    def f_see(acls):
        return rules['SEE'] in acls

    def f_no_notify(acls):
        return rules['NO_NOTIFY'] in acls

    #"methods of checking on each acl rule"#
    access_dict = {
        rules['READ'] 		: f_read,
        rules['WRITE'] 		: f_write,
        rules['USER_ADMIN']	: f_user_admin,
        rules['ADMIN']		: f_admin,
        rules['SEE']		: f_see,
        rules['NO_NOTIFY']  : f_no_notify
    }

class ACL_record:

    def __init__(self, row=""):
        if row:
            self.id 		= row[0]
            self.subject 	= row[1]
            self.object 	= row[2]
            self.rule 		= row[3]
        else:
            self.id = -1
            self.subject = ""
            self.object = ""
            self.rule = ""

    def __str__(self):
        return "%s__%s :: %s" % (self.subject[:4], self.object[:4], self.rule)

    def create(self):
        #if self.id != -1:
        #	raise Exception("This ACL is already exists")
        row = CRUD.retrieve('acl',
                            subject=self.subject, object=self.object, rule=self.rule
                            )
        if row:
            self.id = row[0]
            return self #raise Exception("This ACL is already exists")

        id = CRUD.create('acl',
                         subject=self.subject, object=self.object, rule=self.rule
                         )
        self.id = id
        return self

    def save(self):
        """ Alias for self.create(). """
        return self.create()

    @classmethod
    def retrieve(self, **kwargs):
        rows = CRUD.retrieve("acl", **kwargs)
        return [self(row) for row in rows]

    def update(self):
        row = CRUD.retrieve('acl',
                            subject=self.subject, object=self.object, rule=self.rule
                            )
        if row:	raise Exception("This ACL is already exists")

        CRUD.update("acl",
                    { 'id' : self.id },
                    subject = self.subject,
                    object = self.object,
                    rule = self.rule
                    )

    @classmethod
    def delete(self, **kwargs):
        CRUD.delete("acl", **kwargs)

    @classmethod
    def get_acls(self, subject, object):
        from class_group import Group
        subjects = [subject]
        for g in Group.get_user_groups(subject):
            subjects.append(g.guid)
#		raise Exception(str(subjects))

        result = self.retrieve(subject=subjects, object=object)
        return result