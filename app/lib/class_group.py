
from class_db import Database
from class_ACL import ACL
from class_document_share import Document_share
from class_ACL_proxy import ACL_proxy
import json

class Group( object ):

    rules = ACL_proxy.rules

    @classmethod
    def get_by_guid(self, guid):
        query = """SELECT id, guid,  name
					FROM `group`
					WHERE `guid` = ?"""
        row = Database.maindb().fetch_one(query, (str(guid),))

        if row:
            return Group(row)
        return None

    @classmethod
    def get_by_name(self, name):
        query = """SELECT id, guid, name
					FROM `group`
					WHERE `name` = ?"""
        row = Database.maindb().fetch_one(query, (str(name),))

        if row:
            return Group(row)
        return None

    @classmethod
    def get_user_groups(self, user_guid):
        query = """SELECT `group`.id, `group`.guid, `group`.name
					FROM `group` , `user_in_group`
					WHERE user_in_group.user_guid=?
						and user_in_group.group_guid = `group`.guid
					ORDER BY `group`.name"""
        rows = Database.maindb().fetch_all( query,(str(user_guid),))
        return [Group(row) for row in rows]



    @classmethod
    def get_all(self ):
        query = """SELECT `group`.id, `group`.guid, `group`.name
					FROM `group`
					ORDER BY `group`.name"""
        rows = Database.maindb().fetch_all( query,)

        return [Group(row) for row in rows]


    def __init__(self, row=None):
        if row:
            self.id = row['id']
            self._guid = row['guid']
            self._name = row['name']
            self.dn 	= ''

        else:
            self.id = -1
            self._guid = ""
            self._name = ""
            self.dn = ''
        self.ldap_users = []

    @property
    def name( self ):
        return self._name

    @name.setter
    def name( self, value ):
        self._name = value[0] if type( value ) == type( [] ) else value
    
    @property
    def guid( self ):
        return self._guid

    @guid.setter
    def guid( self, value ):
        self._guid = value[0] if type( value ) == type( [] ) else value    

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return self.id == other.id and self.name == other.name

    def __hash__(self):
        return self.id

    def save(self, validate = True):

        if validate:
            self._validate_name()

        if self.exist():
            return

        if self.name != "":
            import uuid

            if not self.guid:
                self.guid =  str(uuid.uuid4())
            query = """INSERT INTO `group`(name, guid)
						VALUES(?,?)"""
            row = Database.maindb().commit( query,(self.name,  self.guid ))

    def _validate_name(self):
        self.name =  self.name.lower()
        query = """SELECT *
						FROM `group`
						WHERE name= ? """
        row = Database.maindb().fetch_one( query, (self.name,) )
        if row :
#			return
            raise Exception("GROUP_EXISTS")

    def delete(self,):
        for acl in ACL.get_by_subject(self.guid):
            acl.delete()

        query = """DELETE FROM `user_in_group`
					WHERE group_guid=? """
        res = Database.maindb().commit(query, (self.guid,))
#		raise Exception( self.guid, res )

        query = """DELETE FROM `group`
					WHERE guid=? """
        Database.maindb().commit(query, (self.guid,))


    def get_users(self):
        from class_user import User
        return User.get_all_by_group(self.guid)


    def get_users_list(self):
        users = [(sp.email,sp.id) for sp in self.get_users()]
        return str(sorted(set(users)))


    def add_user(self, user_guid):
        from class_user import User
        if isinstance(user_guid,User):
            user = user_guid
        else:
            user = User().get_by_guid(user_guid)
        if user:
            query = """SELECT *
					FROM user_in_group
					WHERE user_guid=? AND group_guid=?"""
            row = Database.maindb().commit( query,(str(user.guid), str(self.guid) ))

            if row:
                return

            query = """INSERT INTO user_in_group(user_guid, group_guid)
					VALUES(?,?)"""
            rows = Database.maindb().commit( query,(str(user.guid), str(self.guid) ))


    def delete_user(self, user_guid):
        if user_guid =="all":
            self.__delete_all_users()
        else:
            from class_user import User
            user = User().get_by_guid(user_guid)
            if user:
                query = """DELETE FROM user_in_group
							WHERE user_guid=? AND group_guid=?"""
                rows = Database.maindb().commit( query,(str(user.guid), str(self.guid) ))


    def __delete_all_users(self):
        query = """DELETE FROM user_in_group
							WHERE  group_guid=?"""
        rows = Database.maindb().commit( query,(str(self.guid) ))


    def add_access_to(self, share, acl_type):
        if share:
            acl = ACL()
            acl.subject = self.guid
            acl.object = share.guid
            acl.rule = acl_type
            acl.save()

    def delete_access_to(self, share, acl_type):
        if share:
            ACL.delete_acl(self.guid, share.guid,  acl_type)

    #get_documents_shares
    def get_documents_shares(self):
        query = """SELECT document_share.id, document_share.guid,  document_share.name
					FROM document_share, acl
					WHERE acl.subject = ?
							and (acl.rule = 'read' or acl.rule = 'write'  )
							and document_share.guid = acl.object
					ORDER BY document_share.name
					"""
        rows = Database.maindb().fetch_all(query, (str(self.guid),))
        return [Document_share(row) for row in rows]

    def get_type_access_to(self, share):
        if share:
            return ACL.get_acl_rule(self.guid, share.guid )

    @classmethod
    def get_ldapgroups( self, ldapconnection, config ):

        filter = '(&(objectClass=%s))' % config[ 'group_class' ][-1]

        groups = ldapconnection.search( config[ "base_dn" ], filter, 0 )

        return [ self().fill_from_ldapobject( config, group ) for group in groups ]


    def get_ldapusers( self, ldapconnection, config ):
        """
        """
        from class_user import User
#		# group.users:
        users_in_group = config[ 'users_in_group' ]
        users = []
        if users_in_group:
            ldapobject = ldapconnection.get_by_dn( self.dn )
            user_dns = ldapobject.attributes.get( users_in_group, [] )
#			raise Exception( user_dns )
            for dn in user_dns:
                ldapuser = ldapconnection.get_by_dn( dn )
#				raise Exception( ldapuser.dn )
                if ldapuser:
                    user = User().fill_from_ldapobject( config, ldapuser )
                    users.append( user )
        return users


    def fill_from_ldapobject( self, config, ldapobject ):
        self.dn = ldapobject.dn

        name_attr = config[ 'group_name' ]
        self.name = ldapobject.attributes[ name_attr ]

        #guid_attr = config[ 'group_guid' ][-1]
        #self.guid = ldapobject.attributes.get( guid_attr, '' )
        
        self.ldap_users = ldapobject.attributes.get(config[ 'users_in_group' ],[])
        return self

    def exist( self ):
        query = """SELECT id FROM `group` WHERE name = ?"""
        tuple = ( self.name, )
        id = Database.maindb().fetch_one( query, tuple )
        return id[0] if id else None

    @classmethod
    def get_relation_list( self ):
        query = """SELECT * FROM `user_in_group`"""
        row = Database.maindb().commit( query, () )
        return row

    def save_ldapgroup( self, dn_map ):
        import uuid

        if not self.name:
            return

        exist = self._exist()
        if exist:
            self.id = exist[0]
            self.guid = exist[1]
            return self

        self.guid =  str(uuid.uuid4())
        query = """INSERT INTO `group`(name, guid)
                        VALUES(?,?)"""

        tuple = ( self.name, self.guid, )
        id = Database.maindb().commit( query, tuple )
        self.id = id if id else -1
        from class_user import User
        
        for user in self.ldap_users:
            if user in dn_map:
                self.add_user(dn_map[user])
        return self


    def _exist(self):
        self.name =  self.name.lower()
        query = """SELECT id, guid
						FROM `group`
						WHERE name= ? """
        row = Database.maindb().fetch_one( query, (self.name,) )
        if row :
            return row


    @classmethod
    def get_by_id( self, id ):
        query = """SELECT id, guid, name FROM `group` WHERE id = ?"""
        tuple = (id,)
        row = Database.maindb().fetch_one( query, tuple )
        return self( row )

    @classmethod
    def clear_ldapsubjects( self, config_id = None ):
        from class_user import User
        users = User.get_ldapusers_from_db( config_id = config_id )
        for user in users:
            groups = user.get_groups()
            for group in groups:
                group.delete()
            user.delete()



    @classmethod
    def get_ldapgroups_from_db( self ):
        ids = self.getLDAPGroupMarker()
        groups = []
        for id in ids:
            group = self.get_by_id( id )
            if group:
                groups.append( group )
        return groups

    @classmethod
    def setLDAPGroupMarker( self, ids ):
        query = """INSERT OR REPLACE INTO config (item, value ) VALUES (?,?)"""
        Database.maindb().commit(query,('ldapgroups', json.dumps(ids)))

    @classmethod
    def getLDAPGroupMarker( self ):
        result = []
        try:
            query = """SELECT value FROM `config` WHERE item=?"""
            result = Database.maindb().fetch_one(query, ('ldapgroups',) )
            result = json.loads(str(result[0])) if result else []
        except Exception as e:
            pass
        return result