import unittest
import random
from class_ShareFile import ShareFile
from uuid import uuid4

kw = {}
count = 20

class Test_ShareFile( unittest.TestCase ):

    #@unittest.skip("")
    def test_000_db_backup(self):
        # here 2 methods are checked: all() and delete().

        # save current data to session
        share_file_db = ShareFile.all()
        session["share_file_db"] = share_file_db

        # clean database (objects are still saved in session)
        [sf.delete() for sf in share_file_db]

    def test_001_test_file_load(self):
        pass

    #@unittest.skip("")
    def test_FINAL_db_restore(self):
        # clean database
        share_file_db = ShareFile.all()
        [sf.delete() for sf in share_file_db]

        # restore previous database

        share_file_db = session.get("share_file_db", default = [])
        for sf in share_file_db:
            # setting id to None signals that object is not persistent and must be inserted, not updated
            sf.id = None
            sf.save()