import threading
import sqlite3
from ..config import CONFIG
class Database:

    #__maindb = None
    __maindb = threading.local()
    __customdb = threading.local()

    @classmethod
    def maindb(self):
        conn = sqlite3.connect(CONFIG["DB-DIRECTORY"]+r"/db_eof.sqlite", timeout=30.0)
        conn.row_factory = sqlite3.Row
        conn.create_function("UPPER_UNICODE", 1, self.sqlite_upper)

        conn.create_collation("NOCASE", self.sqlite_nocase_collation)
        if getattr( self.__maindb, "current_db", None ) is None:
            self.__maindb.current_db =  Database(conn)

        """	Redefining UPPER method in sqlite into python method.
			Sqlite UPPER is not working with utf-8. So to process such text I redefined it.
		"""


        return self.__maindb.current_db

    """ Redefined UPPER for sqlite. Turns the string literals into capital.
	"""
    @staticmethod
    def sqlite_upper(value):
        return value.upper()

    @staticmethod
    def sqlite_nocase_collation(value1_, value2_):
        return cmp(value1_.decode('utf-8').lower(), value2_.decode('utf-8').lower())

    @classmethod
    def customdb(self):

        conn = sqlite3.connect(CONFIG["DB-DIRECTORY"]+r"/db_custom_wf.sqlite")
        conn.row_factory = sqlite3.Row
        if getattr( self.__customdb, "current_db", None ) is None:
            self.__customdb.current_db =  Database(conn)

        return self.__customdb.current_db

    def __init__(self, db=None):
        self.__db = db

    def fetch_one(self, sqlquery, arguments = None):
        if arguments is None:
            arguments = []
        cur = self.__db.execute(sqlquery, arguments)
        return cur.fetchone()

    def fetch_all(self, sqlquery, arguments = None):
        if arguments is None:
            arguments = []
        cur = self.__db.execute(sqlquery, arguments)
        return cur.fetchall()

    def execute(self, sqlquery, arguments = None):
        if arguments is None:
            arguments = []
        return self.__db.execute(sqlquery, arguments)

    def commit(self, sqlquery, arguments = None):
        if arguments is None:
            arguments = []
        cur = self.__db.execute(sqlquery, arguments)
        self.__db.commit()
        return cur.lastrowid
