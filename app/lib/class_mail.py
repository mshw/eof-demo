import re,logging
from ..config import CONFIG
from class_config import Config
from json import loads
# Import smtplib for the actual sending function
import smtplib

# Here are the email package modules we'll need
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

from flask import request
"""

* Classes for working with email *

Usage example:

from class_mail import Mailer

# create mailer
mailer = Mailer()
# send message
mailer.send(from_email,to_email,subject_text)

"""

class Mailer:
    @classmethod
    def send(self, from_email,to_email, subject, mail_text,attachments=[]):
        msg = Message()
        msg.set_to_email(to_email)
        msg.set_from_email(from_email or "noreply@%s"%request.environ["HTTP_HOST"].split(":")[0])
        msg.set_subject(subject)
        msg.set_text(mail_text)    
        if attachments:
            msg.set_attachments(attachments)
        try:
            cfg = Config()
            smtp_cfg = loads(cfg.get_value("smtpconfig"))
            if smtp_cfg["ssl"] == "1":
                s = smtplib.SMTP_SSL(smtp_cfg["server"],int(smtp_cfg["port"]))
            else:
                s = smtplib.SMTP(smtp_cfg["server"],int(smtp_cfg["port"]))
            s.login(smtp_cfg["login"],smtp_cfg["passw"])
            if msg.get_attachments():
                s.sendmail(msg.get_from_email(), msg.get_to_email(), msg.get_as_multipart())
            else:
                s.sendmail(msg.get_from_email(), msg.get_to_email(), msg.get_as_html())
            return True
        except Exception as e:
            logging.exception("Error while sending mail%s"%to_email)
            #print unicode(e)
            return False

class Message(object):

    def __init__(self):

        self._from_email = None
        self._to_email = None
        self._subject = None
        self._text = None
        self._attachments = None

    def check_email(self, email):
        """ Checks if a specified email's format is valid """

        if len(email)>=6 or len(email)<=100:
            emailf=email.find('@')
            if not emailf==-1:
                email_parts = email.split("@")

                if len(email_parts) != 2:
                    return False
                else:
                    if email_parts[0]=="" or email_parts[1]=="":
                        return False
                    else:
                        match = re.match(r"\b[A-Za-z0-9._+-]{1,100}", email_parts[0])
                        if email_parts[0][-1]==".":
                            return False
                        else:
                            if email_parts[1][0]==".":
                                return False
                            else:
                                if not match or len(match.group())<len(email_parts[0]):
                                    return False
                                else:
                                    if not email_parts[1].find(".")==-1:
                                        match = re.match(r"[A-Za-z0-9\-\.]+", email_parts[1])
                                        if not match or len(match.group())<len(email_parts[1]):
                                            return False
                                        else:
                                            match = re.search(r"\.\.", email_parts[1])
                                            if not match:
                                                tchk = email_parts[1][::-1].find(".")
                                                if tchk>=2 and tchk<=4:
                                                    match = re.match(r"\.[A-Za-z]{2,4}\b", email_parts[1][-(tchk+1):])
                                                    if not match or len(match.group())<len(email_parts[1][-(tchk+1):]):
                                                        return False
                                                    else:
                                                        return True
                                                else:
                                                    return False
                                            else:
                                                return False
                                    else:
                                        return False
            else:
                return False
        else:
            return False

    def get_default_from_email(self):
        try:
            cfg = Config()
            smtp_cfg = loads(cfg.get_value("smtpconfig"))        
            return smtp_cfg["login"]# "noreply@" + "eof.com"
        except:
            return "noreply@bgsoft.fr"


    def get_from_email(self):
        return "%s <%s>"%(self._from_email,self.get_default_from_email())

    def set_from_email(self, email):

        #if not self.check_email(email):
        #    raise MailFormatError("Invalid email format.")

        self._from_email = email


    def get_to_email(self):
        return self._to_email

    def set_to_email(self, email):

        if isinstance(email, (list, tuple)):
            if not all(self.check_email(m) for m in email):
                raise MailFormatError("Invalid email format.")
        else:
            if not self.check_email(email):
                raise MailFormatError("Invalid email format.")

        self._to_email = email


    def get_subject(self):
        return self._subject

    def set_subject(self, subject):
        self._subject = subject


    def get_text(self):
        return self._text

    def set_text(self, text):
        self._text = text


    def get_attachments(self):
        return self._attachments if self._attachments else [ ]

    def set_attachments(self, attachments):
        self._attachments = attachments

    def get_as_multipart(self):
        msg = MIMEMultipart()
        msg['Subject'] = self.get_subject()
        msg['From'] = self.get_from_email()
        msg['To'] = self.get_to_email()
        for att in self.get_attachments():
            msg_a = MIMEBase(*att[2].split('/'))
            msg_a.set_payload(att[0])   
            msg_a.add_header('Content-Disposition', 'attachment', filename=att[1])
            encoders.encode_base64(msg_a)
            msg.attach(msg_a)
        msg_t = MIMEText(self.get_text(), 'plain',"utf8")
        msg.attach(msg_t)
        return msg.as_string()
    
    def get_as_plain(self):
        msg = MIMEText(self.get_text(), 'plain',"utf8")
        msg['Subject'] = self.get_subject()
        msg['From'] = self.get_from_email()
        msg['To'] = self.get_to_email()   
        return msg.as_string()

    def get_as_html(self):
        msg = MIMEText(self.get_text(), 'html',"utf8")
        msg['Subject'] = self.get_subject()
        msg['From'] = self.get_from_email()
        msg['To'] = self.get_to_email()   
        return msg.as_string()
    
class MailFormatError(Exception):

    def __init__(self, msg):
        self.message = msg

    def __str__(self):
        return self.message