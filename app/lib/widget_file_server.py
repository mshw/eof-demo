# -*- coding: utf-8 -*-
"""
	WIDGET "File Server"

	Services:
		- File_server

"""

from io import StringIO
from helper import html_escape
from class_workflow import Workflow
import localization
import cgi
import dialogs
from engine_core import engine
from flask import session
#



FOLDER_IMAGE_TEMPLATE 	= """<a href='%(file_url)s' rel='link' class='%(folder_class)s' title='%(desc)s' /></a>"""
EDIT_BUTTON_TEMPLATE 	= """<a href='/file_share_edit?guid=%(guid)s' title='%(edit)s'>8</a>"""

DELETE_BUTTON_TEMPLATE 	= """ <a class="btn-delete" title='%(delete)s' msgtitle='%(msgtitle)s' msgdescr='%(msgdescr)s' onclick='del_share(this, "%(guid)s")'>X</a>"""

#<a href='/file_share_edit?guid=%(guid)s' class='edit' title='%(edit)s'>
def page_title(page):
    loc = localization.get_lang()
    page.title = "%s &raquo; %s" % (loc["header_hgmb"], loc["header_fs"] )

def title_template(richtext):
    richtext.value = """<h1>%s</h1>""" % loc["fs_title"]

def edit_buttons(handler): # shit happens
    lang = session.get("lang")	
    sizes = {"en":140,"fr":175,"ru":155}
    handler.text = loc["fs_addshare"]
    handler.width = sizes[lang]

def draw_file_share_folders(user):
    loc = localization.get_lang()
    lang = session.get("lang")	
    def get_data_for_share(fs):
        desc = html_escape(fs.name )
        name = cgi.escape(fs.name)

        edit_url = del_url = ""

        file_url, dis = (u"/file_share?share=%s"%fs.guid, "") if fs.exists() else ("#"," dis")

        if user.can(user.rules["USER_ADMIN"], fs.guid):
            edit_url 	= EDIT_BUTTON_TEMPLATE 		% {	"guid":fs.guid, "edit" : loc["edit"] }
            del_url 	= DELETE_BUTTON_TEMPLATE 	% { "guid":fs.guid, "delete" : loc["delete"],
                                                       "is_share" : "1", "msgtitle": loc["f_del_arc_title"],
                                                        "msgdescr": loc["f_del_arc_descr"] }

        f_class = "ffolder_wf" if fs.guid in wf_shares else "ffolder"

        file_image = FOLDER_IMAGE_TEMPLATE % { "folder_class":f_class,  "file_url":file_url,  "desc":desc}

        return u"""<li>
    <span class="project_badge red iconsweet">%(edit_url)s%(del_url)s</span>
    <a href="%(file_url)s"><img src="images/repertoires.png" alt="%(name)s"></a>
    <a href="#" class="project_title">%(name)s</a>
    </li>"""%{"file_url":file_url,    "guid":fs.guid, "name":name, "dis":dis, "edit_url":edit_url, "del_url":del_url}

    def get_data_for_alias(f_alias):
        alias_id = f_alias.guid
        alias_name = cgi.escape(f_alias.name)
        link = "smart_folder?folder=%s" % alias_id
        file_image = u""" <a href='%(link)s' rel='link' class='ffolder_alias' title='%(desc)s' /></a>
            """%{ "link":link, "desc":alias_name}

        edit_url = del_url = ""
        if user.can(user.rules["USER_ADMIN"], alias_id):
            edit_url 	= """<a href='/smart_folder_edit?guid=%(guid)s' title='%(edit)s'>8</a>"""% {
                                                        "guid":f_alias.guid, "edit" 	: loc["edit"]  }
            del_url 	= DELETE_BUTTON_TEMPLATE 	% { "guid":f_alias.guid, "delete" : loc["delete"],
                                                        "is_share": "0", "msgtitle": loc["fs_del_smart_title"],
                                                        "msgdescr": loc["fs_del_smart_descr"]}
        return u"""<li>
    <span class="project_badge red iconsweet">%(edit_url)s%(del_url)s</span>
    <a href="%(link)s"><img src="images/dossier-intellegents.png" alt="%(name)s"></a>
    <a href="#" class="project_title">%(name)s</a>
    </li>""" % {
                             "link":link,
                             "guid":str(alias_id),
                             "name":alias_name,
                             "dis":"",
                             "edit_url" : edit_url,
                             "del_url":del_url
                         }


    res = StringIO()
    max_width = 7
    cur_width = 0
    res.write(u"""<div class="one_wrap">
    <div class="widget" >
        <div class="widget_title_bjm"><h5>%s</h5></div>
            <div class="widget_body "><div class="content_pad  ">
            <div class="project_sort">
                <ul class="project_list">"""%loc['fs_title'])

    #if engine.workflow_cache == []:
    #	engine.workflow_cache = Workflow.all()
    #workflows = engine.workflow_cache

    wf_shares = []#wf.folder.guid for wf in workflows if wf.folder]
    empty=True
    for share in user.get_file_shares():
        #if cur_width > max_width:
        #    cur_width = 0
        #    #res.write( u"""<div class='clearfix'></div>""" )
        res.write( get_data_for_share(share) )
        #cur_width += 1
        empty = False

    for f_alias in user.get_smart_folders():
        #if cur_width > max_width:
        #    cur_width = 0
        #    #res.write(u"""<div class='clearfix'></div>""")
        res.write(get_data_for_alias(f_alias))
        #cur_width += 1
        empty = False
    if empty:
        res.write(u"""<div class="empty_folder">%(ds_emptyfolder)s</div> """ % {
            "ds_emptyfolder": loc["ds_emptyfolder"]})
    res.write( u"""</ul>
    </div>
    </div></div>
    </div>
</div>""" )
    res.write( u"""<script type="text/javascript">
               function del_share(ev, guid){
               //if (xdel() == true){
               //window.location.href = "/file_server?delete_share="+guid;
                var d = jQuery("#delete_smartfolder");
                d.modal('show');
                d.find("h3").html(ev.getAttribute("msgtitle"));
	            d.find("#delete_folder_msg").html(ev.getAttribute("msgdescr"));
	            $('#delete').attr('value', guid);
               };

               </script>"""%{"uag_delete_confirm":loc["uag_delete_confirm"]} )
    res.write(dialogs.delete_smartfolder())
    html = res.getvalue()
    res.close()
    return html

def button_simple_dialog(button_title, dialog_label,field_name,submit_title):
    loc = localization.get_lang()
    return u"""<a class="button_big" href="#d_%(field_name)s" data-toggle="modal" style="margin-top: 10px"><span class="iconsweet">+</span>%(button_title)s</a>
    <div class="modal hide" id="d_%(field_name)s" >
    <div class="modal-header"><a class="close" data-dismiss="modal">×</a>
    <h3>%(button_title)s</h3></div><div class="modal-body">
    <form action='#' method='post' class='formnewfolder' id='f_%(field_name)s'>
    <ul class="form_fields_container">
    <li><label>%(dialog_label)s:</label> <div class="form_input_mod"><input name="%(field_name)s" type="text"></div></li>
    </ul>
    </form></div>
    <div class="modal-footer">
    <a href="#" onclick='jQuery("#f_%(field_name)s").submit();return false;' class="bluishBtn button_small">%(create)s</a>
    <a data-dismiss="modal" class="greyishBtn button_small" href="#">%(cancel)s</a>
    </div>
    </div>""" % {
                         "dialog_label":dialog_label,
                         "button_title": button_title,
                         "field_name": field_name,
                         "create": loc["create"],
                         "cancel": loc["cancel"],                         
                     }