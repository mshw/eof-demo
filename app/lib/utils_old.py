from class_ACL import ACL
from class_user import User
from class_group import Group
from class_folder import Folder
#
from class_document_share import Document_share
from class_metafield import Metafield, Metas_type
from class_smart_folder import SmartSubFolder, SmartFolder
from class_share_dir import ShareDir
from indexers import normalize
import dialogs
import utils_old
from datetime import datetime
import localization
import json
import re
import uuid

def set_acl_to_children(subj, object, acl_type):
    for folder in object.get_children():
        subj.add_access_to(folder, acl_type)
        set_acl_to_children(subj, folder, acl_type )
    [subj.add_access_to(doc_share, acl_type) for doc_share in object.get_document_shares_all()]

def del_acl_from_children(subj, object, acl_type):
    acl = ACL.get_by_full_match(subj.guid, object.guid, acl_type)
    if acl:
        acl.delete()	
        for folder in object.get_children():
            del_acl_from_children(subj, folder, acl_type )  
        [subj.delete_access_to(doc_share, acl_type) for doc_share in object.get_document_shares_all()]    

def execute_ACL(args, object, url):

    user = User.current()
    if not user.can_edit_file_share(object): return

    if "del_acl" in args and args["del_acl"] !="":
        """ delete ACL
		"""
        del_acl = args["del_acl"]
        acl = ACL.get_by_id(del_acl)
        subject = None
        if acl and acl.object == object.guid:
            acl_type = acl.rule
            if "setacl" in args:
                if "add_user" in args:
                    add_user = args.get("add_user")#recursive delete only with new add
                    subject = User.get_by_guid(add_user)
                elif "add_group" in args:
                    add_user = args.get("add_group")#recursive delete only with new add
                    subject = Group.get_by_guid(add_user)      
                    
                if subject:
                    if isinstance(object, Folder) :
                        del_acl_from_children(subject, object, acl_type)
                    if isinstance(object, ShareDir):
                        if not object.is_top_folder:
                            raise Exception("NOT_VALID_TOP_FOLDER")
                        object.delete_access_for(subject.guid,acl_type)	    
            else:
                acl.delete()
        #response.redirect(url)


    if 	"acl_type" in args and args["acl_type"] !="":
        acl_type = args["acl_type"]

        if not acl_type in ACL.rules.values() : return
        if acl_type == 'admin': return

        if "add_user" in args:
            add_user = args["add_user"]
            user = User.get_by_guid(add_user)

            if user:
                user.add_access_to(object, acl_type)
                """add this ACL to Children
				"""
                if "setacl" in args:
                    if isinstance(object, Folder) :
                        set_acl_to_children(user, object, acl_type)
                    if isinstance(object, ShareDir):
                        if not object.is_top_folder:
                            raise Exception("NOT_VALID_TOP_FOLDER")
                        object.set_acl_to_childs(user.guid)

        elif 	"del_user" in args:
            delete_user = args["del_user"]
            user = User.get_by_guid(delete_user)
            if user:
                user.delete_access_to(object, acl_type)
            return

        elif 	"add_group" in args:
            add_group = args["add_group"]
            group = Group.get_by_guid(add_group)

            if group:
                group.add_access_to(object, acl_type)
                if "setacl" in args:
                    """add this ACL to Children
					"""
                    if isinstance(object, Folder) :
                        [group.add_access_to(folder, acl_type) for folder in object.get_children()]
                        [group.add_access_to(doc_share, acl_type) for doc_share in object.get_document_shares_all()]

        elif 	"del_group" in args:
            del_group = args["del_group"]
            group = Group.get_by_guid(del_group)
            if group:
                group.delete_access_to(object, acl_type)
            return

        elif 	"add_doc_share" in args:
            add_doc_share = args["add_doc_share"]
            document_share = Document_share.get_by_guid(add_doc_share)
            if object:
                object.add_access_to(document_share, acl_type)

        elif 	"del_doc_share" in args:
            del_doc_share = args["del_doc_share"]
            del_doc_share = Document_share.get_by_guid(del_doc_share)

            if object:
                object.delete_access_to(del_doc_share, acl_type)

            return

def get_expressions_from_args(args):
    """ custom search expressions"""
    from class_metafield import Expression

    expressions = []
    if "order" in args and args["order"] != "":
        #raise Exception(str([(a, args.get(a)) for a in args]))
        try:
            order=args["order"][0].split(',') if isinstance(args["order"], list) else args["order"].split(",")
        except Exception, e:
            raise Exception(str(args["order"]))
        for arg in order:
            if not arg: continue
            key = arg[5:]
            exprn = Expression()
            if args["value_"+key] != "":
                #raise Exception(str(args.values())+"|||"+str(key))
                try: # here was a kind of sophisticated decode error, so I made this try/except

                    exprn.key = key
                    exprn.meta = args["meta_"+key]
                    exprn.value =  float2base(date2base(html_escape(args["value_"+key])))
                    exprn.expr_type = args["expr_"+key] # min

                    #exprn.meta_type =
                    expressions.append( exprn)
                except:
                    #raise Exception(str(args)+"|||"+str(key))
                    exprn.key = key
                    exprn.meta = args["meta_"+key]
                    exprn.value =  float2base(date2base(html_escape(args["value_"+key])))
                    exprn.expr_type = args["expr_"+key] # min
                    expressions.append( exprn)


    return 	expressions

def get_new_metafield_from_args(args, document_share):
    from class_metafield import Metafield

    if  "type" in args and args["name"] !="" :
        try:
            type = args["type"]
            name = args["name"]
            constraint = args["constraint"] if int(type) != 5 else args["archive"]
            default = args["default"]
            required = args["required"]
            modify = "1"
            visible = "1"
        except KeyError:
            return None

        metafield = Metafield()

        metafield.name = name
        metafield.constraint =  constraint
        metafield.default = default
        metafield.required = required
        metafield.user_can_modify = modify
        metafield.visible = visible
        metafield.type_id = type
        metafield.document_share_guid = document_share.guid

        #metafield.is_validat()

        # if list meta type
        if metafield.type_id == "4":
            # don't save if empty list values
            if len(constraint) == 0:
                return None
            # delete trailing and leading characters inside list elements
            metafield.constraint = ",".join([s.strip() for s in metafield.constraint.split(',')])

        return metafield

def get_edited_metafield_from_args(args):
    from class_metafield import Metafield

    if  "metid" in args and args["name"] !="" :
        metid 		= args["metid"]
        metafield = Metafield.get_by_id(metid)
        if not metafield:
            return

        name 		= args["name"]
        constraint 	= args["constraint"] if int(metafield.type_id) != 5 else args["archive"]
        default 	= args["default"]
        required 	= args["required"]
        modify = "1"
        visible = "1"

        metafield.name 			= name
        metafield.constraint 	= constraint
        metafield.default 		= default
        metafield.required 		= required
        metafield.user_can_modify = modify
        metafield.visible 		= visible
        #metafield.type_id = type
        #metafield.document_share_guid = document_share.guid

        # if list meta type
        if metafield.type_id == 4:
            # don't save if empty list values
            if len(constraint) == 0:
                return None
            # delete trailing and leading characters inside list elements
            metafield.constraint = ",".join([s.strip() for s in metafield.constraint.split(',')])

        return metafield

#def document_share_operations(document_share, args, page_cont=None):
    #result = ""

    ##raise Exception(dir(obsolete_request.files))
    #if "file" in obsolete_request.files :
        #""" add document
                #"""
        #file_data = obsolete_request.arguments.get('file')[0]
        #file_name = obsolete_request.arguments.get('file')[1]

        ##raise Exception(unicode(file_data))
        #document_share.put_document(file_name, file_data, get_metas_fromargs(document_share, args, page_cont))
        ##raise Exception("YES")
        #return page_cont

    #""" a list of available documents
        #"""
    #documents = []
    #loc = localization.get_lang()
    #if document_share:
        #docsa, count =  document_share.get_documents()
        #for doc in  docsa:
            #d_name = doc["db_id"][0]
            #if d_name in obsolete_request.arguments:
                #documents.append(d_name)

    #""" do operations """
    #if "task" in obsolete_request.arguments:
        #task = obsolete_request.arguments["task"][0]
        #if task == "del":
            #""" delete """
            #document_share.delete_documents(documents)
        #elif task == "down":
            #""" download """
            #document_share.get_files(documents)
        #elif task == "email":
            #""" send email"""
            #subj = obsolete_request.arguments["subj"][0]
            #msg = obsolete_request.arguments["msg"][0]
            #result = document_share.send_files_by_email(obsolete_request.arguments["email"][0], documents, subj, msg)
        #elif task == "move":
            #document_share.delete_all_contents()


    #return result



def get_meta_selecter(metafield, mark, descr, def_val = ""):
    ret= opt = ""
    options = [word  for word in metafield.constraint.split(",") if len(word) > 0 ]

    if def_val is "":
        def_val = metafield.default.split()
        def_val = normalize( def_val[0] ) if len(def_val) > 0 else ""
    for option in options:
        sel = ""
        option = normalize( option )
        if def_val == option:
            sel =  " selected"#" %(ds_selected)s " % {"ds_selected" : loc["ds_selected"]}

        opt +=	 """<option value='%(val)s' %(sel)s>%(val)s</option>"""%{"val":option, "sel":sel }


    ret += """<li><label>%(name)s:</label> <div class="form_input_mod nomarginw"><select class="combobox" style='width:100px;' name='%(id)s' title='%(descr)s'   >%(opt)s</select></div></li>"""%{
        "name":metafield.name+mark,  "opt":opt, "descr":descr, "id":metafield.id}
    return ret

def get_metas_fromargs(document_share, args, page_cont=""):
    metas = {}

    for metafield in document_share.get_additional_metafields():

        id = str(metafield.id)
        if id in args:
            metas[id] = normalize( args[id] )
            if metafield.type_id == 3:
                metas[id] = utils_old.float2base(normalize( args[id] ))          
            elif metafield.type_id == 2:
                metas[id] = utils_old.date2base(normalize(args[id]))
            elif metafield.type_id == 5:

                if args[id] == "new_smart" or args.get("new_sub_folder_%s"%id):
                    m_id = "new_sub_folder_%s"%id
                    new_smart_sub_folder_name = args[m_id].strip() if m_id in args and args[m_id] else ""
                    if new_smart_sub_folder_name:
                        try:
                            # if new name is already exists - put existing sub folder
                            ssf = SmartSubFolder.get_by_name_and_parent_guid(new_smart_sub_folder_name, metafield.constraint)
                            if ssf:
                                metas[id] = u"%s<span_divider>%s" % (ssf.guid, ssf.name)  #sf.guid
                            else:
                                ssf = SmartSubFolder()
                                ssf.name = new_smart_sub_folder_name
                                ssf.parent_guid = str(metafield.constraint)
                                ssf.save()
                                metas[id] = u"%s<span_divider>%s" % (ssf.guid, ssf.name)  #sf.guid
                        except Exception, e:
                            if page_cont:
                                import localization
                                loc = localization.get_lang()
                                page_cont.title, page_cont.text, page_cont.active = u"ERROR", loc[str(e)] if str(e) in loc else str(e), "1"

                else:
                    sf = SmartSubFolder.get_by_guid( args[id])
                    if not sf:
                        sf = SmartSubFolder.get_by_name_and_parent_guid(args[id], metafield.constraint)
                    if sf:
                        #raise Exception(str(metas[id]))
                        metas[id] = u"%s<span_divider>%s" % (sf.guid, sf.name)  #sf.guid
                        #normalize( self.__filename.replace("_", " _ ") + u"<span_divider>%s" % self.__filename ),
            #if value =="":
            #metas[id] = value#metafield.default
            #else:
            #	metas[id] =  value
#	log_bugs("15")
    return 	metas

def get_document_edit(document_share, doc, args):

    loc = localization.get_lang()
    metafields = "<ul class='form_fields_container'>"
    from class_notification import Notification, NotificationType
    notifications = Notification.get_reminders_for_user_and_doc(User.current(), doc)
    for metafield in document_share.get_additional_metafields():
        meta_type = Metas_type.get_by_id(metafield.type_id)
        if metafield.user_can_modify == "0" and not User.current().is_admin():
            continue
        if meta_type :
            descr = meta_type.description.encode("utf8")
            regexp =  meta_type.regexp.encode("UTF-8")
        else:
            descr = "no title"
            regexp = ".*"

        mark = " *"
        if 	str(metafield.required) == "0":
            regexp += "|(^$)"
            mark = ""

        date = ""

        meta_descr = loc[descr] if descr in loc else descr # loc of description
        meta_descr = html_escape(meta_descr)
        meta_id = str(metafield.id )
        value = doc.meta()[meta_id] if meta_id in doc.meta() else ""
        #raise Exception(value)
        value = html_escape(value.split("<span_divider>")[0])

        if meta_type.id == 4:
            metafields +=  get_meta_selecter(metafield, mark, meta_descr , value)

        elif meta_type.id == 5:
            value = str(value).split("<span_divider>")[0]
            add_templ = """<span class="project_badge red iconsweet iconrfloat"><a href="#"  onclick="$('li.new_archive_'+%s).show();">+</a></span>"""
            selection_template = """
				<select class="combobox smart_selection" attr='%s'  style="width: 100%%;" name="%s"> %s </select>
			"""
            shares_options = ""
            is_display = "style='display:none'"
            parent_folder = SmartFolder.get_by_guid(metafield.constraint)
            if parent_folder:
                shares_options += """<option value="">---</option>"""
                for share in parent_folder.sub_folders:
                    shares_options += """<option %s value="%s">%s</option>""" % (
                        "selected" if value==share.guid else "", share.guid, share.name
                    )
                if not shares_options:
                    is_display = ""
                shares_options += """<option value="new_smart">+ %s</option>"""  % loc["fs_add_smartsubfolder"]
                show_add = True
            meta_folder = selection_template % ("new_archive_"+str(metafield.id), metafield.id, shares_options)

            meta_folder_new = """<li class='%(tr_class)s' %(display)s><label>%(html_archive)s:</label><div class="form_input_mod">
            <input type="text" name="new_sub_folder_%(meta_id)s" value="" /></div></li>

			""" % {"html_archive" : loc["html_archive"], "display" : is_display, "tr_class" : "new_archive_"+str(metafield.id),
                               "meta_id" : str(metafield.id)}

            metafields += """<li><label>%(name)s:</label>  %(add)s<div class="form_input_mod  %(nomarg)s">%(folder)s</div></li>"""%{
                "name":metafield.name+mark, "folder" : meta_folder,"add":add_templ%metafield.id if show_add else "","nomarg": "nomargin" if show_add else "nomarginw"}
            metafields += meta_folder_new

        else:
            if meta_type.id == 2:
                date += " fdate"


            metafields += """
                <li>
                    <label>%(name)s:</label>
                    <div class="form_input_mod">
                        <input type='text' value='%(value)s' title='%(descr)s' regexp='%(regexp)s' class="vtip%(date)s" name='%(id)s' autocomplete="off" />
                    </div>
                </li>""" % {
                "name": metafield.name+mark,
                "id": metafield.id,
                "value": utils_old.date2norm(value),
                "descr": meta_descr,
                "regexp": regexp,
                "date": date}
            if meta_type.id == 2:
                remind_mod = """
                    <li>
                        <a href="#" class="button_big remind-show-btn">
                            <span class="iconsweet" data-target="%(id)s">|</span>%(name)s
                        </a>
                    </li>""" % {"name": loc["remind_me"], "id": metafield.id}
                metafields += remind_mod
                reminders = [r for r in notifications if r.type == NotificationType.reminder() and
                             r.message["meta_guid"] == meta_id]
                for reminder in reminders:
                    days_to_remind = reminder.message["days_before"]
                    mails = [r for r in notifications
                             if r.type == NotificationType.mail_reminder() and
                             days_to_remind == r.message["days_before"]]
                    mails_str = ", ".join(map(lambda r: r.message["mail"], mails))
                    metafields += """
                        <li>
                            <label>%(days_label)s</label>
                            <div class='form_input_mod'>
                                <input type='text' value='%(days_to)s' name='remind_%(form_ident)s'>
                            </div>
                        </li><li>
                            <label>%(sharing_custom_mail)s</label>
                            <div class='form_input_mod'>
                                <input class='custom-user-email-input' value='%(mails)s' type='text' name='remind_mail_%(form_ident)s'>
                            </div>
                        </li><li>
                            <div class='form_input_mod'>
                                <a href='#' class='remove-custom-fields'>%(remove)s</a>
                            </div>
                        </li>""" % {
                        "mails": mails_str,
                        "days_to": reminder.message["days_before"],
                        "form_ident": meta_id,
                        "days_label": loc["remind_on_days"],
                        "sharing_custom_mail": loc["sharing_custom_mail"],
                        "remove": loc["delete"]
                    }




    metafields +=  """</ul>"""

    return """
%(dialog)s
<script>
function adddocsmartchange(t, tr_id) {
  var o=jQuery('#editformmetka li.'+tr_id);
  if ($(':selected',t).val() == 'new_smart') {
  	o.show();
  }
  else {
  	o.hide();
  }

};
jQuery(document).ready(function($){

    $("#editformmetka").modal('show');
});
</script>
""" % {
        "dialog": dialogs.modify_meta(metafields,
                                      json.dumps(dict((a, args.get(a)) for a in args if a not in ("sid", "sender"))),
                                      document_share.guid,
                                      doc.guid())}


def search_escape( text):
    html_escape_table = {
        "&": "&amp;",
        #'"': "&quot;",
        #"'": "&apos;",
        ">": "&gt;",
        "<": "&lt;"

    }

    return "".join(html_escape_table.get(c,c) for c in text)

def html_escape( text):
    html_escape_table = {
        "&": "&amp;",
        '"': "&quot;",
        "'": "&apos;",
        ">": "&gt;",
        "<": "&lt;"

    }

    return "".join(html_escape_table.get(c,c) for c in text)

def filename_escape(text):
    filename_escape_table = {
        "\\": "_",
        "/"	: "_",
        "|"	: "_",
        ":"	: "_",
        "*"	: "_",
        "?"	: "_",
        '"'	: "_",
        "<"	: "_",
        ">"	: "_",
        #"&"	: "_",
        #"#"	: "_",
        #"'"	: "_",
        #","	: "_",
        #";"	: "_"
    }
    return "".join(filename_escape_table.get(c,c) for c in text)

def key_escape(text):
    def extract(key):
        return "".join(c for c in key if c not in "biTKY7-")    
    def bdecode(s):
        alphabet = '12345689acdefghjkmnopqrstuvwxyzABCDEFGHJLMNPQRSUVWXZ'
        num = len(s)
        decoded = 0
        multi = 1
        for i in reversed(range(0, num)):
            decoded = decoded + multi * ( alphabet.index( s[i] ) )
            multi = multi * len(alphabet)
        return decoded    
    try:
        if not bdecode(extract(text[10:])): #That's not valid uid
            return (None,None,None)
        return bdecode(extract(text[0:4])),bdecode(extract(text[4:10])),uuid.UUID(int=bdecode(extract(text[10:])))
    except Exception as e: 
        return (None,None,None)

def float2base(text=""):
    result = text
    re_obj = re.compile(r"\b([0-9]*[\.]?[0-9]+)\b")

    for match in re_obj.finditer(result):
        result = result.replace(match.group(1), match.group(1).replace('.',','))
    return result


def date2base(text=""):
    result = text

    re_obj = re.compile(r"(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}")
    for match in re_obj.finditer(result):
        datevalue = datetime.strptime(match.group(), "%d-%m-%Y").strftime("%Y-%m-%d")
        result = result.replace(match.group(), datevalue)
        
    re_obj = re.compile(r"(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)[0-9]{2}")
    for match in re_obj.finditer(result):
        datevalue = datetime.strptime(match.group(), r"%d/%m/%Y").strftime("%Y-%m-%d")
        result = result.replace(match.group(), datevalue)
    
    return result

def date2norm(text):
    result = text

    re_obj = re.compile(r"(19|20)[0-9]{2}[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])")

    for match in re_obj.finditer(result):
        #print match.group()
        datevalue = datetime.strptime(match.group(), "%Y-%m-%d").strftime("%d-%m-%Y")
        result = result.replace(match.group(), datevalue)
    return result

def get_referer_url():
    from managers import request_manager
    env = request_manager.get_request().environment().environment()
    #raise Exception(str(env)) # to look through env on appropriate params
    result = env["HTTP_REFERER"] if "HTTP_REFERER" in env else "/Documents_share"
    if "home_page" in result.lower():
        result = "/Documents_share"
    #result = result.replace('&',"%26") # to process several arguments
    return result

def collect_from_args_to(req_args, args_list):
    return dict([k, req_args[k] if k in req_args and req_args[k] else ""] for k in args_list)

def getVal(args, a=""):
    result = ""
    if a.__class__ == int:
        try:
            result = args and args[a] or ""
        except:
            result = ""
    else:
        result = args and a in args and args[a] or ""
    return result

def get_logout_back_url():

    # first, get the REQUEST_URI, current address string, with all GET arguments.
    env = request.environment()
    result = env["REQUEST_URI"] if "REQUEST_URI" in env else ""
    # replace "&" with %26 - in order to process several arguments
    result = result.replace('&',"%26")
    return result

def scalar(key, defaultvalue = None, castto = None):
    notdecoded = request.arguments[ key ] if key in request.arguments else defaultvalue
    value = notdecoded.decode( 'utf8' ) if isinstance( notdecoded, str ) else notdecoded
    return castto(value) if castto and value else value