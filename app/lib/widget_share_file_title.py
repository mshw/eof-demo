
import cgi

class WidgetShareFileTitle:

    TITLE_FORMAT 	= """ %(current_dir)s %(tree)s / %(root)s </span>"""

    CURRENT_DIR 	= u"""
        <span><a style='font-size: 16pt; text-decoration:none; font-weight:normal;'href='/file_share?share=%(share_guid)s&parent_dir=%(parent_guid)s'>
        %(title)s
        </a></span>
        """

    TREE_SEPARATOR	= u"<span style='color:#A7A7A7'> / </span>"

    TREE_ELEMENT 	= u"""
        <span><a style='font-family: arial; font-size: 14px; font-weight:normal; color:#A7A7A7; text-decoration:none'href='/file_share?share=%(share_guid)s&parent_dir=%(parent_guid)s'>
        %(title)s
        </a></span>
        """

    ROOT_ELEMENT	= u"""<a style='font-family: arial; color:#A7A7A7 !important; text-decoration:none;font-size: 14px;font-weight:normal;' href='/file_server'>%s</a>"""

    EDIT_FILE_SHARE_LINK = """<span class="project_badge red iconsweet"><a href='/file_share_edit?guid=%(guid)s' class='edit' title='%(edit)s'>8</a></span>"""

    def __init__(self,user, parent_dir):
        self.__user 		= user
        self.__parent_dir	= parent_dir		

    def render(self):
        import localization
        loc = localization.get_lang()
        root = loc["fs_title"]

        parent_dir = self.__parent_dir
        tree = [	self.CURRENT_DIR % {
            "title":cgi.escape(self.__parent_dir.name),
            "share_guid" : self.__parent_dir.top_folder.guid,
            "parent_guid" : self.__parent_dir.guid
        }]

        while parent_dir.parent:
            parent_dir 	= parent_dir.parent
            tree.append(	self.TREE_ELEMENT % {
                "title":cgi.escape(parent_dir.name),
                "share_guid" : parent_dir.top_folder.guid,
                "parent_guid" : parent_dir.guid
            })

        # now current parent_dir should be top folder
        if parent_dir.is_top_folder:
            user_can_admin = self.__user.can(self.__user.rules['USER_ADMIN'], parent_dir.guid)
            edit_link = self.EDIT_FILE_SHARE_LINK % {
                "guid"	: parent_dir.guid,
                "edit" 	: loc["edit"]
                } if user_can_admin else ""

            tree[-1] = edit_link + "&nbsp;" + tree[-1]

        tree.append(	self.ROOT_ELEMENT % root	)

        #return u"""
        #	<div style='border-bottom: 3px solid #E7E7DB;margin: 0 0 20px; padding: 0 0 8px; width: 100%%;'>
        #		%s
        #	</div>	""" 
        return self.TREE_SEPARATOR.join(tree)