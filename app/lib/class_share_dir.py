from class_CRUD import CRUD
from class_share_file import ShareFile
from class_ACL_proxy import ACL_proxy, ACL_record
from uuid import uuid4
from helper import filename_escape
import os, os.path, shutil
from datetime import date
from storage import storage
from ..config import CONFIG
TABLE_NAME = "share_file"

class ShareDir(object):

    def __init__(self):
        self.id		= None
#		self.share_guid = ""
        self.parent_guid = "" 		# if empty - then it is a top folder
        self.guid	= str(uuid4())
        self.name	= ""
        self.is_dir	= "1"

        # lazy attributes
        self._date_created = ""
        self._complete_path = ""
        self._acl_subjects	= None # groups and users in one list, those who have access to this object
        self._is_top_folder = None
        self._parent		= None
        self._childs		= None
        self._top_folder	= None

    def __fill_from_row(self, row):
        (self.id, self.parent_guid, self.guid, self.name, self.is_dir) = row
        return self

    def fill_from_row(self, row):
        (self.id, self.parent_guid, self.guid, self.name, self.is_dir) = row
        return self

    def __insert(self):
        self.id = CRUD.create(TABLE_NAME,
                              parent_guid=self.parent_guid, guid=self.guid,
                              name=self.name, is_dir="1")
        return self

    @classmethod
    def __retrieve(self, **kwargs):
        rows = CRUD.retrieve(TABLE_NAME, **kwargs)
        return [ShareDir().__fill_from_row(row) for row in rows]

    @classmethod
    def __retrieve_one(self, **kwargs):
        row = CRUD.retrieve_one(TABLE_NAME, **kwargs)
        return ShareDir().__fill_from_row(row) if row else None

    def __update(self):
        CRUD.update(TABLE_NAME, {"id": self.id},
                    parent_guid=self.parent_guid,
                    name=self.name)
        return self

    def __delete(self):
        CRUD.delete(TABLE_NAME, guid=self.guid)
        return self

    # STORAGE
    def save(self):
        if not self.name:
            raise Exception("FS_EMPTY_NAME")

        if self.id:
            self.__update()
        else:
            self.name = filename_escape(self.name)
            if not storage.exists(self.complete_path):
                storage.mkdir(self.complete_path)
            self.is_dir = "1"
            self.__insert()

        # share_file folder creation

        return self

    # STORAGE
    def delete(self):
        fs_objects = self.objects()
        if fs_objects:
            # non-empty folder - delete objects inside
            [fso.delete() for fso in fs_objects]

        # empty folder - just delete
        if self.exists():
            storage.rmtree( self.complete_path ) # physical deletion
        self.__delete()
        ACL_record.delete(object=self.guid)


    # STORAGE
    @property
    def complete_path(self):
        if self._complete_path == "":
            self._complete_path = self.guid
        return self._complete_path

    @property
    def date_created(self):
        bad_dir_string = "---"
        if not self._date_created:
            if self.exists():
                self._date_created = date.fromtimestamp(os.stat(storage.abs_path(self.complete_path)).st_mtime ).strftime("%d / %m / %Y")
            else:
                self._date_created = bad_dir_string
        return self._date_created

    @property
    def acl_subjects(self):
        from class_user import User
        from class_group import Group
        if self._acl_subjects == None:
            self._acl_subjects = []
            acls = ACL_record.retrieve(object=self.guid)
            for acl in acls:
                self._acl_subjects.append(
                    User.get_by_guid(acl.subject) or Group.get_by_guid(acl.subject)
                )
        return self._acl_subjects

    @property
    def is_top_folder(self):
        self._is_top_folder = True if not self.parent_guid else False
        return self._is_top_folder

    @property
    def parent(self):
        if not self._parent:
            self._parent = ShareDir().__retrieve_one(guid=self.parent_guid, is_dir="1")
        return self._parent

    @property
    def childs(self):
        if self._childs == None:
            self._childs = self.objects
        return self._childs

    @childs.setter
    def childs(self, value):
        self._childs = value

    @property
    def top_folder(self):
        if self._top_folder == None:
            if self.parent_guid == "":	self._top_folder = self
            else:
                parent_dir = self.parent
                while parent_dir.parent:
                    parent_dir 	= parent_dir.parent
                self._top_folder = parent_dir
        return self._top_folder


    def __getitem__(self, key):
        return self.__dict__[key] if key in self.__dict__ else None

    def __str__(self):
        return self.name.encode("utf8")#u"[ %s ]" % self.name.encode("utf8")

    @staticmethod
    def get_by_id(share_dir_id):
        return ShareDir.__retrieve_one(id=share_dir_id, is_dir="1")

    @staticmethod
    def get_by_guid(share_dir_guid, ignore_is_dir=False):
        if not ignore_is_dir: # main way
            return ShareDir.__retrieve_one(guid=share_dir_guid, is_dir="1")
        # if ignoring is_dir condition - elsewhere return ShareDir or ShareFile, what is correct
        row = CRUD.retrieve_one(TABLE_NAME, guid=share_dir_guid)
        if row:
            return ShareDir().__fill_from_row(row) if row[4]=="1" else ShareFile().fill_from_row(row)

    @staticmethod
    def get_by_parent_guid(share_dir_parent_guid):
        return ShareDir.__retrieve_one(guid=share_dir_parent_guid, is_dir="1")

    @staticmethod
    def get_top_by_name(name):
        """ returns a top level folder with a specified name """
        for tf in ShareDir.top_folders():
            if unicode(tf.name) == unicode(name):
                return tf
        return None

    @staticmethod
    def get_by_name(parent_folder, name):
        """ returns a folder with a specified name inside a specified 'parent_folder' """
        if not parent_folder: return None
        dirs, files = parent_folder.list_dir()
        for d in dirs:
            if d.name == name:
                return d
        return None

    def path(self):
        """ returns a full path (starting from top-level folder) of a current share dir """

        path = [self]
        while not path[-1].is_top_folder:
            path.append(path[-1].parent)
        path.reverse()
        return u"\\".join([e.name for e in path])

    @staticmethod
    def all(limit="", offset=""):
        return ShareDir.__retrieve(limit=limit, offset=offset)

    @staticmethod
    def top_folders():
        return ShareDir.__retrieve(parent_guid="")

    def objects(self):
        """ returns all ShareDirs and ShareFiles located in current file_share [and parent] """
        rows = CRUD.retrieve(TABLE_NAME, parent_guid=self.guid)
        file_share_objects = []
        for row in rows:
            if row[4]=="1":
                file_share_objects.append(ShareDir().__fill_from_row(row))
            else:
                file_share_objects.append(ShareFile().fill_from_row(row))

        self.childs = file_share_objects
        return file_share_objects

    def list_dir(self):
        """ same as 'objects', but dirs and files are divided in two lists, both to be returned """
        rows = CRUD.retrieve(TABLE_NAME, parent_guid=self.guid)

        dirs 	= [ShareDir().__fill_from_row(row) 	for row in rows if row[4]=="1"]
        files 	= [ShareFile().fill_from_row(row) for row in rows if row[4]=="0"]

        self.childs = dirs + files

        return dirs, files

    def size(self):
        return storage.getsize(self.complete_path)

    def link(self): raise Exception("NOT_IMPLEMENTED")

    def walk(self):
        """ Returns a list of 3-lists: [root, dirs, files],	where:
        		'root' 	- parent folder (instance of ShareDir);
        		'dirs'	- list of directories in 'root' folder (instances of ShareDir);
        		'files'	- list of files in parent 'root' folder (instances of ShareFile);
        """
        result = []
        dirs, files = self.list_dir()

        if not dirs:
            return [[self, dirs, files]]
        else:
            for dir in dirs:
                result = ( dir.walk() ) + result

        result = [[self, dirs, files]] + result
        return result

    def exists(self):
        return storage.exists(self.complete_path)

    def abs_path(self):
        return storage.abs_path(self.complete_path)

    def rename(self): raise Exception("NOT_IMPLEMENTED")

    def download(self): raise Exception("NOT_IMPLEMENTED")

    def move_to(self): raise Exception("NOT_IMPLEMENTED")

    def copy_to(self): raise Exception("NOT_IMPLEMENTED")

    def mark(self): raise Exception("NOT_IMPLEMENTED")

    """ Return all users who has access to this folder,
    	including those who are in groups connected to it.
    """
    def users(self):
        result = set(self.users_acl())
        for group in self.groups_acl():
            for user in group.get_users():
                result.add(user)
        return result

    def users_acl(self):
        from class_user import User
        return [subject for subject in self.acl_subjects if isinstance(subject, User)]

    def groups_acl(self):
        from class_group import Group
        return [subject for subject in self.acl_subjects if isinstance(subject, Group)]

    def subjects_acl(self):
        return self.acl_subjects #[subject for subject in self.acl_subjects]

    def add_access_to(self, subject_guid, rule):
        new_acl = ACL_record()
        new_acl.subject = subject_guid
        new_acl.object 	= self.guid
        new_acl.rule 	= rule
        new_acl.save()

    def delete_access_for(self, subject_guid, rule=""):
        ACL_record.delete(
            subject=subject_guid, object=self.guid, rule=rule
        )

    def copy_acls_to(self, share_object):
        for subject in self.subjects_acl():
            new_acl = ACL_record()
            new_acl.object 	= share_object.guid
            new_acl.rule 	= ACL_proxy.rules['SEE']
            new_acl.subject = subject.guid
            new_acl.save()

    def set_acl_to_childs(self, subject_guid):
        for obj in self.objects():
            obj.add_access_to(subject_guid, ACL_proxy.rules['SEE'])
            if obj.is_dir == "1":
                obj.set_acl_to_childs(subject_guid)

    def get_all_share_objects(self, files):
        #TODO: access control
        result = []
        for file in files:
            path = "/".join((self.complete_path, file))
            if storage.exists(path):
                result.append([storage.abs_path(path), unicode(ShareFile().get_by_guid(file).name)])
            else:
                dir = ShareDir.get_by_guid(file)
                if storage.exists(dir.complete_path):
                    result.append([storage.abs_path(dir.complete_path), unicode(dir.name)])
                children = []
                for child in dir.objects():
                    children.append(child.guid)

                lst = dir.get_all_share_objects(children)

                for f in lst:
                    result.append([f[0], os.path.join( dir.name, unicode(f[1]))]) # [dist, file]

        return result


    def send_files(self, files):
        import zipfile
        import os
        zipfilename = os.path.join(os.getcwd(),CONFIG['TEMP-DIRECTORY'], "%s.zip"%self.name)
        zf = zipfile.ZipFile(zipfilename, mode='w')

        # get all dist and file

        allfiles = self.get_all_share_objects(files)
        for file in allfiles:
            zf.write(file[0], unicode(file[1]))

        zf.close()
        #size = os.stat(zipfilename).st_size
        from flask import send_file
        return send_file(zipfilename, "application/zip",as_attachment=True, attachment_filename=unicode(self.name)+".zip")