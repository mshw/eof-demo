import json
class WidgetLogMenu:
    def __init__( self, datatable ):
        self.datatable = datatable

        show_info = session.get( 'show_info', None )
        show_error = session.get( 'show_error', None )
        show_debug = session.get( 'show_debug', None )

        self.show_info = True if show_info is None else show_info
        self.show_debug = False if show_debug is None else show_debug
        self.show_error = True if show_error is None else show_error

        self.hide = session.get( 'hide', 'show_all' )


    def render( self ):
        import localization
        loc = localization.get_lang()
        engine = 'checked' if session.get( 'selected_wf', None ) is None else ''
        finished = 'selected' if self.hide == 'finished' else ''
        active = 'selected' if self.hide == 'active' else ''
        show_all = 'selected' if self.hide == 'show_all' else ''
        data = [ [
            u"""<span>
            <form>
            <a name='engine_log' href='  %(engine_log)s' %(engine)s onclick="execEventBinded('%(id)s', 'custom', {'engine_log': 'engine_log'})"> %(engine_log)s
            </a>
            </form>
            </span>""" %
                       { 	'id': str(self.datatable.id).replace( '-', '_' ),
                                 'engine': engine,
                                 "engine_log" : loc["workflowlog_menu_engine_log"]},

                       u"""<span>
                       <form>
                       <input type='checkbox' name='debug' value='Debug' %(debug)s onclick="execEventBinded('%(id)s', 'custom', {'debug': 'debug'})">%(debug_log)s</input>
                       <span class='arr-divider'>→</span>
                       <input type='checkbox' name='info' value='Info' %(info)s onclick="execEventBinded('%(id)s', 'custom', {'info': 'info'})">%(info_log)s</input>
                       <span class='arr-divider'>→</span>
                       <input type='checkbox' name='error' value='Error' %(error)s onclick="execEventBinded('%(id)s', 'custom', {'error': 'error'})">%(error_log)s</input>
                       </form>
                       </span> """
                       % {'debug': 'checked' if self.show_debug else '',
                          'info': 'checked' if self.show_info else '',
                          'error': 'checked' if self.show_error else '',
                          'id': str(self.datatable.id).replace( '-', '_' ),
                          'debug_log' : loc["workflowlog_menu_debug"],
                          'info_log' 	: loc["workflowlog_menu_info"],
                          'error_log' : loc["workflowlog_menu_error"],
                          }, "",

                       u"""<span><form><select onchange="execEventBinded('%(id)s', 'custom', {'selected_value': $j(this).val() });">
                       <option %(active)s value='active'>%(hide_active)s</option>
                       <option %(finished)s value='finished'>%(hide_finished)s</option>
                       <option %(show_all)s value='show_all'>%(show_all_)s</option>
                       </select></form></span>""" %
                                                  {	'id'		: str(self.datatable.id).replace( '-', '_' ),
                                                           'hide'		: self.hide,
                                                           'finished' 	: finished,
                                                           'active'	: active,
                                                           'show_all'	: show_all,

                                                           'hide_active' 	: loc["workflowlog_menu_hide_active"],
                                                           'hide_finished' : loc["workflowlog_menu_hide_finished"],
                                                           'show_all_' 		: loc["workflowlog_menu_show_all"],
                                                           } ] ]
#		raise Exception( self.hide )
        self.datatable.data = json.dumps( data )


    def check( self, type ):
        if type == 'info':
            self.show_info = not self.show_info
            session[ 'show_info' ] = self.show_info
            return
        if type == 'error':
            self.show_error = not self.show_error
            session[ 'show_error' ] = self.show_error
            return
        if type == 'debug':
            self.show_debug = not self.show_debug
            session[ 'show_debug' ] = self.show_debug
            return

    def select( self, hide = 'show_all' ):
        if hide == 'finished' or hide == 'active' or hide == 'show_all':
            self.hide = hide
            session[ 'hide' ] = hide