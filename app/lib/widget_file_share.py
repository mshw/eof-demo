"""
	WIDGET "File Share"

	Services:
		- File_share

"""

from io import StringIO
from helper import html_escape, scalar
import localization

import json
from flask import session
loc = localization.get_lang()
lang = session.get("lang", None)

# TEMPLATES

def popup_growl(growl, ttl, msg):
    growl.title, growl.text, growl.active, growl.style = ttl, msg, "1", "4"

def set_page_title(obj):
    """ Page title for a browser window/tab """
    loc = localization.get_lang()
    obj.title = "%s &raquo; %s" % (loc["header_hgmb"], loc["header_fshv"])

def title_template(obj, user, fs, parent="" ):
    tmp = """ %(title)s %(tree)s / %(root)s """
    if not parent:
        title 	= fs.name
        tree 	= ""
    else:

        sdirs = []
        p = parent
        while True:
            p = ShareDir.get_by_guid(p.parent_guid) if p.parent_guid!="" else None
            if not p: break
            sdirs.append(p)
        sdirs.append(fs)

        shares = "/ %s" % " / ".join([sd.name for sd in sdirs])

        title 	= parent.name
        tree 	= shares
    root = loc["fs_title"]
    obj.htmlcode = tmp % {"title":title,"tree":tree,"root":root}


def edit_buttons(handler): # shit happens
    # langs
    loc = localization.get_lang()
    titles = [
        loc["fs_addfile"],	loc["fs_addfolder"], loc["fs_delete"], loc["fs_download"],
        loc["fs_copy"],	loc["fs_mark"],	loc["fs_move"],	loc["fs_print"]
    ]

    sizes = {
        "en" : [43, 60, 40, 55],
        "ru" : [90, 95, 50, 50],
        "fr" : [100, 118, 58, 65]
    }

    lang = session.get("lang", None)

    left_sum = 0


    for i,h in enumerate(handler):
        if i >= 4:
            break
        h.text = titles[i]
        h.left = left_sum
        h.width = sizes[lang][i]
        left_sum += sizes[lang][i] + 20


def header_format():
    return [ "guid", loc["fs_filename"], loc["fs_date"], loc["mf_functions"] ]

def dir_row_format(sd):
    return [sd.guid, dir_string_format(sd), sd.date_created(), "dir"]

def file_row_format(sf):
    return [sf.guid, file_string_format(sf), sf.date_created(), "file"]

def dir_string_format(sd):
    return """<div>
		<img src='/e2116dd1-ccac-4237-9d12-cba2038ccdbb.png'>&nbsp;&nbsp;%s
	</div>""" % sd.name

def file_string_format(sf):
    return """<div>
		<img src='/a1b60a58-5f75-47fb-ab01-e32f2cfc4055.png'>&nbsp;&nbsp;%s
	</div>""" % sf.name
