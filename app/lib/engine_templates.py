class_template = u"""# -*- coding: utf-8 -*-
from engine_core import engine
from class_workflow_scripting import WF_proxy, get_wrapper, get_user_by_login
from uuid import uuid4
from class_api import EOFAPI
import datetime
from copy import copy

class {wf_name}:

    def __init__(self, creator_id, original_doc_id):
    self.id = str(uuid4())
    self.creator_id = creator_id
    self.transitionlist = {transitionlist}
    self.error_transitions = {error_transitions}
    self.timeout_transitions = {timeout_transitions}
    self.steplist = {steplist}
    self.name = '{wf_name}'
    self.label = '{wf_label}'
    self.hostname = '{hostname}'
    self.host_ip = '{host_ip}'
    self.lang	= '{language}'
    self.current_question = u""

    self.initialstep = self.step_{step_init}
    self.finalsteps = {finalsteps}
    self.currentstep = self.step_{step_init}
    self.laststep_name = ""
    self.current_document = original_doc_id
    self.document_content = ""
    self.step_data = {{}}
    self.current_event = None
    self.saved_events = {{}}
    self.update_time = datetime.datetime.now()
    self.namespace = {namespace}
    self.active_users	= set()
    self.folder_listeners = {folder_listeners}
    self.triggered_users	= []
    self.triggered_step		= ""
    {init_code}

    @classmethod
    def initiate_listener(self):
    self.namespace = {{'__builtins__':None, 'WF_proxy':WF_proxy,'workflow_id':None}}
    engine.register_global({wf_name}, (str({initial_folder}),{initial_operation}))

    def variables(self, key):
    if key == "now":
    return datetime.datetime.now().strftime('%H:%M:%S')
    elif key == "date":
    return datetime.date.today().strftime('%Y-%m-%d')
    elif key == "workflow":
    return "{wf_name}"
    elif key == "admin":
    return EOFAPI.admin_email()
    else:
    return self.__variables.get(key)

    def set_variable(self, key, value):
    self.__variables[key] = value

    {wf_body}

"""
#{{"EventFaxIn":step_EventFaxIn,"MoveToFaxOut":step_MoveToFaxOut,"Init1":step_Init1,"WorkDone":step_WorkDone,"StopUserFound":step_StopUserFound}}
#{{step_Init1:(transition_Auto1,),step_EventFaxIn:(transition_Condition1,transition_Condition2),step_MoveToFaxOut:(transition_Auto2,)}}
start_step = u"""def step_{step_name}(self):
    self.__variables = {variable}
    self.laststep_name = '{step_name}'

    EOFAPI.action_workflow_trigger(
    log 		= {log},
    action_name="workflowStarted",
    workflow=self,
    file_id=self.current_document,
    user_id=self.creator_id
    )
    """

event_step = u"""def step_{step_name}(self):
    if {log}:
    EOFAPI.send_log("event",
    workflow = self,
    user_id = self.current_event.user_id,
    doc = self.current_event.file_name,
    event_type = self.current_event.event_type,
    step_name = '{step_name}'
    )
    self.saved_events[u"{step_name}"] = copy(self.current_event)
    engine.unregister_event(self, self.current_event.event_type)
    self.current_document = self.current_event.file_name
    self.document_content = self.current_event.ocr_content
    self.active_users.add(self.current_event.user_id)
    self.current_event  = None
    self.laststep_name = '{step_name}'

    """

move_step = u"""def step_{step_name}(self):

    self.current_document = EOFAPI.move_document(
    log 		= {log},
    workflow 	= self,
    document_guid = {document},
    folder_from_guid = {from},
    folder_to_guid = {to},
    users={users},
    set_right={setRightsToFile},
    keep_copy = {keepCopyOfFile},
    keep_version_source = {keepVersionSource},
    keep_version_target = {keepVersionTarget},
    step_name = '{step_name}'
    )
    self.step_data[u'{step_name}'] = {{
    "folder_to" : {to},
    "document_id" : self.current_document,

    }}
    if unicode({to}).lower() != "#trash":
    return self.handler_{step_name}

    self.laststep_name = '{step_name}'

    def handler_{step_name}(self):

    self.saved_events["{step_name}"] = self.current_event
    engine.unregister_event(self, self.current_event.event_type)
    self.current_event = None

    return self.step_{step_name}
    """

users_step = u"""def step_{step_name}(self):
    notified_popup = EOFAPI.show_dialog(
    log = {log},
    workflow = self,
    notifyByDropToWeb = {notifyByDropToWeb},
    users = {to},
    question = {question},
    answerType = {answerType},
    answer = {answer},
    iDontKnow = {iDontKnow},
    renewDelay = {renewDelay}
    )

    self.confirmation_guid = uuid4()
    notified_mail = EOFAPI.send_question_by_mail(
    log = {log},
    workflow = self,
    notifyByEmail = {notifyByEmail},
    emailTo = {emailTo},
    emailObject = {emailObject},
    emailAttachDocument = {emailAttachDocument},
    emailDocument = {emailDocument},
    question = {question},
    answerType = {answerType},
    answer = {answer}
    )
    self.current_question = {question}
    if notified_popup or notified_mail:
    return self.handler_{step_name}

    self.laststep_name = '{step_name}'

    def handler_{step_name}(self):
    if {log}:

    EOFAPI.send_log("answered",
    workflow = self,
    user_id = self.current_event.user_id,
    answer 	= self.current_event.answer_value,
    step_name = '{step_name}',
    question = {question},
    users = {to},
    )

    self.saved_events["{step_name}"] = self.current_event
    engine.unregister_event(self, self.current_event.event_type)


    self.current_event = None
    self.confirmation_guid = u""
    return self.step_{step_name}
    """

archive_step = u"""def step_{step_name}(self):
    if {to}:
    EOFAPI.archive_document(
    step_name	= '{step_name}',
    log 		= {log},
    workflow 	= self,
    destination = {to},
    autoCreate 	= {autoCreateArchive},
    index		= {index},
    document	= {document},
    ocr_content	= self.document_content
    )
    self.laststep_name = '{step_name}'
    """

notification_step = u"""def step_{step_name}(self):
    self.step_data["{step_name}"] = {{ "notified" : 0 }}

    if {notifyByEmail}:
    EOFAPI.send_mail(
    log 		= {log},
    workflow		= self,
    users 			= {emailTo},
    email_title 	= {emailObject},
    email_content 	= {emailContent},
    email_attach 	= {emailAttachDocument},
    email_doc 		= {emailDocument}
    )

    if {notifyByEmail} and not {notifyByPopup}:
    EOFAPI.send_log("step_notify_only_mail",
    workflow 		= self,
    step_name 		= '{step_name}',
    users_popup		= {popupTo},
    info			= {popupContent},

    users_mail		= {emailTo},
    mail_content	= {emailContent},
    attachment		= {emailAttachDocument}
    )

    if {notifyByPopup}:

    notified = EOFAPI.send_popup(
    log 		= {log},
    workflow 	= self,
    users		= {popupTo},
    content		= {popupContent}
    )
    if notified:
    return self.handler_{step_name}
    self.laststep_name = '{step_name}'

    def handler_{step_name}(self):
    if {log}:
    step_type = "step_notify_mail"

    if {notifyByPopup} and {notifyByEmail}:
    step_type = "step_notify_mail"

    elif {notifyByPopup}:
    step_type = "step_notify_no_mail"

    EOFAPI.send_log(step_type,
    workflow 		= self,
    step_name 		= '{step_name}',
    users_popup		= {popupTo},
    info			= {popupContent},

    users_mail		= {emailTo},
    mail_content	= {emailContent},
    attachment		= {emailAttachDocument}
    )

    self.step_data["{step_name}"]['notified'] = 1
    self.saved_events["{step_name}"] = self.current_event
    engine.unregister_event(self, self.current_event.event_type)
    self.current_event = None

    return self.step_{step_name}
    """

gateway_step = u"""def step_{step_name}(self):
    if {log}:
    message = "Gateway step {step_name} has been processed."
    engine.log(self.id, message, "info")

    self.laststep_name = '{step_name}'

    """
scripting_step = u"""def step_{step_name}(self):
    EOFAPI.b_exec(
    step_name 	= '{step_name}',
    log 		= {log},
    workflow 	= self,
    source 		= {source}
    )
    self.laststep_name = '{step_name}'

    """
transition_auto = u"""def transition_{transition_name}(self):
    if {log}:
    message = "Auto transition {transition_name} has been processed."
    engine.log(self.id, message, "info")

    step_type = "auto"
    EOFAPI.send_log(step_type,
    workflow 	= self,
    step_name 	= '{transition_name}',
    step_from 	= self.laststep_name,
    step_to		= '{connected_step}'
    )
    return self.step_{connected_step}

    """

transition_condition = u"""def transition_{transition_name}(self):
    if {condition}:
    if {log}:
    message = "Conditional transition {transition_name} has been processed."
    engine.log(self.id, message, "info")

    step_type = "conditional"
    EOFAPI.send_log(step_type,
    workflow 	= self,
    step_name 	= '{transition_name}',
    step_from 	= self.laststep_name,
    step_to		= '{connected_step}'
    )
    return self.step_{connected_step}
    else:
    return None

    """

transition_error = u"""def transition_{transition_name}(self):
    if {log}:
    message = "Error transition {transition_name} has been processed with Error code %s"%({errorCode},)
    engine.log(self.id, message, "info")

    step_type = "error"
    EOFAPI.send_log(step_type,
    workflow 	= self,
    step_name 	= '{transition_name}',
    step_from 	= self.laststep_name,
    step_to		= '{connected_step}'
    )
    return self.step_{connected_step}

    """

transition_timeout = u"""def transition_{transition_name}(self):
    time_arr = map(int,({timeoutAfter}).split(':'))
    if len(time_arr) == 4:
    #time = ((time_arr[0]*24+time_arr[1])*60+time_arr[2])*60+time_arr[3]

    if self.update_time + datetime.timedelta(days = time_arr[0],
    hours=time_arr[1],minutes=time_arr[2], seconds = time_arr[3])<=datetime.datetime.now():
    if {log}:
    message = "Timeout transition {transition_name} has been processed after timeout %s."%({timeoutAfter},)
    engine.log(self.id, message, "info")

    step_type = "timeout"
    EOFAPI.send_log(step_type,
    workflow 	= self,
    step_name 	= '{transition_name}',
    step_from 	= self.laststep_name,
    step_to		= '{connected_step}'
    )
    return self.step_{connected_step}
    """

final_step = u"""def step_{step_name}(self):
    EOFAPI.action_workflow_trigger(
    log = {log},
    action_name	="workflowFinished",
    workflow	=self,
    file_id=self.current_document,
    user_id=self.creator_id,
    log_data = {logData}
    )
    return True

    """