from engine_core import engine, global_log
from datetime import datetime
import cgi

TYPES={0:"debug", 1:"info", 2:"error"}
LIMIT = 50

class RecordLog( object ):

    def __init__( self ):

        self.type 		= 0				# type can be: debug, info, error OR 0, 1, 2
        self._content 	= ""
        self.trace 		= ""
        self.time 		= None

    def save(self):
        #filter for type -- leave the int representation or change it from same string value to int.
        for k, v in TYPES.items():
            if self.type == k or str(self.type).lower() == v:
                self.type = k
                break

        #check type for constraints and non-empty content
        if self.type in TYPES:
            self.time = datetime.now()
            if not self.content:
                self.content = "[empty content]"
            return self
        else:
            raise Exception("EMPTY OR BAD LOG VALUES: type=%s, content=%s"%(self.type, self.content))

    def put_into(self, wf_log):
        self.save()
        wf_log.put(self)

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, value):
        self._content = value#cgi.escape(value).replace("&lt;b&gt;","<b>").replace("&lt;/b&gt;","</b>").replace("&lt;i&gt;","<i>").replace("&lt;/i&gt;","</i>")


    @classmethod
    def get_list( self, log_list = None, index = 0 ):
        log_list = log_list if log_list else engine.global_log
#		raise Exception( list )
        data = []
        debug = []
        if index is None:
            index = 0
        for record in log_list:
            if record != '' and log_list.index( record ) < index + LIMIT:
                if isinstance( record, RecordLog ):
                    data.append( record )
                    debug.append( 'instance' )
                else:
                    rec = RecordLog()
                    rec.type = 'debug'
                    rec.content = record
                    rec.time = datetime.now()
                    data.append( rec )
        return data

    @classmethod
    def get_datatable_data( self, log_list = None ):
        import localization
        loc = localization.get_lang()
        if log_list is None:
            log_list = self.get_list()
#		raise Exception( list )
        data = []
        for i in log_list:
            if not isinstance( i, list ):
                time = i.time.strftime( '%H:%M:%S' ) if type( i.time ) == type( datetime.now() ) else i.time
                date = i.time.strftime( '%d-%m-%y' ) if type( i.time ) == type( datetime.now() ) else i.time
                type_ = i.type if isinstance( i.type, str ) else TYPES.get( i.type, 'info' )
                css_class = '<span class="%s"/>' % str(type_ + '-color')
                type_loc = {
                    "debug"	: loc["workflowlog_log_debug"],
                    "info"	: loc["workflowlog_log_info"],
                    "error"	: loc["workflowlog_log_error"],
                    }[type_]
                row = [ css_class, type_loc,
                        i.content,#cgi.escape(i.content).replace("&lt;b&gt;","<b>").replace("&lt;/b&gt;","</b>").replace("&lt;i&gt;","<i>").replace("&lt;/i&gt;","</i>"),
                        time, date ]
                data.append( row )
            else: data.append( i )
        return data

    @classmethod
    def sort_by_category( self, log_list = None, reverse = False ):
        import localization
        loc = localization.get_lang()
        if log_list is None:
            log_list = self.get_list()
        info = []
        debug = []
        error = []
        for i in log_list:
            if i.type == 'error' or i.type == 2:
                error.append( i )
            if i.type == 'info' or i.type == 1:
                info.append( i )
            if i.type == 'debug' or i.type == 0:
                debug.append( i )

#		raise Exception( [ i.type for i in list ], len( error ), len( info ), len( debug ) )
        info = sorted( info, key = lambda k: k.time )
        debug = sorted( debug, key = lambda k: k.time )
        error = sorted( error, key = lambda k: k.time )
#		raise Exception( len(list), len(info) + len(debug) + len(error) )
        if reverse:
            info.reverse()
            debug.reverse()
            error.reverse()

        show_info = session.get( 'show_info', True )
        show_error = session.get( 'show_error', True )
        show_debug = session.get( 'show_debug', True )
        data = []
        if show_info and info:
            data.append( [ u'<span class="info-color"/>', '', u'<b>%s</b>'%loc["workflowlog_log_info"], '', ''] )
            for i in info:
                data.append( i )
        if show_debug and debug:
            data.append( [ u'<span class="debug-color"/>', '', u'<b>%s</b>'%loc["workflowlog_log_debug"], '', '' ] )
            for i in debug:
                data.append( i )
        if show_error and error:
            data.append( [ u'<span class="error-color"/>', '', u'<b>%s</b>' % loc["workflowlog_log_error"], '', '' ] )
            for i in error:
                data.append( i )

        return data

    @classmethod
    def sort_by_time( self, log_list = None, reverse = False ):
        if log_list is None:
            log_list = self.get_list()

        show_info = session.get( 'show_info', True )
        show_error = session.get( 'show_error', True )
        show_debug = session.get( 'show_debug', True )
        if not show_debug:
            for i in log_list:
                if i.type == 'debug' or i.type == 0:
                    log_list.remove( i )
        if not show_info:
            for i in log_list:
                if i.type == 'info' or i.type == 1:
                    log_list.remove( i )
        if not show_error:
            for i in log_list:
                if i.type == 'error' or i.type == 2:
                    log_list.remove( i )

        log_list = sorted( log_list, key = lambda k: k.time )
        if reverse:
            log_list.reverse()

        return log_list


    @classmethod
    def hide_type( self, type = None, log_list = None ):
        if type == None:
            return
        if log_list is None:
            log_list = self.get_list()
#		raise Exception( type )
        str_type = TYPES[ type ] if isinstance( type, int ) else type
        int_type = -1

        if isinstance( type, str ):
            for key, value in TYPES.items():
                if value == type:
                    int_type = key

        for i in log_list:
            if not isinstance( i, list ):
                if i.type == str_type or i.type == int_type:
                    log_list.remove( i )
        return log_list