'''
	CRUD model class
'''

###" IMPORTS "###
from class_db import Database

###" GLOBALS "###

# below is a RELEVANT database template
tables = {
    'acl'			: ["id","subject","object","rule"],
    'share_file'	: ["id","parent_guid","guid","name","path","is_dir"],
    'workflow_library'	: ["id","name","source","description"],
    'workflow'		: ["id","name","description","status","modified","error","source", "guid","image","folder", "compilable"],
    'workflow_instance'	: ["id","workflow_guid","author_guid","current_user_guid","document_guid","guid","status","state"],
    'smart_sub_folder'	: ["id","parent_guid", "guid", "name"],
    'smart_folder'	: ["id", "guid", "name"],
    'backup_task'	: ["id", "mode", "interval", "unit", "at_time","rotation","destination","status","last_backup"]

}
exceptions = ["order_by","limit","offset"]

class CRUD:

    @staticmethod
    def validate_args(to_check, table_name):
        if not table_name in tables:
            raise Exception(
                "Wrong table name. Current relevant table names: %s" % ", ".join(tables.keys())
            )
        if not all( str(key) in tables[table_name] or str(key) in exceptions for key in to_check.keys() ):
            raise Exception(
                "Wrong arguments. Check spelling: %s" % ", ".join(tables[table_name])
            )

        return True


    @staticmethod
    def create(table_name, **kwargs):
        CRUD.validate_args(kwargs, table_name)
        query = """ INSERT INTO `%(table_name)s`(%(inputs)s) VALUES(%(values)s) """
        values = ""
        inputs = ""
        query_parse = []

        if any(a in kwargs.keys() and kwargs[a] for a in tables[table_name][1:]):
            args 		= [  [k,v] 	for k,v in kwargs.iteritems()]
            inputs		= ", ".join([k[0] 	for k in args])
            values 		= ", ".join(["?" 	for k in args])
            query_parse = tuple( [k[1] for k in args] )

        query = query % {
            "table_name" 	: table_name,
            "inputs"     	: inputs,
            "values" 		: values
        }
        id = Database.maindb().commit(query, query_parse)
        return id

    @staticmethod
    def retrieve(table_name, **kwargs):
        CRUD.validate_args(kwargs, table_name)
        query = """ SELECT * FROM `%(table_name)s` %(conditions)s %(order_by)s %(limit)s %(offset)s"""
        values = ""
        order_by = ""
        query_parse = []


        order_by 	= kwargs["order_by"] if "order_by" in kwargs and kwargs["order_by"] in tables[table_name] else ""
        order_by 	= "ORDER BY %s.%s" % (table_name, order_by) if order_by else ""
        limit 		= kwargs["limit"] if "limit" in kwargs else ""
        limit 		= "LIMIT %s" % limit if (limit != "") else ""
        offset 		= kwargs["offset"] if "offset" in kwargs else ""
        if "offset" in kwargs and "limit" not in kwargs:
            raise Exception("OFFSET must be with LIMIT parameter.")
        offset 		= "OFFSET %s" % offset if (offset != "" and limit != "") else ""

        if any(a in kwargs.keys() for a in tables[table_name]):
            for k,v in kwargs.iteritems():
                kwargs[k] = v if v.__class__==list else [v]

            args = dict([k,"%s.%s=?" % (table_name, k)] for k in tables[table_name])

            where_args= []
            for key in args.keys():
                if key in kwargs.keys():
                    temp = []
                    for value in kwargs[key]:
                        temp.append(args[key])
                        query_parse.append(value)
                    where_args.append( "(%s)"%" or ".join(temp) )

            values = "WHERE %s" % " and ".join(where_args)

        query = query % {
            "table_name" : table_name,
            "conditions" : values,
            "order_by"	 : order_by,
            "limit"		 : limit,
            "offset"	 : offset
        }
        query_parse = tuple(query_parse)
        rows = Database.maindb().fetch_all(query, query_parse)
        return rows

    @staticmethod
    def retrieve_one(table_name, **kwargs):
        CRUD.validate_args(kwargs, table_name)
        query = """ SELECT * FROM `%(table_name)s` %(conditions)s %(order_by)s"""
        values = ""
        order_by = ""
        query_parse = []

        if any(a in kwargs.keys() for a in tables[table_name]):
            for k,v in kwargs.iteritems():
                kwargs[k] = v if v.__class__==list else [v]

            args = dict([k,"%s.%s=?" % (table_name, k)] for k in tables[table_name])

            where_args= []
            for key in args.keys():
                if key in kwargs.keys():
                    temp = []
                    for value in kwargs[key]:
                        temp.append(args[key])
                        query_parse.append(value)
                    where_args.append( "(%s)"%" or ".join(temp) )

            values = "WHERE %s" % " and ".join(where_args)
        order_by 	= kwargs["order_by"] if "order_by" in kwargs and kwargs["order_by"] in tables[table_name] else ""
        order_by 	= "ORDER BY %s.%s" % (table_name, order_by) if order_by else ""

        query = query % {
            "table_name" : table_name,
            "conditions" : values,
            "order_by"	 : order_by
        }
        query_parse = tuple(query_parse)

        row = Database.maindb().fetch_one(query, query_parse)
        return row

    @staticmethod
    def update(table_name, where_args={}, **kwargs):
        CRUD.validate_args(kwargs, table_name)
        CRUD.validate_args(where_args, table_name)
        query = """ UPDATE `%(table_name)s` SET %(values)s %(conditions)s """
        values = ""
        conditions = ""
        query_parse = []

        if any(a in kwargs.keys() and kwargs[a] for a in tables[table_name][1:]):
            args = [[k,v] for k,v in kwargs.iteritems()]
            values  = "%s"%", ".join(["%s = ?" % k[0] for k in args])
            query_parse = [k[1] for k in args]

            if where_args:
                args = [[k,v] for k,v in where_args.iteritems()]
                conditions = "WHERE %s"%", ".join(["%s = ?" % k[0] for k in args])
                for k in args: query_parse.append(k[1])

            query = query % {
                "table_name" : table_name,
                "conditions" : conditions,
                "values" : values
            }
            query_parse = tuple(query_parse)

        row = Database.maindb().commit(query, query_parse)
        return row

    @staticmethod
    def delete(table_name, **kwargs):
        CRUD.validate_args(kwargs, table_name)
        query = """ DELETE FROM %(table_name)s %(conditions)s """
        conditions = ""
        query_parse = []

        if any(a in kwargs.keys() for a in tables[table_name]):
            for k,v in kwargs.iteritems():
                kwargs[k] = v if v.__class__==list else [v]

            args = dict([k,"%s.%s=?" % (table_name, k)] for k in tables[table_name])

            where_args= []
            for key in args.keys():
                if key in kwargs.keys() and kwargs[key]:
                    temp = []
                    for value in kwargs[key] :
                        temp.append(args[key])
                        query_parse.append(value)
                    where_args.append( "(%s)"%" or ".join(temp) )

            conditions = "WHERE %s" % " and ".join(where_args)

        query = query % {
            "table_name" : table_name,
            "conditions" : conditions
        }
        query_parse = tuple(query_parse)
        row = Database.maindb().commit(query ,query_parse)
        return row