from engine_core import engine, global_log
from datetime import datetime

STATE={0:"active", 1:"finished", 2:"error"}
STATE_REVERSE={"active":0, "finished":1, "error":2}
LIMIT = 50

class RecordWorkflow( object ):
    def __init__( self ):
        self._logs = []
        self.id 			= ''
        self._state 		= '' 			# state can be: active, finished, error OR 0, 1, 2
        self.name 			= ''
        self.document_id 	= ''
        self.document_name 	= ''
        self.classname 		= ''
        self.time_started 	= ''
        self.time_finished 	= datetime.now()
        self.user_id		= ''

    @property
    def state( self ):
        return self._state

    @state.setter
    def state( self, value ):
        self._state = STATE_REVERSE.get( value, -1 ) if not isinstance( value, int ) else value



    # put RecordLog into current object stack
    def put(self, rlog):
        from class_record_log import RecordLog
        if isinstance(rlog, RecordLog):
            self._logs.append(rlog)

    def start(self):
        self.state 			= 0
        self.time_started 	= datetime.now()

    def finish(self, error = False):
        self.state 			= 2 if error else 1
        self.time_finished 	= datetime.now()


    def get_logs( self, index = 0 ):
#		result = []
#		for record in global_log:
#			if isinstance( record, RecordLog ):
#				if record.workflow_log == self:
#					result.append( record )
#		return result
        from copy import copy
        if index is None:
            index = 0
        return copy( self._logs[ index:index+50 ] )


    @classmethod
    def get_list( self ):
        data = []
#		raise Exception( engine.workflow_log )
        for key, value in engine.workflow_log.items():
            if isinstance( value, RecordWorkflow ):
                if value.id in engine.running_workflows:
                    value.time_finished =  engine.running_workflows[value.id].update_time
                data.append( value )
            else:
                record = RecordWorkflow()
                record.id = key
                record.name = value
                record.state = 'active' if key in engine.running_workflows else 'finished'
                if record.id in engine.running_workflows:
                    record.time_finished = engine.running_workflows[record.id].update_time
                else:
                    record.time_finished = datetime.now()
                data.append( record )

        data.sort(key=lambda record: engine.running_workflows[record.id].update_time if record.id in engine.running_workflows else  record.time_finished)
#		rec = RecordWorkflow()
#		rec.id = -13
#		rec.name = 'test finish'
#		rec.time = datetime.now()
#		rec.state = 'error'
#		data.append( rec )
#
#		rec = RecordWorkflow()
#		rec.id = -14
#		rec.name = 'test active'
#		rec.time = datetime.now()
#		rec.state = 'active'
#		data.append( rec )
#
#		rec = RecordWorkflow()
#		rec.id = -17
#		rec.name = 'test all'
#		rec.time = datetime.now()
#		rec.state = 'finished'
#
#		data.append( rec )
#
#		rec = RecordWorkflow()
#		rec.id = -12
#		rec.name = 'sfasdfsd 0'
#		rec.time = datetime.now()
#		rec.state = 0
#		data.append( rec )
#
#		rec = RecordWorkflow()
#		rec.id = -12
#		rec.name = 'dflk 1'
#		rec.time = datetime.now()
#		rec.state = 1
#		data.append( rec )

#		raise Exception( data )
        return data

    @classmethod
    def get_by_id( self, id ):
        record = engine.workflow_log[ id ] if id in engine.workflow_log else None
        if isinstance( record, str ):
            result = RecordWorkflow()
            result.id = id
            result.state = 'active' if id in engine.running_workflows else 'finished'
            result.name = record
            result.time_finished = datetime.now()
            record = result
        return record


    @classmethod
    def get_datatable_data( self, log_list = None ):
        if log_list is None:
            log_list = self.get_list()
        data = []
        for record in log_list:
            time = record.time_finished if isinstance( record.time_finished, str ) else record.time_finished.strftime( '%H:%M:%S' )
            date = record.time_finished if isinstance( record.time_finished, str ) else record.time_finished.strftime( '%d-%m-%y' )
            state = record.state if isinstance( record.state, str ) else STATE [record.state]
            span = ''
            if state == 'active' or state == 0:
                span = '<span class=active-step/>'
            elif state == 'finished' or state == 1:
                span = '<span class=finished-ok/>'
            elif state == 'error' or state == 2:
                span = '<span class=finished-failed/>'
            row = [ record.id, span, record.name, time, date ]
            data.append( row )
#		rec = [ 'o', '<span class=finished-failed/>', 'ewiuf osdjfdkjfoie feiu ', datetime.now().strftime( '%H:%M:%S' ), datetime.now().strftime( '%d-%m-%y' )]
#		data.append(rec)
#		rec = [ 'o', '<span class=finished-ok/>', 'dfkj sdfj joeiufsdljflsdf', datetime.now().strftime( '%H:%M:%S' ), datetime.now().strftime( '%d-%m-%y' )]
#		data.append(rec)
#		rec = [ 'o', '<span class=active-step/>', 'fja sdlfjas dlfkja dslfj sdlfkj', datetime.now().strftime( '%H:%M:%S' ), datetime.now().strftime( '%d-%m-%y' ) ]
#		data.append(rec)
        return data


#	@classmethod
#	def hide( self, value, log_list = None ):
##		value_int = value if isinstance( value, int ) else
#		if log_list is None:
#			log_list = self.get_list()
#		if value == 'show_all':
#			return log_list
#		for record in log_list:
#			if record.state == value or record.state == STATE.get( value, None ) or record.state == STATE_REVERSE.get( value, None ):
#				log_list.remove( record )
#		if value == 'finished' or value == 1 or value == '1':
#			self.hide( value = 'error', log_list = log_list )
#		return log_list


    @classmethod
    def hide( self, value, log_list = None ):
        if log_list is None:
            log_list = self.get_list()
        if value == 'show_all':
            return log_list
        if not isinstance( value, int ):
            value = STATE_REVERSE.get( value, -1 )
        result = []
        for record in log_list:
            if record.state != value:
                result.append( record )
        if value == 'finished' or value == 1:
            result = self.hide( value = 2, log_list = result )
        return result

    @classmethod
    def select( self, value, log_list = None ):
        if log_list is None:
            log_list = self.get_list()
        if not isinstance( value, int ):
            value = STATE_REVERSE.get( value, -1 )
        result = []
        for record in log_list:
            if record.state == value:
                result.append( record )
        return result

