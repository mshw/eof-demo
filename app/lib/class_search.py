import re
from uuid import uuid4


from class_ACL import ACL

from class_db import Database
from class_folder import Folder
from class_metafield import Expression_type, Metas_type, Expression
from class_document_share import Document_share
from class_user import User
import localization




class ISearch:

    def __init__(self, row=None):
        if row:
            self.id = row[0]
            self.guid = row[1]
            self.name = row[2]
        else:
            self.id = ""
            self.guid = ""
            self.name = ""


    #@property
    #def

    @classmethod
    def get_all(self):
        query = """SELECT id, guid, name
					FROM search
					ORDER BY name """
        rows = Database.maindb().fetch_all(query,)

        return [ISearch(row) for row in rows]


    def delete(self):
        # delete old expressions
        for expression in  Expression.get_by_search_guid(self.guid):
            expression.delete()
        # delete old objects
        for obj in  Search_object.get_by_search_guid(self.guid):
            obj.delete()

        for acl in ACL.get_by_object(self):
            acl.delete()

        query = """DELETE FROM search
						WHERE guid=? """
        Database.maindb().commit(query, (str(self.guid),))

    def folder(self):
        obj = Search_object.get_by_search_guid(self.guid)
        if obj:
            obj = obj[0]
            return obj.object_guid


class Search():


    @classmethod
    def get_all(self):
        query = """SELECT id, guid, name
					FROM search
					"""
        rows = Database.maindb().fetch_all(query,)

        return [Search(row) for row in rows]


    @classmethod
    def get_by_guid(self, guid):
        query = """SELECT id, guid, name
					FROM search
					WHERE guid=?"""
        row = Database.maindb().fetch_one(query, (str(guid),))

        if row:
            return Search(row)
        return None

    def __init__(self, row=None):

        self.loc = localization.get_lang()


        self.__objects = []
        self.__doc_shares = []
        self.__metas = {}
        self.__expressions  = []
        self.__query = []

        meta_type = Metas_type.get_by_id(1)
        if meta_type:
            item = {}
            descr = meta_type.description.encode("utf8")
            item["title"] = self.loc[descr] if descr in self.loc else descr
            item["regexp"] = meta_type.regexp.encode("UTF-8")
            item["expr"] = meta_type.get_expressions()
            item["name"] = self.loc["mf_auto_text"]
            item["isdate"] = 0;
            self.__metas["text"] = item

        if row:
            self.id = row[0]
            self.guid = row[1]
            self.name = row[2]

            self.__expressions =  Expression.get_by_search_guid(self.guid)
            self.__objects = Search_object.get_by_search_guid(self.guid)
            self.__validate_doc_shares()
            self.__validate_query()

        else:
            self.id = ""
            self.guid = ""
            self.name = ""

    def __str__(self):
        return self and self.name+"_"+self.guid

    def set_objects(self, objects, parent=None):
        self.__objects = objects
        self.__validate_doc_shares()

    def get_objects(self):
        return self.__objects

    def __load_objects(self):
        # load object from DB
        pass

    def delete(self):
        # delete old expressions
        for expression in  Expression.get_by_search_guid(self.guid):
            expression.delete()
        # delete old objects
        for obj in  Search_object.get_by_search_guid(self.guid):
            obj.delete()

        for acl in ACL.get_by_object(self.guid):
            acl.delete()

        query = """DELETE FROM search
						WHERE guid=? """
        Database.maindb().commit(query, (str(self.guid),))

    def save(self):
        # save search
        if self.guid !="" and Search.get_by_guid(self.guid):
            # delete old expressions
            for expression in  Expression.get_by_search_guid(self.guid):
                expression.delete()
            # delete old objects
            for obj in  Search_object.get_by_search_guid(self.guid):
                obj.delete()

            query = """UPDATE search
						SET name=? WHERE guid=?"""
            Database.maindb().commit(query, (self.name,self.guid ,))
        else:
            if not self.guid:
                self.guid =  str(uuid4())

            query = """INSERT INTO search(guid, name)
						VALUES(?,?)"""
            row = Database.maindb().commit( query,( self.guid , self.name))


        # add new expressions
        for expression in self.__expressions:
            expression.search_guid =  self.guid
            expression.save()

        # add new objects
        for obj in self.__objects:
            obj.search_guid = self.guid
            obj.save()





    """
	def get_metas(self):
		if self.__objects:
			return [ [meta.name, meta.name] for meta in self.__objects.get_metafields()]
		else:
			return []
	"""
    def get_metas(self):
        #raise Exception(str(self.__metas))
        return [[meta, self.__metas[meta]["name"]] for meta in self.__metas]


    def	__validate_doc_shares(self):
        #raise Exseption("No worc	")
        for obj in self.__objects:
            if obj.parent_guid != "":
                folder = Folder.get_by_guid(obj.parent_guid)
                doc_share = Document_share	.get_by_guid(obj.object_guid)

                if doc_share and folder:
                    self.__doc_shares.append([doc_share, folder])
            else:
                folder = Folder.get_by_guid(obj.object_guid)
                if folder:
                    self.__set_doc_shares(folder)

        self.__validate_metas()

    def __validate_metas(self):
        from class_db import Database

        shares_arr = [doc[0].guid for doc in self.__doc_shares]
        shares_str = "','".join(shares_arr)

        if self.__doc_shares:
            query	= """SELECT name, type_id
							FROM (SELECT DISTINCT metafield_in_doc_share.doc_share_guid, name, type_id
								FROM metafield
								INNER JOIN metafield_in_doc_share
									ON metafield.id=metafield_in_doc_share.metafield_id
								WHERE metafield_in_doc_share.doc_share_guid IN ('"""+shares_str+ """')
								)
							GROUP BY name, type_id
							HAVING Count(name) ="""+ str(len(shares_arr))
            rows = Database.maindb().fetch_all(query,)
            #raise Exception(str([row[1] for row in rows]).replace("<","&lt;"))
            for row in rows:
                meta_type_id = row[1]
                meta_name = row[0]
                meta_type = Metas_type.get_by_id(meta_type_id)
                if meta_type:
                    #metas.append([row[0], row[0]])
                    item = {}
                    descr = meta_type.description.encode("utf8")
                    item["title"] = self.loc[descr] if descr in self.loc else descr
                    item["regexp"] = meta_type.regexp.encode("UTF-8")
                    item["expr"] = meta_type.get_expressions()
                    item["field3"] = "[]"
                    item["name"] = self.loc[meta_name]	if meta_name in self.loc else meta_name
                    item["isdate"] = 0 if meta_type.id != 2 else 1
                    self.__metas[row[0]] = item
            #return metas

    def get_expressions_dic(self,):
        import json
        #debug("metas arr: "+str(self.__metas))
        return json.dumps(self.__metas)

    def get_expressions(self,):
        return self.__expressions

    # set available to the user doc_shares
    def __set_doc_shares(self, folder):
        user = User.current()
        if folder:
            for doc_share  in user.get_documents_shares(folder):
                self.__doc_shares.append([doc_share, folder])
            for s_folder  in user.get_subfolders(folder):
                self.__set_doc_shares(s_folder)

    def get_top_meta(self):
        for meta in self.__metas:
            return meta
    #def get_star_expr(self):
    #	return  self.__metas["text"]["expr"]#  [[ "max", "Max"], [ "min", "Min"]]

    def get_expr_types(self, meta_name):
        if meta_name in self.__metas:
            return  self.__metas[meta_name]["expr"]
        return ""

    def get_desription(self, meta_name):
        if meta_name in self.__metas:
            return  self.__metas[meta_name]["title"]#  [[ "max", "Max"], [ "min", "Min"]]
        return ""


    def is_valid_value(self, exprn):
#		if isinstance(exprn.meta, list):
#			raise Exception(str(exprn.meta))
        regex = self.get_regexp(exprn.meta.encode("utf8") if isinstance(exprn.meta, str) else exprn.meta[0].encode("utf8"))#self.__metas[exprn.meta.encode("utf8")]["regexp"]
        reobj = re.compile(regex, re.MULTILINE)

        if reobj.search(exprn.value):
            return True

        return False

    def is_date(self, meta_name):
        if meta_name in self.__metas:
            return  self.__metas[meta_name]["isdate"] == 1
        return False




    def get_regexp(self, meta_name):
        if meta_name in self.__metas:
            res =  self.__metas[meta_name]["regexp"]
            if res and res != None:
                return 	res
        return 	""

    def set_expressions(self, data):
        self.__expressions = data
        self.__validate_query()

    def __validate_query(self):
        self.__query = []

        from class_smart_folder import SmartSubFolder
        found_smartsubfolders = []
        ssf_all = SmartSubFolder.all()
        search_for_ssf = dict( [(ssf.name.lower(), ssf ) for ssf in ssf_all] )

        for exprn in self.__expressions:
            if self.is_valid_value(exprn):
                #raise Exception(str(exprn.value))
                #if exprn.meta_type==5:
                if exprn.value.lower() in search_for_ssf:
                    sm = search_for_ssf[exprn.value.lower()].parent_metafields
                    if sm:
                        for m in sm:
                            if m.name == exprn.meta:
                                found = search_for_ssf[exprn.value.lower()]
                                self.__query.append({"meta":exprn.meta, "value":found.guid+" "+found.name, "type":exprn.expr_type  })
                                break
                else:
                    self.__query.append({"meta":exprn.meta, "value":exprn.value, "type":exprn.expr_type  })
            else:
                #raise Exception("No Valid value")
                self.__query = []
                return

        #raise Exception(str(self.__query))


    def get_query(self):
        #raise Exception(str(self.__query))
        return self.__query # [{"meta":str(meta.id), "value":word, "type":"" }]




class Search_object:

    @classmethod
    def get_all(self):
        query = """SELECT id,  search_guid, object_guid, parent_guid
					FROM search_object"""
        rows = Database.maindb().fetch_all(query)

        return [Search_object(row) for row in rows]

    @classmethod
    def get_by_search_guid(self, search_guid):
        query = """SELECT id,  search_guid, object_guid, parent_guid
					FROM search_object
					WHERE search_guid=?"""
        rows = Database.maindb().fetch_all(query, (str(search_guid),))

        return [Search_object(row) for row in rows]


    def __init__(self, row=None):
        if row:
            self.id = row[0]
            self.search_guid = row[1]
            self.object_guid = row[2]
            self.parent_guid = row[3]
        else:
            self.id = 0
            self.search_guid = ""
            self.object_guid = ""
            self.parent_guid = ""

    def save(self):
        query = """INSERT INTO search_object(search_guid, object_guid, parent_guid  )
					VALUES(?,?,?)"""
        row = Database.maindb().commit( query,(self.search_guid, self.object_guid, self.parent_guid))

    def delete(self):
        # delete
        query = """DELETE FROM search_object
					WHERE id=? """
        Database.maindb().commit(query, (self.id,))