"""
	Class for storing key:value data into table 'config'

	get_value 	- gets the value from config by input key
	set_value 	- save key and value inside current instance, but doesn't save the data inside config.
	commit 		- save all data in self.values inside config. Overwrites already existing items.
"""


from class_db import Database

class Config(object):

    def __init__(self):
        self.values = {}

    def get_value(self, key):
        query = """
			SELECT value FROM config
			WHERE item=?
		"""
        result = Database.maindb().fetch_one(query, (key,))
        if result:
            self.values[key] = result[0]
            return self.values[key]
        else:
            return None

    def set_value(self, key, value):
        self.values[key] = value
        return True

    def commit(self):
        query = """INSERT OR REPLACE INTO config (item, value ) VALUES (?,?)"""
        for key, value in self.values.items():
            Database.maindb().commit(query,(key, value))
        return True