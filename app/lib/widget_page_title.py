
PAGE_TITLE = """"%s &raquo; %s" """

class WidgetPageTitle(object):

    def __init__(self, page):
        self.__page = page
        self.__title_key = ""

    def set_data(self, title_key):
        self.__title_key = title_key

    def render(self):
        import localization
        loc = localization.get_lang()

        self.__page.title = PAGE_TITLE % (
            loc["header_hgmb"],
            loc.get(self.__title_key, "Unknown page")
        )