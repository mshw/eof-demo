


TITLE_TEMPLATE = """ %(style)s
	<div class='%(css_class)s'>
		%(title)s
	</div>
"""

# would be used if css_class is empty
CSS_INTERNAL = """
<style>
	.content_title_default
	{
		border-bottom:3px solid #E7E7DB!important;
		font-size: 24px;
		padding-bottom: 6px;
	}
</style>
"""

class WidgetContentTitle:

    def __init__(self, html=None):
        self.__html	= html
        self.__title_key	= ""
        self.__css_class	= ""

    # if you do not overide the css_class, that will use current one.
    def set_data(self, title_key, css_class="content_title_default"):
        import localization
        loc = localization.get_lang()

        self.__title		= loc.get(title_key, "Workflows")
        self.__css_class	= css_class

    def render(self):
        result = TITLE_TEMPLATE % {
            "style"		: CSS_INTERNAL,
            "css_class"	: self.__css_class,
            "title"		: self.__title
        }
        if self.__html:
            self.__html= result
        else:
            return result

