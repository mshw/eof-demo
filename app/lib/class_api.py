from engine_core import engine
from xappy_indexconnector import indexconnector
from class_user import User
from class_xml_action import xml_action
from class_document_share import Document_share
from class_document import Document, wDocument
from class_folder import Folder
from class_share_file import ShareFile
from class_metafield import Metafield
from class_share_dir import ShareDir
from class_smart_folder import SmartFolder, SmartSubFolder
from class_workflow_scripting import UserScriptObject, UserContainerScriptObject
from storage import storage

import localization, class_xml_action
import shutil, cgi, os, os.path, re

class EOFAPI:

    @staticmethod
    def admin_email():
        admin = User.current_admin()
        return admin.email if admin else ""

    @staticmethod
    def b_exec(step_name, log, workflow, source):
        from class_workflow_scripting_v import v_WorkflowScriptObject, v_logger, dbdictionary
        from class_workflow_scripting_v_eof import globalApi, v_system
        from vscript import engine as eng

        if not source:
            if log:
                engine.log(workflow.id,  "VScript step: empty source. Exit script step.", "info")
            return

        if log:
            engine.log(workflow.id,  "VScript initializing.", "debug")

            engine.workflow_log[workflow.id].reportworkflow.set_data("step_script",
                                                                     step_name=step_name,
                                                                     info=u"VScript had been initialized",
                                                                     )

        wf = engine.running_workflows[workflow.id]
        cmpl = eng.vcompile(source)
        vscript_wrappers_name="wrappers"
        g_api = globalApi()
        g_api.set_workflow(wf)
        try:
            server.vscript.execute(
                source, # source

                **{

                    'workflow' 		: v_WorkflowScriptObject( wf ),
                    'logger' 		: v_logger(workflow.id).v_log,
                    'report'		: v_logger(workflow.id).v_report,

                    'search'		: g_api.v_search,
                    'searchindex' 	: g_api.v_searchindex,
                    'searchfulltext': g_api.v_searchfulltext,
                    'getsmartfolder': g_api.v_getsmartfolder,
                    'setuser'		: g_api.v_setuser,
                    'send_email'	: v_system.v_send_email,
                    'setreferencevalue':g_api.v_setreferencevalue,
                    'get_share_by_path': g_api.v_get_share_by_path,
                    'cutthecomment'	: g_api.v_cutthecomment,
                    'sleep'			: g_api.v_sleep,

                    "dbconnection": vscript_wrappers_name,
                    "dbdictionary":dbdictionary(wf.name, "db_custom_wf"),
                }
            )
            g_api.v_commit()
        except:
            engine.log(workflow.id, "Error while vscript execution.")
            from class_trace import Trace
            engine.log(workflow.id, Trace.exception_trace())
#		if log:
#			engine.log(workflow.id,  "VScript successfully launched.", "debug")


    @staticmethod
    def show_dialog(log, workflow, notifyByDropToWeb, users, question, answerType, answer, iDontKnow, renewDelay):

        if not notifyByDropToWeb:
            return False

        users = EOFAPI.normalize_users(users)

        if not users:
            raise Exception("No users have received the question. Workflow will be terminated")


        if str(answerType) == "multipleChoice":
            answers_xml = u"".join(
                class_xml_action.USER_DIALOG_ANSWER % {
                    "value" 	: key,
                    "display" 	: answer[key],
                    "cdata"		: u""
                    } for key in answer
            )
        else:
            answers_xml = u" "

        dialog = xml_action()
        dialog.set_data("user_dialog",
                        workflow_id	= workflow.id,
                        answerType	= answerType,
                        question	= question,
                        renewDelay	= renewDelay,
                        answers 	= answers_xml,
                        idontKnow 	= "Yes" if iDontKnow=="True" or iDontKnow==True else "No"
                        )
        dialog_xml = dialog.render()

        if log:
            engine.log(workflow.id,
                       u"Question to users:%s -> '%s'" %(
                           unicode(", ".join([u.name or u.email for u in users]) ),
                           question
                           ),"info")

        for user in users:
            engine.add_action( str(user.guid), dialog_xml )


        return True if users else False

    @staticmethod
    def send_question_by_mail(log, workflow, notifyByEmail, emailTo, emailObject, emailAttachDocument, emailDocument, question, answerType, answer):

        from helper import get_current_lang

        loc = get_current_lang(workflow.lang)

        """ Varify the email and other data.
		"""
        if not notifyByEmail:
            return False

        users = []

        if isinstance(emailTo, unicode) and re.search("[a-z0-9._%+-]+@(?:[a-z0-9-]+\.)+[a-z]{2,4}", emailTo):

            emails = emailTo.split(u",")
            for e in emails:
                e = e.strip().lower()
                match = re.search("[a-z0-9._%+-]+@(?:[a-z0-9-]+\.)+[a-z]{2,4}", e)
                if not match:
                    raise Exception("Invalid E-mail address: %s"%e)
                u = User.get_by_email(e)
                if u:
                    users.append(u)
        else:
            users = EOFAPI.normalize_users(emailTo)

        if not users:
            engine.log(workflow.id, "No users have received the question by email:<br> <i>%s</i>"%cgi.escape(emailTo))
            return False

        """ Prepare data for mail
		"""
        email_from = "EyeOnFiles workflow engine"
        attachment = []
        if emailAttachDocument:
            doc = ShareFile.get_by_guid(str(emailDocument))
            if doc:
                attachment = [(open(storage.abs_path(doc.complete_path), "rb").read(), doc.name.encode("utf8"), doc.mime(), doc.extension())]
            else:
                msg = u"The document you trying to attach is not exist or document id is incorrect: %s"%(unicode(email_doc))
                engine.log(workflow.id, msg,"error")
                raise Exception(msg)


        """ Compile question & answer into correct html form.
		"""
        if str(answerType) == "multipleChoice":
            answers_xml = u"""
                <style type=text/css rel=stylesheet>
                .multi {
                font-size:14px;
                font-family:arial;
                color: #888888;
                }
                .multi:hover {
                color: #0084FF;
                }
                </style>
                %s
                """ % u"".join(u"""
                               <div><a class='multi' href="%(link)s">%(display)s</a></div>
                               """ % {
                                       "link"		: u"http://%s/workflow_answer?type_var=multi&guid=%s&user_id=%s&answer=%s&key=%s" % (
                                           workflow.hostname,
                                           workflow.id,
                                           "%(user_id)s",
                                           key,
                                           workflow.confirmation_guid
                                           ),
                                       "display" 	: answer[key]
                                       } for key in answer
                                   )
        else:
            answers_xml = u"""
                <label>%(wf_q_toanswer)s:</label> <br/>
                <a href='%(link)s'>Link</a>
                """% {
                       "link" 			: u"http://%s/workflow_answer?type_var=freetext&guid=%s&user_id=%s&key=%s" % (
                           workflow.hostname,	workflow.id, u"%(user_id)s", workflow.confirmation_guid
                           ),
                       "workflow_id" 	: workflow.id,
                       #"user_id" 		: "%(user_id)s",
                       "wf_q_toanswer" : loc["wf_q_toanswer"]
                   }


        email_content = u"""<h3>%(workflow_label)s</h3>

            %(hp_hello)s, %(username)s. %(wf_q_received)s:
            <br/>
            <b>%(question_block)s</b>
            <br/><br/>
            <div>%(answer_block)s</div>
            """ % {
                    "question_block"	: question, #cgi.escape(question).replace("&ltbr&/gt", "<br/>"),
                    "workflow_label"	: workflow.label.decode("utf8"),
                    "answer_block"		: answers_xml,
                    "username"			: u"%(username)s",
                    "home_page"			: u"http://%s/documents_share"%workflow.hostname,
                    "logo_link"			: u"http://%s/c329ecb0-e088-4f45-ba3e-87b4c3cd7799.png"%workflow.hostname,
                    "wf_q_received"		: loc["wf_q_received"],
                    "hp_hello"			: loc["hp_hello"]

                }

        """ Send the mail to users.
		"""
        result = False
        for user in users:

            _id = server.mailer.send(email_from, user.email, emailObject, email_content % {"user_id":user.guid, "username":user.name}, attachment)
            notsent = server.mailer.check(_id)
            result = False if notsent else True

            if log:
                engine.log(workflow.id,u"Trying to send mail to <b>%s</b>"%user.email, "debug")

        """ return True - awaits for answer handler.
		"""
        return result


    @staticmethod
    def archive_document(step_name, log, workflow, destination, autoCreate, index, document, ocr_content=None):

        folder = None
        share = None
        auto_meta = ['mf_auto_fn','mf_auto_cd','mf_auto_crtr']

        #normalize the destination
        dest = os.path.normpath(destination).split("/")
        if dest[0] == "": dest = dest[1:] 	# cut
        #engine.log(workflow.id,u"norm d = %s"%unicode(dest),"debug")

        # check if path too short
        if len(dest) < 2:
            engine.log(workflow.id,u"Too short destination: %s"%destination,"error")
            return

        # check existance
        folders = Folder.get_top()
        reached = False
        parent_folder = None
        for d in dest[:-1]:
            #engine.log(workflow.id,u"d = %s"%unicode(d),"debug")
            reach_flag = False
            for f in folders:
                #engine.log(workflow.id,u"f = %s"%unicode(f.name),"debug")
                if unicode(d) == unicode(f.name):
                    #engine.log(workflow.id,u"f(%s) = d(%s)"%(unicode(f.name), unicode(d)),"debug")
                    reach_flag = True
                    parent_folder = f
                    folders = f.get_children()
                    break
            if reach_flag and d == dest[-2]:
                reached = True
            elif reach_flag:
                continue
            else:
                break

        if reached:
            # try to find share
            shares = parent_folder.get_document_shares_all()
            #engine.log(workflow.id,u"Shares = %s"%unicode([s.name for s in shares]), "debug")
            for sh in shares:
                #engine.log(workflow.id,u"sh(%s) = d(%s)"%(unicode(sh.name), unicode(dest[-1])))
                if unicode(sh.name) == unicode(dest[-1]):
                    share = sh
                    break

        if not isinstance(share, Document_share) and autoCreate:

            d = dest[0]
            folders = Folder.get_top()
            need_creation = False
            pf = None
            for f in folders:
                if unicode(f) == unicode(d):
                    pf = f
                    break
            if not pf:
                pf = Folder()
                pf.name = d
                pf.save(workflow.id)
                need_creation = True

            for d in dest[1:-1]:
                if not need_creation:
                    folders = pf.get_children()
                    found = False
                    for f in folders:
                        if unicode(f) == unicode(d):
                            pf = f
                            found = True
                            break
                    if found:
                        continue
                    else:
                        need_creation = True
                f_shares = pf.get_document_shares_all()
                if f_shares:
                    msg = u"One of destination folders '%s' contains document shares: %s"%(unicode(destination), unicode([sh.name for sh in f_shares]) )
                    engine.log(workflow.id, msg, "error")
                    raise Exception(msg)
                f = Folder()
                f.name = d
                f.parent_guid = pf.guid
                f.save(workflow.id)
                pf = f
            share = Document_share()
            share.name = dest[-1]
            share.create()
            if 	share:
                pf.add_ds(share)
                for ind in index:
                    if ind not in auto_meta:
                        mf 					= Metafield()
                        #engine.log(workflow.id, ind)
                        mf.name 			= ind#.decode("utf8")
                        mf.constraint 		= ""#index[ind]
                        mf.default 			= ""
                        mf.required 		= "0"
                        mf.user_can_modify 	= "1"
                        mf.visible 			= "1"
                        mf.type_id 			= "1" if not unicode(index[ind]).isdigit() else "3"
                        mf.document_share_guid = share.guid
                        mf.set_connector(indexconnector())
                        mf.save()
                        share.add_metafield(mf.id)

        if isinstance(share, Document_share):
            # send file inside share
            file = ShareFile.get_by_guid(str(document))
            if not file:
                msg = u"File doesn't exist: %s"%unicode(document)
                engine.log(workflow.id, msg,"error")
                raise Exception(msg)

            # all metafields == index income

            metafields 	= {} # id : value
            share_metas = share.get_metafields()
            for meta in share_metas:
                if meta.name in index:
                    meta_name = meta.name
                    # if meta type is smart folder, so read input value and assign right smart sub folder if possible
                    if meta.type_id == 5:
                        ssf = SmartSubFolder.get_by_name_and_parent_guid(index[meta_name], meta.constraint)
                        if ssf:
                            index[meta_name] = ssf.guid

                    metafields[meta.name] = (index[meta_name], meta)

            filename = index["mf_auto_fn"] if "mf_auto_fn" in index else file.name
            saved_doc 	= share.put_document(filename, file.handler(), metafields, ocr_content or None, workflow.id)

            if log:
                msg = u"Document '%s' was successfully put into share '%s' "%(filename, share.name)
                engine.log(workflow.id, msg, "info")

                engine.workflow_log[workflow.id].reportworkflow.set_data("step_archive",
                                                                         step_name 	= step_name,
                                                                         document	= filename,
                                                                         archive 	= destination
                                                                         )


            # create the folders/shares with correct index and put file inside
        else:
            msg = u"This share (%s) doesn't exist and autoCreate property set to False"%unicode(dest[-1])
            engine.log(workflow.id, msg,"error")
            raise Exception(msg)

    @staticmethod
    def action_workflow_trigger(log, action_name, workflow, file_id, user_id, log_data=None):

        if action_name not in ['workflowStarted', 'workflowFinished']:
            engine.log(workflow.id,u"Bad action name (%s). Must be 'workflowStarted', 'workflowFinished'"%action_name,"error")
            return
        trigger = xml_action()
        trigger.set_data("trigger",
                         workflow_id = workflow.id, action_name = action_name, file_id = file_id
                         )
        trigger_xml = trigger.render()
        engine.add_action( user_id, trigger_xml )

        if log:
            if action_name == 'workflowStarted':
                message = "Workflow <i>%s</i> has been started"%(workflow.name)
                engine.log(workflow.id, message, "info")
            elif action_name == 'workflowFinished' and log_data:
                message = log_data #"Workflow '%s' has been finished"%(workflow.name)
                engine.log(workflow.id, message, "info")



    @staticmethod
    def move_document(	log, workflow, document_guid, folder_from_guid, folder_to_guid, users=None, set_right=True,
                              keep_copy=True, keep_version_source=True, keep_version_target="version_folder_guid", step_name="default_name"):


        file = ShareFile.get_by_guid(str(document_guid))
        original_folder = file.parent_guid

        users = EOFAPI.normalize_users(users)
        for user in users:
            workflow.active_users.add(user.guid)

        if folder_to_guid and unicode(folder_to_guid).lower() == u"#trash":
            if file:
                #delete_file
                delete_file = xml_action()
                delete_file.set_data("delete_file",	workflow_id = workflow.id, file_id = file.guid, folder_id = file.parent.guid	)
                delete_file_xml = delete_file.render()

                users_add = file.users() # if file.acl_subjects else file.top_folder.users()
                for u in users_add: users.add(u)

                for user in users:
                    engine.add_action( user.guid, delete_file_xml )

                if log:
                    message = u"Deleting document <s>%s</s> from users: %s." %(file.name, unicode(", ".join([u.name or u.email for u in users])))
                    engine.log(workflow.id, message, "info")

                    engine.workflow_log[workflow.id].reportworkflow.set_data("step_move_trash",
                                                                             step_name	= step_name,
                                                                             users		= unicode(", ".join([u.name if u.name else u.email for u in users]) ),
                                                                             folder_from	= file.parent.path(),
                                                                             document_name = file.name
                                                                             )
                file.delete()

            return file.guid if file else None


        to_folder 	= ShareDir.get_by_guid(unicode(folder_to_guid))

        if log:
            engine.log(workflow.id, u"Moving document <b>%s</b> into folder <i>%s</i> to users: %s" %(
                unicode(file.name),
                unicode(to_folder.name),
                unicode(", ".join([(u.name if u.name else u.email) for u in users]) )
                ),"info")


            engine.workflow_log[workflow.id].reportworkflow.set_data("step_move",
                                                                     step_name	= step_name,
                                                                     users		= unicode(", ".join([u.name if u.name else u.email for u in users]) ),
                                                                     folder_from = file.parent.path(),
                                                                     folder_to 	= to_folder.path(),
                                                                     document_name = file.name
                                                                     )

        if not isinstance(to_folder, ShareDir):
            raise Exception("Destination folder coult not be found")


        if keep_copy:
            file = file.copy_to(to_folder)
        else:
            file.move_to(to_folder)
            #delete_file
            delete_file = xml_action()
            delete_file.set_data("delete_file",	workflow_id = workflow.id, file_id = file.guid, folder_id = original_folder			)
            delete_file_xml = delete_file.render()

            for user in file.users():
                engine.add_action( user.guid, delete_file_xml )

            file.clean_acls()

        download_file = xml_action()
        download_file.set_data("download_file",
                               workflow_id = workflow.id, folder_id = to_folder.guid, file_id = file.guid
                               )
        download_file_xml = download_file.render()


        if set_right:
            download_users = users
        elif to_folder.users():
            download_users = to_folder.users()
        else:
            download_users = to_folder.top_folder.users()

        for user in download_users:
            engine.add_action( user.guid, download_file_xml )

        for user in download_users:
            file.add_access_to(user.guid, unicode(user.rules["SEE"]))
            share_obj = file
            while share_obj.parent:
                p = share_obj.parent
                rule = user.rules["READ"] if p.is_top_folder else user.rules["SEE"]
                p.add_access_to(user.guid, unicode(rule))
                share_obj = share_obj.parent


        return file.guid

    @staticmethod
    def send_mail(log, workflow, users, email_title, email_content, email_attach, email_doc):

        emailTo = users
        if isinstance(emailTo, unicode) and re.search("[a-z0-9._%+-]+@(?:[a-z0-9-]+\.)+[a-z]{2,4}", emailTo):
            users = []
            #engine.log(workflow.id, emailTo)
            emails = emailTo.split(u",")
            for e in emails:
                e = e.strip().lower()
                match = re.search("[a-z0-9._%+-]+@(?:[a-z0-9-]+\.)+[a-z]{2,4}", e)
                if not match:
                    raise Exception("Invalid E-mail address: %s"%e)
                u = User.get_by_email(e)
                if u:
                    users.append(u)
        else:
            users = EOFAPI.normalize_users(emailTo)

        if not users: return

        #engine.workflow_log[workflow.id].insert(0, "::".join([workflow.id, str(users), email_title, email_content, str(email_attach), str(email_doc)]))

        email_from = "EyeOnFiles workflow engine"

        #attuple=(open(zipfilename, "rb").read(),name,"application/zip","zip")
        attachment = []
        if email_attach:
            #engine.log(workflow.id,"Sending mail: trying to found and attach doc with id: %s"%email_doc)
            doc = ShareFile.get_by_guid(str(email_doc))
            if doc:
                #engine.log(workflow.id,"Doc found!")
                attachment = [(open(storage.abs_path(doc.complete_path), "rb").read(), doc.name.encode("utf8"), doc.mime(), doc.extension())]
            else:
                msg = u"The document you trying to attach is not exist or document id is incorrect: %s"%(unicode(email_doc))
                engine.log(workflow.id, msg,"error")
                raise Exception(msg)


        if isinstance(emailTo, unicode) and re.search("[a-z0-9._%+-]+@(?:[a-z0-9-]+\.)+[a-z]{2,4}", emailTo):
            users = []
            #engine.log(workflow.id, emailTo)
            emails = emailTo.split(u",")
            for e in emails:
                e = e.strip().lower()
                match = re.search("[a-z0-9._%+-]+@(?:[a-z0-9-]+\.)+[a-z]{2,4}", e)
                if not match:
                    raise Exception("Invalid E-mail address: %s"%e)
                u = User.get_by_email(e)
                if u:
                    users.append(u)

        for user in users:

            #server.mailer.send(email_from, user.email, email_title, email_content, attachment)
            if log:
                engine.log(workflow.id,u"Trying to send mail to <b>%s</b>"%user.email, "debug")

    @staticmethod
    def send_popup(log, workflow, users, content):

        users = EOFAPI.normalize_users(users)
        if not users: return

        online_users = set()
        for id in engine.online_users():
            u = User.get_by_guid(id)
            if u:
                online_users.add(UserScriptObject(u))

        if log:
            engine.log(workflow.id, u"Showing Popup to %s -> <i>%s</i>"%(u", ".join([u.name for u in users]), unicode(content)), "debug")

        notify = xml_action()
        notify.set_data("notify",
                        workflow_id = workflow.id, content = content
                        )
        notify_xml = notify.render()

        for user in users:
            #engine.log(workflow.id,"Adding Notify action into queue for user: <b>%s</b>"%user.email, "debug")
            engine.add_action( user.guid, notify_xml )

        users &= online_users

        return True if users else False

    """ Filter input users. Converts to list.
		Replace user email or id with UserSctiptObject if needed.
	"""
    @staticmethod
    def normalize_users(users):
        if users is None: return set()
        if isinstance(users, UserContainerScriptObject):
            return set(users)
        if not isinstance(users, list): users = [users]

        result = set()
        for user in users:
            if isinstance(user, UserScriptObject):
                result.add(user)
                continue
            u = User.get_by_guid(user)
            if u:
                result.add(UserScriptObject(u))
                continue
            u = User.get_by_email(user)
            if u:
                result.add(UserScriptObject(u))

        return result

    @staticmethod
    def send_log(step_type, **kwargs):

        if step_type == "event":
            workflow 	= kwargs.get("workflow", "bad_param")
            user		= User.get_by_guid(kwargs.get("user_id", "bad_param"))
            doc 		= ShareFile.get_by_guid(kwargs.get("doc"))
            event_type 	= kwargs.get("event_type", "bad_param")
            step_name	= kwargs.get("step_name", "bad_param")

            message = "User <i>%s</i> send document <b>%s</b> with event <i>%s</i>" % (
                user.name if user else "bad_param",
                doc.name if doc else "bad_param",
                event_type
            )
            engine.log(workflow.id, message, "info")

            rWorkflow = engine.workflow_log[workflow.id].reportworkflow
            rWorkflow.set_data("step_event_"+event_type[:3],
                               step_name	= step_name,
                               author		= user.name if user else "bad_param",
                               doc_name	= doc.name 	if doc else "bad_param"
                               )

        elif step_type == "answered":
            workflow 	= kwargs.get("workflow", "bad_param")
            user		= User.get_by_guid(kwargs.get("user_id", "bad_param"))
            answer 		= kwargs.get("answer", "bad_param")
            step_name	= kwargs.get("step_name", "bad_param")
            question	= kwargs.get("question", "bad_param")
            users		= EOFAPI.normalize_users(kwargs.get("users", "bad_param"))

            msg = u"User <i>%s</i> answered <b>%s</b> on previous question" % (user.name, answer)
            engine.log( workflow.id, msg, "info" )

            rWorkflow = engine.workflow_log[workflow.id].reportworkflow
            rWorkflow.set_data("step_question",
                               step_name	= step_name,
                               author		= user.name,
                               question	= question,
                               users		= unicode(", ".join([u.name if u.name else u.email for u in users]) ),
                               answer 		= answer
                               )
        elif step_type in ("step_notify_mail","step_notify_only_mail","step_notify_no_mail"):
            workflow 	= kwargs.get("workflow", "bad_param")
            step_name 	= kwargs.get("step_name", "bad_param")
            users_popup	= EOFAPI.normalize_users(kwargs.get("users_popup", "bad_param"))
            info		= kwargs.get("info", "bad_param")

            users		= kwargs.get("users_mail", "bad_param")
            mail_content= kwargs.get("mail_content", "bad_param")
            attachment	= kwargs.get("attachment", "bad_param")

            if isinstance(users, unicode) and re.search("[a-z0-9._%+-]+@(?:[a-z0-9-]+\.)+[a-z]{2,4}", users):
                users_mail = []
                emails = users.split(u",")
                for e in emails:
                    e = e.strip().lower()
                    match = re.search("[a-z0-9._%+-]+@(?:[a-z0-9-]+\.)+[a-z]{2,4}", e)
                    if not match:
                        raise Exception("Invalid E-mail address: %s"%e)
                    u = User.get_by_email(e)
                    if u:
                        users_mail.append(u)
            else:
                users_mail = EOFAPI.normalize_users(users)


            rWorkflow = engine.workflow_log[workflow.id].reportworkflow

            if step_type=="step_notify_mail":
                rWorkflow.set_data(step_type,
                                   step_name 		= step_name,
                                   users_popup		= unicode(", ".join([u.name if u.name else u.email for u in users_popup]) ),
                                   info			= info,

                                   users_mail		= unicode(", ".join([u.name if u.name else u.email for u in users_mail]) ),
                                   mail_content	= mail_content,
                                   attachment		= u"<b>With attachment</b>" if attachment else u"<s>No attachment</s>"
                                   )
            elif step_type=="step_notify_only_mail":
                rWorkflow.set_data(step_type,
                                   step_name 		= step_name,
                                   users			= unicode(", ".join([u.name if u.name else u.email for u in users_mail]) ),
                                   mail_content	= mail_content,
                                   attachment		= u"<b>With attachment</b>" if attachment else u"<s>No attachment</s>"
                                   )
            else:
                rWorkflow.set_data(step_type,
                                   step_name 		= step_name,
                                   users			= unicode(", ".join([u.name if u.name else u.email for u in users_popup]) ),
                                   info			= info,
                                   )
        elif step_type in ("auto", "conditional", "timeout", "error"):
            workflow 	= kwargs.get("workflow", "bad_param")
            step_name 	= kwargs.get("step_name", "bad_param")
            step_to 	= kwargs.get("step_to", "bad_param")
            step_from 	= kwargs.get("step_from", "bad_param")

            rWorkflow = engine.workflow_log[workflow.id].reportworkflow
            rWorkflow.set_data(step_type,
                               step_name 	= step_name,
                               step_to		= step_to,
                               step_from	= step_from
                               )

