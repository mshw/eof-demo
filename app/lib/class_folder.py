from class_ACL import ACL
from class_db import Database
from class_document_share import Document_share

class Folder:

    @classmethod
    def get_document_share_in_folder(self):

        query = """SELECT id, document_share_guid,  folder_guid
					FROM `document_share_in_folder`
					"""
        rows = Database.maindb().fetch_all(query,)

        return [{"id":row[0], "document_share_guid":row[1],  "folder_guid":row[2]} for row in rows]

    def search_shares(self, doc_shares, search_words, searchmethod, startrank, endrank, sortby):

        from indexers import normalize
        from utils_old import search_escape
        from class_user import User
        from class_document import Document

        result = {}

        if not doc_shares: return result

        conn = self.__search.get_iconn()
        checkatleast=0

        type = str(searchmethod)
        if 		type == "1":
            q1 = conn.query_parse(search_words, None,["text", "category", "db_id"],  0,None, ["text", "category", "db_id"], allow_wildcards=True)
        elif 	type == "2":
            q1 = conn.query_parse(search_words,["text"], None,  0,["text"], None, allow_wildcards=True)
        else:
            q1 = conn.query_parse(search_words, allow_wildcards=True)

        queries_shares = [conn.query_field('category', ds.guid) for ds in doc_shares]

        query_shares = conn.query_composite(conn.OP_OR, queries_shares)

        #query = query_shares

        #raise Exception(str(query))
        query = conn.query_composite(conn.OP_AND, (query_shares, q1))


        maxdocs = conn.get_doccount()
        result_docs = conn.search(query, 0, maxdocs, checkatleast, sortby=sortby)
        if not result_docs: return result

        shares_dict = dict([ds.guid, ds] for ds in doc_shares)
        result = dict([ds, [ [], 0 ]] for ds in doc_shares)
        for doc in result_docs:
            document = Document.from_xapian_document(doc)
            docinf = document.xapian_description()
            if "deleted" not in docinf:
                result[shares_dict[document.ds_guid()]][0].append( document )
                result[shares_dict[document.ds_guid()]][1] += 1
        return result


        #raise Exception(str(result))
#		return [Document.from_xapian_document(doc) for doc in search_result],  search_result.matches_estimated


    @classmethod
    def get_by_guid(self, guid):
        query = """SELECT id, guid,  name, parent_guid, deleted
					FROM folder
					WHERE `guid` = ?"""
        row = Database.maindb().fetch_one(query, (str(guid),))

        if row:
            return Folder(row)
        return None

    @classmethod
    def get_all(self, page=-1,top=False):
        if page == -1:
            limit_str = ""
            query_parse = ()
        else:
            limit_str = "LIMIT ?,500"
            query_parse = (int(page) * 500,)
        where_str = "WHERE `parent_guid` is NULL" if top else ""
        query = """SELECT id, guid,  name , parent_guid, deleted
					FROM `folder`
					%s
					ORDER BY name
					%s
					""" % (where_str,limit_str)
        rows = Database.maindb().fetch_all(query, query_parse)
        return [Folder(row) for row in rows]
    
    @classmethod
    def get_top(self, page=-1):
        return self.get_all(page,True)
    
    @classmethod
    def get_top_for_user(self, user, page=-1):
        if user.is_admin():
            return Folder.get_top(page)

        groups, subj_guids = user.get_groups(), [user.guid]
        for g in groups:
            subj_guids.append(g.guid.encode("utf8"))
        subj_guids = ["acl.subject='%s'"%subj for subj in subj_guids]
        where_acl = "WHERE (%s)" % " OR ".join(subj_guids)

        if page == -1:
            limit_str = ""
            query_parse = ()
        else:
            limit_str = "LIMIT ?,500"
            query_parse = (int(page) * 500,)

        query = """
					SELECT folder.id, folder.guid,  folder.name , folder.parent_guid, folder.deleted
					FROM `folder`
					WHERE folder.parent_guid is NULL AND folder.guid in
					(SELECT acl.object FROM acl
					%s)
					ORDER BY name
					%s
				""" % (where_acl, limit_str)
        rows = Database.maindb().fetch_all(query, query_parse)
        return [Folder(row) for row in rows]

    def __init__(self, row=None):
        self.__search = None
        self.__connector = None

        if row:
            self.id = row[0]
            self.guid = row[1]
            self.name = row[2]
            self.parent_guid = row[3]
            self.deleted = row[4]

        else:
            self.id = -1
            self.guid = ""
            self.name = ""
            self.parent_guid = None
            self.deleted = "false"

    def __str__(self):
        return self.name

    def delete(self):
        if not ACL.current_user_can_delete_object(self): return None

        for a_folder in self.get_children():
            a_folder.set_xapy_access(self.__connector, self.__search)
            a_folder.delete()

        for doc_share in self.get_document_shares():
            doc_share.set_connector(self.__connector)
            doc_share.set_search(self.__search)
            doc_share.delete(skip_xapian=True)

        for acl in ACL.get_by_object(self.guid):
            acl.delete()

        query = """DELETE FROM folder
					WHERE guid=? """
        Database.maindb().commit(query, (str(self.guid),))

    def get_users(self):
        query = """SELECT    user.id,  user.guid,   user.password,  user.email, user.picture
					FROM user, acl
					WHERE acl.subject = user.guid and acl.object = ?
					ORDER BY user.email	"""
        rows = Database.maindb().fetch_all(query, (str(self.guid),))

        from class_user import User
        return [User(row) for row in rows]

    def set_xapy_access(self, connector, search):
        self.__search = search
        self.__connector = connector

    def save(self, workflow_id=None):

        if workflow_id:
            import uuid
            self.name = self.name
            if not self.guid:
                self.guid =  str(uuid.uuid4())
            query = """INSERT INTO folder(name, guid, parent_guid)	VALUES(?,?,?)"""
            rows = Database.maindb().commit(query,(self.name, self.guid, self.parent_guid),)

            return

        parent = self.get_parent()
        from class_user import User
        user = User.current()
        if parent and user and parent.get_document_shares(user) :
            raise Exception("CANT_CREATE_FOLDER_WITH_SHARES")

        import uuid
        from utils_old import html_escape
        self.name = self.name#html_escape(self.name)

        if not self.guid:
            self.guid =  str(uuid.uuid4())
        query = """INSERT INTO folder(name, guid, parent_guid)
					VALUES(?,?,?)"""
        rows = Database.maindb().commit(query,(self.name, self.guid, self.parent_guid),)

    def get_children(self, page=-1):
        if page == -1:
            limit_str = ""
            query_parse = (self.guid, )
        else:
            limit_str = "LIMIT ?,500"
            query_parse = (self.guid, int(page) * 500,)
        query = """SELECT id, guid,  name, parent_guid, deleted
					FROM folder
					WHERE parent_guid = ?
					ORDER BY name
					%s
					""" % limit_str
        rows = Database.maindb().fetch_all(query, query_parse)

        return [Folder(row) for row in rows]

    def get_children_for_user(self, user, page=-1):
        groups, subj_guids = user.get_groups(), [user.guid]
        for g in groups:
            subj_guids.append(g.guid.encode("utf8"))
        subj_guids = ["acl.subject='%s'"%subj for subj in subj_guids]
        where_acl = "WHERE (%s)" % " OR ".join(subj_guids)

        if page == -1:
            limit_str = ""
            query_parse = (self.guid, )
        else:
            limit_str = "LIMIT ?,500"
            query_parse = (self.guid, int(page) * 500,)
        query = """
					SELECT folder.id, folder.guid,  folder.name , folder.parent_guid, folder.deleted
					FROM `folder`
					WHERE folder.parent_guid = ?  AND folder.guid in
					(SELECT acl.object FROM acl
					%s)
					ORDER BY name
					%s
				""" % (where_acl, limit_str)

        #raise Exception(query)
        rows = Database.maindb().fetch_all(query, query_parse)

        return [Folder(row) for row in rows]

    def get_parent(self):
        return Folder.get_by_guid(self.parent_guid)

    def get_document_shares_all(self):
        return Document_share.get_by_folder_guid(self.guid)

    def get_document_shares(self, user="", page=-1):
        #return Document_share.get_by_folder_guid(self.guid)
        if not user:
            from class_user import User
            user = User.current()
        return Document_share.get_by_folder_guid_for_user(self.guid, user, page=-1)

    def add_document_share(self, document_share, user=""):
        if not user:
            from class_user import User
            user = User.current()
        if document_share and not self.get_children_for_user(user):
            query = """INSERT INTO document_share_in_folder(folder_guid, document_share_guid)
						VALUES(?,?)"""
            rows = Database.maindb().commit(query,(self.guid, document_share.guid),)
        else:
            raise Exception("CANT_CREATE_FOLDER_WITH_SHARES")

    def add_ds(self, ds):
        query = """INSERT INTO document_share_in_folder(folder_guid, document_share_guid)
						VALUES(?,?)"""
        rows = Database.maindb().commit(query,(self.guid, ds.guid),)

    def rename(self, new_name):

        if new_name == "": return
        self.name = new_name
        query = """
			UPDATE folder
			SET name=?
			WHERE guid=?
		"""
        #debug("new_name = " + new_name)
        rows = Database.maindb().commit(query,(self.name, self.guid,))
        return None

    def set_deleted_partial(self):
        query = """
            UPDATE folder
        	SET deleted=?
        	WHERE guid=?
        """
        Database.maindb().commit(query, ("partial", self.guid,))

    def set_deleted_true(self):
        query = """
            UPDATE folder
        	SET deleted=?
        	WHERE guid=?
        """
        Database.maindb().commit(query, ("true", self.guid,))

    def set_deleted_false(self):
        query = """
            UPDATE folder
        	SET deleted=?
        	WHERE guid=?
        """
        Database.maindb().commit(query, ("false", self.guid,))

    def set_parents_partial_deleted(self):
        if self.deleted == 'false':
            self.set_deleted_partial()

        # start recursion if has parent
        parent = self.get_parent()
        if parent:
            parent.set_parents_partial_deleted()

    def correct_status(self):
        children = self.get_children()
        document_shares = self.get_document_shares()
        has_deleted = False
        # Correct status for shares

        for doc in document_shares:
            if doc.deleted != 'false':
                has_deleted = True
                self.set_deleted_partial()
                break

        # Correct status for folders
        for folder in children:
            if folder.deleted != 'false':
                has_deleted = True
                self.set_deleted_partial()
                break

        # Restore folder if it does no have deleted folders or shares
        if not has_deleted:
            self.set_deleted_false()

        # Get Parent folder to check correction
        parent = self.get_parent()
        if parent:
            parent.correct_status()


    """
		copy DS structure
	"""
    def copy_structure(self, user, connector, search, from_guid, to_guid, count=1):

        def get_attr(obj_name, obj):
            attr = {}
            if obj_name == obj_folder:
                attr = {
                    "id" : obj.id,
                    "guid" : obj.guid,
                    "name" : obj.name,
                    "parent_guid" : obj.parent_guid
                }
            elif obj_name == obj_share:
                attr =  {
                    "id" : obj.id,
                    "guid" : obj.guid,
                    "name" : obj.name
                }
            return attr # attributes

        def get_share_content(share):
            meta_content, docs_content = [],[]#["SHARE CONTENT"]
            share.set_connector(connector)

            a_metas = share.get_additional_metafields()
            for meta in a_metas:
                new_metafield = {
                    "id" : meta.id, "name" : meta.name, "type_id" : meta.type_id,
                    "default" : meta.default, "constraint" : meta.constraint, "required" : meta.required,
                    "user_can_modify" : meta.user_can_modify, "visible" : meta.visible
                }
                meta_content.append(new_metafield)

            share.set_search(search)
            a_docs = share.get_documents2()
            for doc in a_docs:
                new_doc = {
                    "file_name" : doc.filename(html_encode=True)
                }
                docs_content.append(new_doc)

            return [meta_content, docs_content]

        def get_content(folder):
            obj_attr, obj_content = [],[]
            obj_attr = get_attr(obj_folder, folder)

            for a_folder in user.get_subfolders(folder):
                obj_content.append( get_content(a_folder) )

            for doc_share in user.get_documents_shares(folder):
                obj_content.append([
                    obj_share, get_attr(obj_share, doc_share), get_share_content(doc_share)
                ])

            return [obj_folder, obj_attr, obj_content]

        def set_folder(f_attr, f_folder):
            new_folder_name = unicode(f_attr["name"])#.encode("utf8")
            new_folder = Folder()
            new_folder.name = new_folder_name
            new_folder.parent_guid = f_folder.guid
            new_folder.save()
            #if "setacl"  in args:	set_parents_ACL(new_folder)
            return new_folder

        def set_document_share(d_attr, d_content, f_folder):
            new_share = d_attr["name"]
            doc_share = Document_share()
            doc_share.name = new_share
            doc_share.create()
            if 	doc_share:
                f_folder.add_document_share(doc_share)
                #if "setacl"  in args:	set_parents_ACL(doc_share)

                for meta in d_content[0]:
                    metafield = Metafield()

                    metafield.name = meta["name"]
                    metafield.constraint =  meta["constraint"]
                    metafield.default = meta["default"]
                    metafield.required = meta["required"]
                    metafield.user_can_modify = meta["user_can_modify"]
                    metafield.visible = meta["visible"]
                    metafield.type_id = meta["type_id"]
                    metafield.document_share_guid = doc_share.guid

                    if metafield:
                        metafield.set_connector(connector)
                        metafield.save()
                        doc_share.add_metafield(metafield.id)

                for doc in d_content[1]:
                    pass

            return ""

        def set_content(content, cur_folder=""):
            if content:
                if content[0] == obj_folder:
                    cur_folder = set_folder(content[1], cur_folder)
                    for item in content[2]:
                        set_content( item, cur_folder )
                else:
                    set_document_share(content[1], content[2], cur_folder)

            return 	""

        ### "init" ###
        from class_metafield import Metafield
        copy_temp = []
        obj_folder, obj_share = "folder", "share"

        ### "making COPY template" ###
        for guid in from_guid:
            DS_object = Folder.get_by_guid(guid)
            # "if share is copied..."
            if not DS_object: # should be share
                DS_object = Document_share.get_by_guid(guid)
                copy_temp.append([
                    obj_share, get_attr(obj_share, DS_object), get_share_content(DS_object)
                ])
            else:
                copy_temp.append( get_content(DS_object) )

        ### "making PASTE into to_guid folder " ###
        st = ""

        for times in range(count):
            for top_object in copy_temp:
                set_content(top_object, Folder.get_by_guid(to_guid))

        return
