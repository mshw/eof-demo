from modules.session import ManagedSessionInterface, CachingSessionManager, FileBackedSessionManager
import datetime, pytz,os,shutil,flask,tempfile,logging,json
from tempfile import NamedTemporaryFile
from io import BytesIO
from werkzeug.datastructures import iter_multi_items
from logging.handlers import RotatingFileHandler
from config import CONFIG,config_wrap
formatter = logging.Formatter(
    "[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s")
handler = RotatingFileHandler(CONFIG['LOG_FILENAME'], maxBytes=10000000, backupCount=5,encoding="utf8")
handler.setFormatter(formatter)
log = logging.getLogger()
log.addHandler(handler)
log.setLevel(logging.INFO)

log2 = logging.getLogger('werkzeug')
log2.setLevel(logging.INFO)

stdout_handler = logging.StreamHandler()
log2.addHandler(stdout_handler)
logging.info("EOF v.2 application starting")

app = flask.Flask(__name__, static_url_path='',static_folder='newstatic')
from app import views


class MyRequest(flask.Request):
    def _get_file_stream(self, total_content_length, content_type,
                         filename=None, content_length=None):
        """The stream factory that is used with NamedTempFile."""
        if total_content_length > 1024 * 500:
            return NamedTemporaryFile('wb+',delete=False)
        return BytesIO()        
    
    def close(self):
        """Closes associated resources of this request object.  This
        closes all file handles explicitly.  You can also use the request
        object in a with statement with will automatically close it.

        .. versionadded:: 0.9
        """
        files = self.__dict__.get('files')
        for key, value in iter_multi_items(files or ()):
            if not value.closed:
                value.close()    
                try:
                    if hasattr(value.stream,"name"):
                        os.unlink(value.stream.name)
                except Exception:
                    pass

app.request_class = MyRequest


#root.addHandler(stdout_handler)    
    
tempfile.tempdir = CONFIG['TEMP-DIRECTORY']
#checking folder structure
if not os.path.isdir(CONFIG['TEMP-DIRECTORY']):
    os.makedirs(CONFIG['TEMP-DIRECTORY'])
    
if not os.path.isdir(CONFIG["LOCAL-BACKUPS"]):
    os.makedirs(CONFIG["LOCAL-BACKUPS"])    
if not os.path.isdir(CONFIG['STORAGE-DIRECTORY']):
    os.makedirs(os.path.join(CONFIG['STORAGE-DIRECTORY'],"useravatars"))
    
    shutil.copy(os.path.join("app","newstatic","images","avatar.png"), os.path.join(CONFIG['STORAGE-DIRECTORY'],"useravatars","default.png"))
if not os.path.isdir(CONFIG['DB-DIRECTORY']):
    os.makedirs(CONFIG['DB-DIRECTORY'])
    shutil.copy("db_eof.sqlite", CONFIG['DB-DIRECTORY'])
if not os.path.isdir("thumbnails-cache"):    
    os.makedirs("thumbnails-cache")
if not os.path.isdir(os.path.join(CONFIG['STORAGE-DIRECTORY'],"pdf-thumbnails")):    
    os.makedirs(os.path.join(CONFIG['STORAGE-DIRECTORY'],"pdf-thumbnails"))

os.environ["overridden_settings"] = json.dumps({"THUMBNAIL_STORAGE_BACKEND":"app.lib.thumbnail_storage.FilesystemStorageBackend","THUMBNAIL_ALTERNATIVE_RESOLUTIONS":[]})

#,"THUMBNAIL_PATH":os.path.join(CONFIG['STORAGE-DIRECTORY'],"thumbnails-cache")})
import locale
try:
    locale.setlocale(locale.LC_ALL, CONFIG['LOCALE'])
    pass
except:pass
app.tz =  pytz.timezone(CONFIG['TIMEZONE'])
app.debug = bool(CONFIG['DEBUG'] == "1")
app.session_interface = ManagedSessionInterface(CachingSessionManager(FileBackedSessionManager(CONFIG['TEMP-DIRECTORY'], app.secret_key), 1000), ['img','css','fonts','images','js','uploadify'], datetime.timedelta(minutes=60))

from app.lib.update import Update
Update.run()

from app.lib.scheduler import Scheduler
Scheduler.start()

version = "v.%s/%s. DB v.%s"%(CONFIG["VERSION"],CONFIG["VERSIONDATE"],Update.get_cur_version())
logging.info("Version: %s"%version)
